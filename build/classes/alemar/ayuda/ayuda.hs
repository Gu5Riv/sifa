<?xml version='1.0' encoding='ISO-8859-1'?>
<helpset>
	<!-- Titulo de la ventana de ayuda -->
	<title>Ayuda Del Sistema Alemar S.A De C.V</title>
	
	<!-- Referencia al archivo de mapeo -->
	<maps>
		<homeID>inicio</homeID>
		<mapref location="mapeo.jhm"/>
	</maps>
	
	<!-- Aca se establece el arbol de archivos -->
	<view xml:lang="es">
		<name>Tabla de contenidos</name>
		<label>Tabla De Contenidos</label>
		<type>javax.help.TOCView</type>
		<data>toc.xml</data>
	</view>
	
	<presentation default="true">
		<name>ventana principal</name>
		<size width="800" height="600" />
		<location x="200" y="200" />
		<title>Ayuda Del Sistema Alemar S.A De C.V</title>
		<toolbar>
		<helpaction>javax.help.BackAction</helpaction>
		<helpaction>javax.help.ForwardAction</helpaction>
		<helpaction image="homeicon">javax.help.HomeAction</helpaction>
		</toolbar>
	</presentation>
</helpset>
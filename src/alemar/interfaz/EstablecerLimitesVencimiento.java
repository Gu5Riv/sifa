
package alemar.interfaz;


import alemar.configuracion.HibernateConexion;
import alemar.entidad.Producto;
import java.util.List;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;
import org.hibernate.*;
import java.util.Date;
/**
 *
 * @author Gus
 */
public class EstablecerLimitesVencimiento extends javax.swing.JPanel {

    
    //Atributos para el uso de Hibernate
    private static SessionFactory sessionFactory = null;

    //Para guardar el id del proveedor
    //private String id;
    private int id;
    //private String nom;
    

    public EstablecerLimitesVencimiento() {
        initComponents();
        
        //Se carga el comboBox Dinamicamente de la BD
        cargarComboBox();
        
        //Por default estaran desabilitados por si quiere ingresar datos
        //sin haber seleccionado un producto
        cldVencimiento.setEnabled(false);
        cldLimiteVencimiento.setEnabled(false);

        
        //Agregando mensajes de ayuda para el llenado de los campos
        cldVencimiento.setToolTipText("Fecha de vencimiento del producto.");
        cldLimiteVencimiento.setToolTipText("Digite la fecha donde desea se le notifique \n del producto próximo a vencer.");
    }
    
    
    
        //Metodo para cargar el comboBox dinamicamente de la BD
    private void cargarComboBox(){
        
        //Obtencion de todos los productos de la base de datos
        //para llenar el comboBox de manera dinamica
        
        //Variable que contendra la lista de productos
        List<Producto> listaProducto = null;
        
        //Inicializacion de session de hibernate
        Session session = null;
        try{
            try{
                //Chunces que ocupa Hibernate
                sessionFactory = HibernateConexion.getSessionFactory();
                session = sessionFactory.openSession();
                Transaction tx = session.beginTransaction();
                
                //Obteniendo de la BD todos los proveedores                
                listaProducto = session.createQuery("from Producto").list();
                
                //Creando un modelo de comboBox                
                DefaultComboBoxModel modeloCombo = new DefaultComboBoxModel();
                
                //Poniendole un mensaje de seleccion (Primer elemento)
                 modeloCombo.addElement("<--Seleccione un Producto-->");
                
                //Llenando el modelo de comboBox con los nombres de los proveedores
                for(Producto p : listaProducto) 
                { 
                    modeloCombo.addElement(p.getNombreProducto());
                }
                
                //Agregando el modelo del comboBox al comboBox del panel
                cmbProductos.setModel(modeloCombo);
                
            }catch(Exception e){
                    System.out.println(e.getMessage());
                }
            }finally {
                session.close();
        }
        
    }
    
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        btnEstablecer = new javax.swing.JButton();
        btnLimpiar = new javax.swing.JButton();
        cmbProductos = new javax.swing.JComboBox();
        cldVencimiento = new com.toedter.calendar.JDateChooser();
        cldLimiteVencimiento = new com.toedter.calendar.JDateChooser();

        setMaximumSize(new java.awt.Dimension(745, 400));
        setMinimumSize(new java.awt.Dimension(745, 400));
        setPreferredSize(new java.awt.Dimension(745, 400));

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        jLabel1.setText("Establecer límites de vencimiento  de productos");

        jLabel2.setText("Fecha de Vencimiento");

        jLabel3.setText("Fecha Límite");

        btnEstablecer.setText("Establecer");
        btnEstablecer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEstablecerActionPerformed(evt);
            }
        });

        btnLimpiar.setText("Limpiar");
        btnLimpiar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLimpiarActionPerformed(evt);
            }
        });

        cmbProductos.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        cmbProductos.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbProductosItemStateChanged(evt);
            }
        });

        cldLimiteVencimiento.setMaxSelectableDate(new java.util.Date(253370790076000L));
        cldLimiteVencimiento.setMinSelectableDate(new java.util.Date(-62135744324000L));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(197, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(btnLimpiar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnEstablecer)
                        .addContainerGap())
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(cmbProductos, javax.swing.GroupLayout.PREFERRED_SIZE, 245, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(234, 234, 234))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                .addGap(40, 40, 40)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel3)
                                    .addComponent(jLabel2))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(cldVencimiento, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(cldLimiteVencimiento, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                            .addComponent(jLabel1))
                        .addGap(212, 212, 212))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(67, 67, 67)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addGap(72, 72, 72)
                        .addComponent(cmbProductos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(59, 59, 59)
                        .addComponent(jLabel2))
                    .addComponent(cldVencimiento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(23, 23, 23)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel3)
                    .addComponent(cldLimiteVencimiento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 105, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnEstablecer)
                    .addComponent(btnLimpiar))
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void cmbProductosItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbProductosItemStateChanged
        List<Producto> NProducto = null;
        
        //Se extrae el proveedor seleccionado
        String nombreProducto = cmbProductos.getSelectedItem().toString();
        
        if(!(nombreProducto.equalsIgnoreCase("<<--Seleccione un Producto-->"))){
                        
            //Chunche de Hibernate (Inicio de sesion de hibernate)
            Session session = null;
            //Cadena de seleccion de HQL
            String hql = "FROM alemar.entidad.Producto AS p WHERE p.nombreProducto = :nombreProducto";
            try{
                try{
                    //Conexion a la BD
                    sessionFactory = HibernateConexion.getSessionFactory();
                    //Apertura de la sesion
                    session = sessionFactory.openSession();
                    Transaction txt = session.beginTransaction();                                       
                    
                    //Extraccion de los datos del proveedor de la BD en base al nombre proporcionado                    
                    Query query = session.createQuery(hql).setString("nombreProducto", nombreProducto);
                    
                    //Se pone en una lista el registro sacado
                    NProducto = query.list();
                    
                    //Guardando el id del producto en una variable global
                    //id = NProducto.get(0).getNombreProducto();
                    id = NProducto.get(0).getIdProducto();
                    
                    //Guardando el nombre del producto en una variable global
                    String nomProd = NProducto.get(0).getNombreProducto();
                    
                    limpiarHabilitar();
                    
                    //Se ponen los valores en los campos de texto correspondientes
                    //para que se modifiquen
                    
                    cldVencimiento.setDate(NProducto.get(0).getVencimiento());
                                    
                    
                    txt.commit();
                    
                    }catch(Exception e){
                    System.out.println(e.getMessage());
                }
            } finally {
                //Finalizacion de la sesion de hibernate
                session.close();
                }
        }else{
            cldVencimiento.setEnabled(false);
            cldLimiteVencimiento.setEnabled(false);
            cmbProductos.setSelectedIndex(0);

        }
    }//GEN-LAST:event_cmbProductosItemStateChanged

    private void btnEstablecerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEstablecerActionPerformed
        //Variables que contendran los valores de los campos de texto 
//        String cantidadActual = "";         
//        String limiteMinimo = "";
        String fechaLimite = "";
        
        //Variable que maneja la respuesta del JOptionpane
        int conf;
             
        //Extrayendo la informacion de los campos de texto
//        cantidadActual = txtCantidadActual.getText();
//        limiteMinimo = txtLimiteMinimo.getText();
        fechaLimite = cldLimiteVencimiento.getDateFormatString();
        Date fechaLim = cldLimiteVencimiento.getDate();
        
        //Validaciones
        if(!(fechaLimite == null)){ //Si no esta vacio algun campo           
            //Chunche de Hibernate (Inicio de sesion de hibernate)
            Session session = null;
            try{
                try{
                    //Conexion a la BD
                    sessionFactory = HibernateConexion.getSessionFactory();
                    //Apertura de la sesion
                    session = sessionFactory.openSession();
                    Transaction tx = session.beginTransaction();                    
                    
                    //Extrayendo el regitro del producto seleccionado
                    //en base al ID guardado previamente de manera global
                    Producto prod = (Producto)session.get(Producto.class, id);                    
                    
                    //Ingresando la nueva informacion del producto al 
                    //objeto Producto
                    
                    prod.setLimiteVencimiento(fechaLim);
                                      
                    //Mensaje de pregunta si quiere guardar los datos proporcionados 
                    conf=JOptionPane.showConfirmDialog(null,"¿Esta seguro de ingresar esos datos?","Confirmación",JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE);
                    if(conf == JOptionPane.YES_OPTION){//Si la opcion fue SI
                        
                        //Actualiza los datos del proveedor
                        session.update(prod);
                       
                        tx.commit();
                        //Mensaje de confirmacion de insercion Exitosa
                        JOptionPane.showMessageDialog(null,"Registro guardado exitosamente","Información",JOptionPane.INFORMATION_MESSAGE);
                        
                        //Se resetean los campos de texto
                            limpiarDeshabilitar();
              
                        cmbProductos.setSelectedIndex(0);

                    }
                    else{
                        cmbProductos.setSelectedIndex(0);
                    }
                }catch(Exception e){
                    System.out.println(e.getMessage());
                }
            } finally {
                //Finalizacion de la sesion de hibernate
                session.close();
                //Se vuelve a cargar el comboBox
                cargarComboBox();
        }
      }
        else {//Si hay campos vacios, mensaje de error
            JOptionPane.showMessageDialog(null,"Error, no pueden haber campos vacios","Error en los datos",JOptionPane.ERROR_MESSAGE);
            
        }
    }//GEN-LAST:event_btnEstablecerActionPerformed

    private void btnLimpiarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLimpiarActionPerformed
        limpiarDeshabilitar();
    }//GEN-LAST:event_btnLimpiarActionPerformed
    
    private void limpiarDeshabilitar() {
        cldVencimiento.setEnabled(false);
        cldLimiteVencimiento.setEnabled(false);
     }
    
    private void limpiarHabilitar() {

        cldVencimiento.setEnabled(true);
        cldLimiteVencimiento.setEnabled(true);
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnEstablecer;
    private javax.swing.JButton btnLimpiar;
    private com.toedter.calendar.JDateChooser cldLimiteVencimiento;
    private com.toedter.calendar.JDateChooser cldVencimiento;
    private javax.swing.JComboBox cmbProductos;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    // End of variables declaration//GEN-END:variables
}

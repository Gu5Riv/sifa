//Interfaz para agregar nuevos Estantes al sistema
package alemar.interfaz;

import alemar.configuracion.HibernateConexion;
import alemar.entidad.Estante;
import java.util.List;
import javax.swing.JOptionPane;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

public class AgregarEstante extends javax.swing.JPanel {

    //Atributos para el uso de Hibernate
    private static SessionFactory sessionFactory = null;

    public AgregarEstante() {
        initComponents();

        formatCodigoEstante.setToolTipText("Ingrese el código del estante. \n Ejemplo: 01-001-000001");
        cmbUbicacionEstante.setToolTipText("Seleccione la ubicación en bodega");
        cmbNumeroNiveles.setToolTipText("Seleccione el número de niveles");
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnAgregarEstante = new javax.swing.JButton();
        btnLimpiar = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        cmbUbicacionEstante = new javax.swing.JComboBox();
        cmbNumeroNiveles = new javax.swing.JComboBox();
        formatCodigoEstante = new javax.swing.JFormattedTextField();

        setBackground(new java.awt.Color(255, 255, 255));
        setMaximumSize(new java.awt.Dimension(745, 400));
        setMinimumSize(new java.awt.Dimension(745, 400));
        setPreferredSize(new java.awt.Dimension(745, 400));

        btnAgregarEstante.setText("Agregar");
        btnAgregarEstante.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAgregarEstanteActionPerformed(evt);
            }
        });

        btnLimpiar.setText("Limpiar");
        btnLimpiar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLimpiarActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        jLabel1.setText("Agregar nuevo estante al sistema");

        jLabel2.setText("Código del estante");

        jLabel3.setText("Ubicación del estante");

        jLabel4.setText("Número de niveles");

        cmbUbicacionEstante.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "<-- Seleccione una opción -->", "Bodega", "Mostrador" }));

        cmbNumeroNiveles.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15" }));

        formatCodigoEstante.setBackground(new java.awt.Color(238, 253, 253));
        try {
            formatCodigoEstante.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##-###-######")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(213, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(btnLimpiar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnAgregarEstante)
                        .addContainerGap())
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel2)
                            .addComponent(jLabel3)
                            .addComponent(jLabel4))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(cmbUbicacionEstante, 0, 240, Short.MAX_VALUE)
                            .addComponent(cmbNumeroNiveles, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(formatCodigoEstante))
                        .addGap(172, 172, 172))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addGap(245, 245, 245))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(69, 69, 69)
                .addComponent(jLabel1)
                .addGap(60, 60, 60)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(formatCodigoEstante, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(cmbUbicacionEstante, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(cmbNumeroNiveles, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 121, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnAgregarEstante)
                    .addComponent(btnLimpiar))
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnAgregarEstanteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAgregarEstanteActionPerformed
              //Variables que contendran los valores de los campos de texto
        String codigo = "";
        String ubicacion = "";
        String numniveles = "";

        //Variable que maneja la respuesta del JOptionPane
        int conf;

        //Extrayendo la informacion de los campos de texto
        codigo = formatCodigoEstante.getText();
        ubicacion = cmbUbicacionEstante.getSelectedItem().toString();
        numniveles = cmbNumeroNiveles.getSelectedItem().toString();

        int nNiveles;

        //Validaciones
        if (!(codigo.isEmpty() || ubicacion.isEmpty() || 0 == ubicacion.compareTo("<-- Seleccione una opción -->"))) { //Si no esta vacio algun campo           
            //Chunche de Hibernate (Inicio de sesion de hibernate)
            Session session = null;
            nNiveles = Integer.parseInt(numniveles);
            try {
                try {
                    //Conexion a la BD
                    sessionFactory = HibernateConexion.getSessionFactory();
                    //Apertura de la sesion
                    session = sessionFactory.openSession();
                    Transaction tx = session.beginTransaction();

                    //Creando el objeto Proveedor 
                    Estante estante = new Estante();
                    //Ingresando la informacion del proveedor al 
                    //objeto Proveedor
                    estante.setCodigoEstante(codigo);
                    estante.setUbicacion(ubicacion);
                    estante.setNumeroNiveles(nNiveles);

                    //Mensaje de pregunta si quiere guardar los datos proporcionados 
                    conf = JOptionPane.showConfirmDialog(null, "¿Esta seguro de ingresar estos datos?", "Confirmación", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);

                    if (conf == JOptionPane.YES_OPTION) {//Si la opcion fue SI
                        if (!(verificarCodigoEstante(codigo))) {
                            //Guardando el objeto en la base de datos
                            session.save(estante);
                            tx.commit();
                            //Mensaje de confirmacion de insercion Exitosa
                            JOptionPane.showMessageDialog(null, "Registro guardado exitosamente", "Información", JOptionPane.INFORMATION_MESSAGE);

                            formatCodigoEstante.setText("");
                            cmbUbicacionEstante.setSelectedIndex(0);
                            cmbNumeroNiveles.setSelectedIndex(0);
                        } else {
                            JOptionPane.showMessageDialog(null, "Error, Codigo del estante ya existe", "Error en los datos", JOptionPane.ERROR_MESSAGE);
                            //Se resetean los capos de texto y los comboBox
                            formatCodigoEstante.setText("");
                            cmbUbicacionEstante.setSelectedIndex(0);
                            cmbNumeroNiveles.setSelectedIndex(0);
                        }
                    }
                } catch (org.hibernate.exception.ConstraintViolationException cve) {
                    session.getTransaction().rollback();
                    JOptionPane.showMessageDialog(null, "ElCodigo " + codigo + " ya se encuentra asignado a otro Estante previamente registrado\n"
                            + "Ingrese un codigo distinto para el nuevo Estante a registrar", "Duplicidad en registros", JOptionPane.ERROR_MESSAGE);
                    formatCodigoEstante.setText("");
                }
            }
            catch (org.hibernate.JDBCException e) {
                JOptionPane.showMessageDialog(null, "El codigo " + codigo + " no fue agregado debido a un problema de conexión con la base de datos.", "Error de conexión", JOptionPane.ERROR_MESSAGE);
            }
            finally {
                //Finalizacion de la sesion de hibernate
                session.close();
            }
        } else {//Si hay campos vacios, mensaje de error
            JOptionPane.showMessageDialog(null, "Error, no pueden haber campos vacios", "Error en los datos", JOptionPane.ERROR_MESSAGE);
        }

    }//GEN-LAST:event_btnAgregarEstanteActionPerformed

    private void btnLimpiarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLimpiarActionPerformed

        //Se resetean los capos de texto y los comboBox
        formatCodigoEstante.setText("");
        cmbUbicacionEstante.setSelectedIndex(0);
        cmbNumeroNiveles.setSelectedIndex(0);

    }//GEN-LAST:event_btnLimpiarActionPerformed

    //Metodo para verificar la existencia de un proveedor
    private boolean verificarCodigoEstante(String codigo) {

        //Variable que contendra la lista de proveedores
        List<Estante> listaEstante = null;

        boolean resultado;

        //Chunche de Hibernate (Inicio de sesion de hibernate)
        Session session = null;
        try {
            try {
                //Conexion a la BD
                sessionFactory = HibernateConexion.getSessionFactory();
                //Apertura de la sesion
                session = sessionFactory.openSession();
                Transaction tx = session.beginTransaction();

                //Obteniendo de la BD todos los estantes                
                listaEstante = session.createQuery("from Estante").list();

                //Verificando los codigos de los estantes
                for (Estante e : listaEstante) {
                    if (codigo.equals(e.getCodigoEstante())) {
                        return resultado = true;
                    }

                }

            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        } finally {
            //Finalizacion de la sesion de hibernate
            session.close();
        }

        return resultado = false;
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAgregarEstante;
    private javax.swing.JButton btnLimpiar;
    private javax.swing.JComboBox cmbNumeroNiveles;
    private javax.swing.JComboBox cmbUbicacionEstante;
    private javax.swing.JFormattedTextField formatCodigoEstante;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    // End of variables declaration//GEN-END:variables
}

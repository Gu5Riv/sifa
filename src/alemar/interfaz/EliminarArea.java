//Interfaz que se encarga de Eliminar los datos de un Area registrada en el sistema
package alemar.interfaz;
import alemar.clases.CargarComboArea;
import alemar.configuracion.HibernateConexion;
import alemar.entidad.Area;
import java.util.List;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

/**
 *
 * @author Teto
 */
public class EliminarArea extends javax.swing.JPanel {

    private static SessionFactory sessionFactory = null;
    List<Area> listaArea = null;
    
    public EliminarArea() {
        initComponents();
        cargarComboBox();
    }
    
   
    
    private void cargarComboBox() {
        //Se inicializa la lista a null
        listaArea = null;

        //Instanciamos la clase CargarCMBUsuario para cargar todos los usuarios registrados en una lista
        CargarComboArea cargarArea = new CargarComboArea();
        listaArea = cargarArea.cargarArea();

        //Creando un modelo de comboBox                
        DefaultComboBoxModel modeloCombo = new DefaultComboBoxModel();

        //Poniendole un mensaje de seleccion (Primer elemento) al combo box
        modeloCombo.addElement("<--Seleccione un usuario-->");

        //Llenando el modelo de comboBox con los nombres de los usuarios
        for (Area area : listaArea) {
           modeloCombo.addElement(area.getNombreArea()); 
        }
        //Agregando el modelo del comboBox al comboBox del panel
        cmbNombreArea.setModel(modeloCombo);
    }


    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        cmbNombreArea = new javax.swing.JComboBox();
        jLabel2 = new javax.swing.JLabel();
        btnEliminarArea = new javax.swing.JButton();

        setBackground(new java.awt.Color(255, 255, 255));
        setMaximumSize(new java.awt.Dimension(745, 400));
        setMinimumSize(new java.awt.Dimension(745, 400));
        setPreferredSize(new java.awt.Dimension(745, 400));

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Eliminar área del sistema");

        cmbNombreArea.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel2.setText("Nombre de área");

        btnEliminarArea.setText("Eliminar");
        btnEliminarArea.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEliminarAreaActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(190, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(btnEliminarArea)
                        .addContainerGap())
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGap(18, 18, 18)
                                .addComponent(cmbNombreArea, javax.swing.GroupLayout.PREFERRED_SIZE, 240, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 366, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(189, 189, 189))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(95, 95, 95)
                .addComponent(jLabel1)
                .addGap(49, 49, 49)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cmbNombreArea, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 182, Short.MAX_VALUE)
                .addComponent(btnEliminarArea)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnEliminarAreaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEliminarAreaActionPerformed
        
        String nombreArea = "";
        int conf;
        nombreArea = cmbNombreArea.getSelectedItem().toString();
        
        if(nombreArea.equals("<--Seleccione una area de producto-->")){
            } else { //Se elmina el usuario
            //Inicio de sesion en Hibernate
            //Chunche de Hibernate (Inicio de sesion de hibernate)
            Session session = null;
            //Cadena de eliminacion de HQL
            String hql = "delete alemar.entidad.Area a where a.nombreArea = :nombreArea";
            try{
                try{
                    //Conexion a la BD
                    sessionFactory = HibernateConexion.getSessionFactory();
                    //Apertura de la sesion
                    session = sessionFactory.openSession();
                    Transaction txt = session.beginTransaction();
                    //Mensaje de pregunta si quiere eliminar el registro 
                    conf=JOptionPane.showConfirmDialog(null,"¿Esta seguro que quiere eliminar esta Area de Producto?\n" + nombreArea,"Confirmación",JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE);
                    
                    if(conf == JOptionPane.YES_OPTION){//Si la opcion fue SI Eliminacion del proveedor de la BD en base al nombre
                        int resultado = session.createQuery(hql).setString("nombreArea",nombreArea).executeUpdate();
                        txt.commit();
                        session.close();
                        //Mensaje de confirmacion de insercion Exitosa
                        JOptionPane.showMessageDialog(null, "El area " + nombreArea + " ha sido eliminado exitosamente", "Información", JOptionPane.INFORMATION_MESSAGE);

                        //cmbNombreArea.setSelectedIndex(0);
                    }
                } catch (org.hibernate.exception.ConstraintViolationException cve) {
                    JOptionPane.showMessageDialog(null, "Error: No se puede Eliminar " + nombreArea + " Porque contiene Productos en Bodega ", "Error de conexión", JOptionPane.ERROR_MESSAGE);
                }
            } catch (org.hibernate.JDBCException e) {
                JOptionPane.showMessageDialog(null, "El Area:  " + nombreArea + " no puede ser eliminada,  debido a un problema de conexión con la base de datos.", "Error de conexión", JOptionPane.ERROR_MESSAGE);
            }
            finally {
                //Finalizacion de la sesion de hibernate
                //session.close();
                //Se carga nuevamente el comboBox
                cargarComboBox();
               // cmbNombreArea.setSelectedIndex(0);
           }
      
        }
    }//GEN-LAST:event_btnEliminarAreaActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnEliminarArea;
    private javax.swing.JComboBox cmbNombreArea;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    // End of variables declaration//GEN-END:variables
}

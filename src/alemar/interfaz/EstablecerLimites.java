
package alemar.interfaz;


import alemar.configuracion.HibernateConexion;
import alemar.entidad.Producto;
import java.awt.Color;
import java.util.List;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;
import org.hibernate.*;
/**
 *
 * @author Gus
 */
public class EstablecerLimites extends javax.swing.JPanel {

    
    //Atributos para el uso de Hibernate
    private static SessionFactory sessionFactory = null;
    
    //Para guardar el id del proveedor
    //private String id;
    private int id;
    //private String nom;
    

    public EstablecerLimites() {
        initComponents();
        
        //Se carga el comboBox Dinamicamente de la BD
        cargarComboBox();
        
        //Por default estaran desabilitados por si quiere ingresar datos
        //sin haber seleccionado un producto
        txtCantidadActual.setEnabled(false);
        txtLimiteMinimo.setEnabled(false);
        
        txtCantidadActual.setEditable(false);
        txtLimiteMinimo.setEditable(false);
        
        //Agregando mensajes de ayuda para el llenado de los campos
        txtCantidadActual.setToolTipText("Cantidad actual del producto seleccionado");
        txtLimiteMinimo.setToolTipText("Digite el nivel mínimo del producto");
    }
    
    
    
        //Metodo para cargar el comboBox dinamicamente de la BD
    private void cargarComboBox(){
        
        //Obtencion de todos los productos de la base de datos
        //para llenar el comboBox de manera dinamica
        
        //Variable que contendra la lista de productos
        List<Producto> listaProducto = null;
        
        //Inicializacion de session de hibernate
        Session session = null;
        try{
            try{
                //Chunces que ocupa Hibernate
                sessionFactory = HibernateConexion.getSessionFactory();
                session = sessionFactory.openSession();
                Transaction tx = session.beginTransaction();
                
                //Obteniendo de la BD todos los proveedores                
                listaProducto = session.createQuery("from Producto").list();
                
                //Creando un modelo de comboBox                
                DefaultComboBoxModel modeloCombo = new DefaultComboBoxModel();
                
                //Poniendole un mensaje de seleccion (Primer elemento)
                 modeloCombo.addElement("<--Seleccione un Producto-->");
                
                //Llenando el modelo de comboBox con los nombres de los proveedores
                for(Producto p : listaProducto) 
                { 
                    modeloCombo.addElement(p.getNombreProducto());
                }
                
                //Agregando el modelo del comboBox al comboBox del panel
                cmbProductos.setModel(modeloCombo);
                
            }catch(Exception e){
                    System.out.println(e.getMessage());
                }
            }finally {
                session.close();
        }
        
    }
    
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        txtCantidadActual = new javax.swing.JTextField();
        txtLimiteMinimo = new javax.swing.JTextField();
        btnEstablecer = new javax.swing.JButton();
        btnLimpiar = new javax.swing.JButton();
        cmbProductos = new javax.swing.JComboBox();

        setMaximumSize(new java.awt.Dimension(745, 400));
        setMinimumSize(new java.awt.Dimension(745, 400));
        setPreferredSize(new java.awt.Dimension(745, 400));

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        jLabel1.setText("Establecer límites mínimos de productos");

        jLabel2.setText("Cantidad Actual");

        jLabel3.setText("Límite Mínimo");

        txtLimiteMinimo.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                txtLimiteMinimoMouseClicked(evt);
            }
        });
        txtLimiteMinimo.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtLimiteMinimoKeyTyped(evt);
            }
        });

        btnEstablecer.setText("Establecer");
        btnEstablecer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEstablecerActionPerformed(evt);
            }
        });

        btnLimpiar.setText("Limpiar");
        btnLimpiar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLimpiarActionPerformed(evt);
            }
        });

        cmbProductos.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        cmbProductos.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbProductosItemStateChanged(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(239, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(btnLimpiar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnEstablecer)
                        .addContainerGap())
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addGap(212, 212, 212))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(cmbProductos, javax.swing.GroupLayout.PREFERRED_SIZE, 245, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(234, 234, 234))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addGap(18, 18, 18)
                        .addComponent(txtCantidadActual, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(22, 22, 22)
                        .addComponent(jLabel3)
                        .addGap(18, 18, 18)
                        .addComponent(txtLimiteMinimo, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(202, 202, 202))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(67, 67, 67)
                .addComponent(jLabel1)
                .addGap(72, 72, 72)
                .addComponent(cmbProductos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(84, 84, 84)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(jLabel3)
                    .addComponent(txtCantidadActual, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtLimiteMinimo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 117, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnEstablecer)
                    .addComponent(btnLimpiar))
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void cmbProductosItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbProductosItemStateChanged
        
        String nombreProducto = "";
        
        List<Producto> NProducto = null;
        
        //Se extrae el proveedor seleccionado
        nombreProducto = cmbProductos.getSelectedItem().toString();
        
        if(!(nombreProducto.equalsIgnoreCase("<<--Seleccione un Producto-->"))){
                        
            //Chunche de Hibernate (Inicio de sesion de hibernate)
            Session session = null;
            //Cadena de seleccion de HQL
            String hql = "FROM alemar.entidad.Producto AS p WHERE p.nombreProducto = :nombreProducto";
            try{
                try{
                    //Conexion a la BD
                    sessionFactory = HibernateConexion.getSessionFactory();
                    //Apertura de la sesion
                    session = sessionFactory.openSession();
                    Transaction txt = session.beginTransaction();                                       
                    
                    //Extraccion de los datos del proveedor de la BD en base al nombre proporcionado                    
                    Query query = session.createQuery(hql).setString("nombreProducto", nombreProducto);
                    
                    //Se pone en una lista el registro sacado
                    NProducto = query.list();
                    
                    //Guardando el id del producto en una variable global
                    //id = NProducto.get(0).getNombreProducto();
                    id = NProducto.get(0).getIdProducto();
                    
                    //Guardando el nombre del producto en una variable global
                    String nomProd = NProducto.get(0).getNombreProducto();
                    
                    limpiarHabilitar();
                    
                    //Se ponen los valores en los campos de texto correspondientes
                    //para que se modifiquen
                    String SCantidadActual = Integer.toString(NProducto.get(0).getCantidad());
                    txtCantidadActual.setText(SCantidadActual);
                    String SLimiteProducto = Integer.toString(NProducto.get(0).getLimiteProducto());
                    txtLimiteMinimo.setText(SLimiteProducto);
                                    
                    //Se ponen los valores en los campos de texto correspondientes
                    //para que se modifiquen
                    //txtLimiteMinimo.setEnabled(true);
                    //txtLimiteMinimo.setEditable(true);
                    
                    txt.commit();
                    
                    }catch(Exception e){
                    System.out.println(e.getMessage());
                }
            } finally {
                //Finalizacion de la sesion de hibernate
                session.close();
                }
        }else{
            txtCantidadActual.setEnabled(false);
            txtLimiteMinimo.setEnabled(false);
            
            cmbProductos.setSelectedIndex(0);

        }
    }//GEN-LAST:event_cmbProductosItemStateChanged

    private void btnEstablecerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEstablecerActionPerformed
        
        //Variables que contendran los valores de los campos de texto 
        String cantidadActual = "";         
        String limiteMinimo = "";
        
        //Variable que maneja la respuesta del JOptionpane
        int conf;
             
        //Extrayendo la informacion de los campos de texto
        cantidadActual = txtCantidadActual.getText();
        limiteMinimo = txtLimiteMinimo.getText();
        
        //Validaciones
        if(!(0 == cantidadActual.compareTo("<--Seleccione un Estante-->"))){ //Si no esta vacio algun campo           
            //Chunche de Hibernate (Inicio de sesion de hibernate)
            Session session = null;
            try{
                try{
                    //Conexion a la BD
                    sessionFactory = HibernateConexion.getSessionFactory();
                    //Apertura de la sesion
                    session = sessionFactory.openSession();
                    Transaction tx = session.beginTransaction();                    
                    
                    //Extrayendo el regitro del producto seleccionado
                    //en base al ID guardado previamente de manera global
                    Producto prod = (Producto)session.get(Producto.class, id);                    
                    
                    //Ingresando la nueva informacion del producto al 
                    //objeto Producto
                    
                    int cantidadActualINT = Integer.parseInt(cantidadActual);
                    int limiteMinimoINT = Integer.parseInt(limiteMinimo);
                    
                    prod.setCantidad(cantidadActualINT);
                    prod.setLimiteProducto(limiteMinimoINT);
                                      
                    //Mensaje de pregunta si quiere guardar los datos proporcionados 
                    conf=JOptionPane.showConfirmDialog(null,"¿Esta seguro de ingresar esos datos?","Confirmación",JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE);
                    if(conf == JOptionPane.YES_OPTION){//Si la opcion fue SI
                        
                        //Actualiza los datos del proveedor
                        session.update(prod);
                       
                        tx.commit();
                        //Mensaje de confirmacion de insercion Exitosa
                        JOptionPane.showMessageDialog(null,"Registro guardado exitosamente","Información",JOptionPane.INFORMATION_MESSAGE);
                        
                        //Se resetean los campos de texto
                            limpiarDeshabilitar();
              
                        cmbProductos.setSelectedIndex(0);

                    }
                    else{
                        cmbProductos.setSelectedIndex(0);
                    }
                }catch(Exception e){
                    System.out.println(e.getMessage());
                }
            } finally {
                //Finalizacion de la sesion de hibernate
                session.close();
                //Se vuelve a cargar el comboBox
                cargarComboBox();
        }
      }
        else {//Si hay campos vacios, mensaje de error
            JOptionPane.showMessageDialog(null,"Error, no pueden haber campos vacios","Error en los datos",JOptionPane.ERROR_MESSAGE);
            
        } 
      
    }//GEN-LAST:event_btnEstablecerActionPerformed

    private void txtLimiteMinimoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtLimiteMinimoKeyTyped
        // SOLO NUMEROS
        char c;
        //capturar el caracter digitado
        c = evt.getKeyChar();
        if (c < '0' || c > '9') {
            evt.consume();//ignora el caracter digitado 
        }
    }//GEN-LAST:event_txtLimiteMinimoKeyTyped

    private void txtLimiteMinimoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtLimiteMinimoMouseClicked
        txtLimiteMinimo.setText("");
        txtLimiteMinimo.setForeground(Color.BLACK);
    }//GEN-LAST:event_txtLimiteMinimoMouseClicked

    private void btnLimpiarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLimpiarActionPerformed
        limpiarDeshabilitar();
    }//GEN-LAST:event_btnLimpiarActionPerformed

    //Metodo para verificar la existencia de un producto
//    private boolean verificarNombreProducto(String nombre) {
//
//        //Variable que contendra la lista de proveedores
//        List<Producto> listaProducto = null;
//
//        boolean resultado;
//        
//        //Chunche de Hibernate (Inicio de sesion de hibernate)
//        Session session = null;
//        try {
//            try {
//                //Conexion a la BD
//                sessionFactory = HibernateConexion.getSessionFactory();
//                //Apertura de la sesion
//                session = sessionFactory.openSession();
//                Transaction tx = session.beginTransaction();
//
//                //Creando el objeto Proveedor 
//                Producto producto = new Producto();
//                //Ingresando la informacion del proveedor al 
//                //Obteniendo de la BD todos los proveedores                
//                listaProducto = session.createQuery("from Producto").list();
//
//                //Verificando los nombre de los proveedores
//                for (Producto p : listaProducto) {
//                    if (nombre.equals(p.getNombreProducto()) && nombre.equals(nom)) {
//                        return resultado = false;
//                    }
//                }
//
//            } catch (Exception e) {
//                System.out.println(e.getMessage());
//            }
//        } finally {
//            //Finalizacion de la sesion de hibernate
//            session.close();
//        }
//
//        return resultado = true;
//    }
    
    private void limpiarDeshabilitar() {
        txtCantidadActual.setText("");
        txtCantidadActual.setForeground(Color.LIGHT_GRAY);
        txtCantidadActual.setEnabled(false);
        txtCantidadActual.setEditable(false);
        
        txtLimiteMinimo.setText("");
        txtLimiteMinimo.setForeground(Color.LIGHT_GRAY);
        txtLimiteMinimo.setEnabled(false);
        txtLimiteMinimo.setEditable(false);
    }
    
    private void limpiarHabilitar() {

        txtCantidadActual.setText("");
        txtCantidadActual.setForeground(Color.BLACK);

        txtLimiteMinimo.setText("");
        txtLimiteMinimo.setForeground(Color.BLACK);

        //Deshabilitando edicion
        txtLimiteMinimo.setEditable(true);
        txtLimiteMinimo.setEnabled(true);

    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnEstablecer;
    private javax.swing.JButton btnLimpiar;
    private javax.swing.JComboBox cmbProductos;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JTextField txtCantidadActual;
    private javax.swing.JTextField txtLimiteMinimo;
    // End of variables declaration//GEN-END:variables
}

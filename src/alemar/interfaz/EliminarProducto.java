//Interfaz que se encarga de eliminar un Producto  registrado en el sistema
package alemar.interfaz;

import alemar.configuracion.HibernateConexion;
import alemar.entidad.Area;
import alemar.entidad.Producto;
import java.util.List;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

/**
 *
 * @author Teto
 */
public class EliminarProducto extends javax.swing.JPanel {

    //Atributos para el uso de Hibernate
    private static SessionFactory sessionFactory = null;
   
    //Atributo que guarda el id del producto al cual se hara referencia
    int idProducto;
    
    public EliminarProducto() {
        initComponents();
        cargarComboBoxProducto();
    }
    
    //Carga el comboBox con el nombre de las areas
    //de los productos a asignar
    private void cargarComboBoxProducto() {

        //Obtencion de todos las areas de la base de datos
        //para llenar el comboBox de manera dinamica

        //Variable que contendra la lista de las areas
        List<Producto> listaProducto;

        //Inicializacion de session de hibernate
        Session session = null;

        try {
            //Chunces que ocupa Hibernate
            sessionFactory = HibernateConexion.getSessionFactory();
            session = sessionFactory.openSession();
            session.beginTransaction();

            //Obteniendo de la BD todos los nombres de los productos
            //registrados en el sistema
            listaProducto = session.createQuery("from Producto").list();

            //Creando un modelo de comboBox                
            DefaultComboBoxModel modeloCombo = new DefaultComboBoxModel();

            //Poniendole un mensaje de seleccion (Pirmer elemento)
            modeloCombo.addElement("<--Seleccione un producto-->");

            //Llenando el modelo de comboBox con los nombres de los productos
            for (Producto p : listaProducto) {
                modeloCombo.addElement(p.getNombreProducto());
            }

            //Agregando el modelo del comboBox al comboBox del panel
            cmbNombreProducto.setModel(modeloCombo);

        } catch (org.hibernate.JDBCException e) {
            JOptionPane.showMessageDialog(null, "No se puede establecer conexión con la base de datos.", "Error de conexión", JOptionPane.ERROR_MESSAGE);
        }

        //Cierre de sesion de Hibernate
        session.close();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        cmbNombreProducto = new javax.swing.JComboBox();
        jButton1 = new javax.swing.JButton();

        setBackground(new java.awt.Color(255, 255, 255));
        setMaximumSize(new java.awt.Dimension(794, 432));
        setMinimumSize(new java.awt.Dimension(794, 432));
        setPreferredSize(new java.awt.Dimension(794, 432));

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        jLabel1.setText("Eliminar producto del sistema");

        jLabel2.setText("Nombre del producto");

        cmbNombreProducto.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        jButton1.setText("Eliminar");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(222, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jButton1)
                        .addContainerGap())
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addGap(18, 18, 18)
                        .addComponent(cmbNombreProducto, javax.swing.GroupLayout.PREFERRED_SIZE, 240, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(214, 214, 214))))
            .addGroup(layout.createSequentialGroup()
                .addGap(289, 289, 289)
                .addComponent(jLabel1)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(94, 94, 94)
                .addComponent(jLabel1)
                .addGap(76, 76, 76)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(cmbNombreProducto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 188, Short.MAX_VALUE)
                .addComponent(jButton1)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
       
        String nombreProducto;
        int conf;
        nombreProducto = cmbNombreProducto.getSelectedItem().toString();
        
        if(!(nombreProducto.equals("<--Seleccione un producto-->"))){
           
            //Chunche de Hibernate (Inicio de sesion de hibernate)
            Session session = null;
            //Cadena de eliminacion de HQL
            String hql = "delete alemar.entidad.Producto a where a.nombreProducto = :nombreProducto";
            try{
                try{
                    //Conexion a la BD
                    sessionFactory = HibernateConexion.getSessionFactory();
                    //Apertura de la sesion
                    session = sessionFactory.openSession();
                    Transaction txt = session.beginTransaction();
                    //Mensaje de pregunta si quiere eliminar el registro 
                    conf=JOptionPane.showConfirmDialog(null,"¿Esta seguro que quiere eliminar este producto?\n" + nombreProducto,"Confirmación",JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE);
                    
                    if(conf == JOptionPane.YES_OPTION){//Si la opcion fue SI Eliminacion del proveedor de la BD en base al nombre
                        int resultado = session.createQuery(hql).setString("nombreProducto",nombreProducto).executeUpdate();
                        txt.commit();
                        //Mensaje de confirmacion de insercion Exitosa
                        JOptionPane.showMessageDialog(null,"El producto "+ nombreProducto +" ha sido eliminado exitosamente","Información",JOptionPane.INFORMATION_MESSAGE);
                        cmbNombreProducto.setSelectedIndex(0);
                     }
                    }
                catch(Exception e)
                {
                    System.out.println(e.getMessage());
                }
            } finally 
            {
                //Finalizacion de la sesion de hibernate
                session.close();
                //Se carga nuevamente el comboBox
                cargarComboBoxProducto();
                cmbNombreProducto.setSelectedIndex(0);
           }
      } else{
        JOptionPane.showMessageDialog(null,"Seleccione un producto","Selección de datos",JOptionPane.ERROR_MESSAGE);
        cmbNombreProducto.setSelectedIndex(0);
       }
        
    }//GEN-LAST:event_jButton1ActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox cmbNombreProducto;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    // End of variables declaration//GEN-END:variables
}

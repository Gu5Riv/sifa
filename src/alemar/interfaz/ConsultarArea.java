
package alemar.interfaz;

import alemar.clases.CargarComboArea;
import alemar.configuracion.HibernateConexion;
import alemar.entidad.Area;
import java.util.List;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

/**
 *
 * @author Teto
 */
public class ConsultarArea extends javax.swing.JPanel {

    //Atributos para el uso de Hibernate
    private static SessionFactory sessionFactory = null;
    List<Area> listaArea = null;
    
    public ConsultarArea() {
        initComponents();        
        cargarComboBox();
    }
    
       private void cargarComboBox() {
        //Se inicializa la lista a null
        listaArea = null;

        //Instanciamos la clase CargarCMBUsuario para cargar todos los usuarios registrados en una lista
        CargarComboArea cargarArea = new CargarComboArea();
        listaArea = cargarArea.cargarArea();

        //Creando un modelo de comboBox                
        DefaultComboBoxModel modeloCombo = new DefaultComboBoxModel();

        //Poniendole un mensaje de seleccion (Primer elemento) al combo box
        modeloCombo.addElement("<--Seleccione un usuario-->");

        //Llenando el modelo de comboBox con los nombres de los usuarios
        for (Area area : listaArea) {
           modeloCombo.addElement(area.getNombreArea()); 
        }
        //Agregando el modelo del comboBox al comboBox del panel
        cmbNombreArea.setModel(modeloCombo);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        cmbNombreArea = new javax.swing.JComboBox();
        jLabel2 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        jLabel3 = new javax.swing.JLabel();
        btnConsultarArea = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtIdentificacionArea = new javax.swing.JTextArea();

        setBackground(new java.awt.Color(255, 255, 255));
        setMaximumSize(new java.awt.Dimension(745, 400));
        setMinimumSize(new java.awt.Dimension(745, 400));
        setPreferredSize(new java.awt.Dimension(745, 400));

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Consultar datos de área");

        cmbNombreArea.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel2.setText("Nombre de área");

        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel3.setText("Descripción");

        btnConsultarArea.setText("Consultar");
        btnConsultarArea.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnConsultarAreaActionPerformed(evt);
            }
        });

        txtIdentificacionArea.setEditable(false);
        txtIdentificacionArea.setColumns(20);
        txtIdentificacionArea.setRows(5);
        jScrollPane1.setViewportView(txtIdentificacionArea);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(btnConsultarArea)
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 237, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(3, 3, 3))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(0, 190, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGap(18, 18, 18)
                                .addComponent(cmbNombreArea, javax.swing.GroupLayout.PREFERRED_SIZE, 240, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 366, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(189, 189, 189))
            .addGroup(layout.createSequentialGroup()
                .addGap(108, 108, 108)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 528, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(82, 82, 82)
                .addComponent(jLabel1)
                .addGap(47, 47, 47)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cmbNombreArea, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addGap(18, 18, 18)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(55, 55, 55)
                        .addComponent(btnConsultarArea))
                    .addComponent(jLabel3))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnConsultarAreaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnConsultarAreaActionPerformed
        // TODO add your handling code here:
        
        String nombreArea = "";
        
        //List<Area> area = null;
        
        //Se extrae el nombre del proveedor seleccionado
        nombreArea = cmbNombreArea.getSelectedItem().toString();
        
        if(nombreArea.equalsIgnoreCase("<--Seleccione un Area de productos-->")){
            JOptionPane.showMessageDialog(null,"Seleccione un nombre de Area de Productos","Selección de datos",JOptionPane.ERROR_MESSAGE);
        }
        else{
            
            //Chunche de Hibernate (Inicio de sesion de hibernate)
            Session session = null;
            //Cadena de seleccion de HQL
            String hql = "FROM alemar.entidad.Area AS a WHERE a.nombreArea = :nombreArea";
            try{
                try{
                    //Conexion a la BD
                    sessionFactory = HibernateConexion.getSessionFactory();
                    //Apertura de la sesion
                    session = sessionFactory.openSession();
                    Transaction txt = session.beginTransaction();                                       
                    
                    //Extraccion de los datos del proveedor de la BD                    
                    Query query = session.createQuery(hql).setString("nombreArea", nombreArea);
                    
                    //Se extrae los registros del proveedor
                    listaArea = query.list();
                    
                    //Se ponen los datos del proveedir en los campos de texto
                    //para su respectiva consulta
                                       
                    txtIdentificacionArea.setText(listaArea.get(0).getIdentificacion());
                    txt.commit();
                    session.close();
                     
                    }catch (org.hibernate.JDBCException e) {
                JOptionPane.showMessageDialog(null, "No se puede establecer conexión con la base de datos.", "Error de conexión", JOptionPane.ERROR_MESSAGE);

                }
            } finally {
                //Finalizacion de la sesion de hibernate
                
                }
        }
    
        
    }//GEN-LAST:event_btnConsultarAreaActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnConsultarArea;
    private javax.swing.JComboBox cmbNombreArea;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JTextArea txtIdentificacionArea;
    // End of variables declaration//GEN-END:variables
}

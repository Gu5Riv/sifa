//Interfaz para agregar nuevos usuarios al sistema
package alemar.interfaz;

import alemar.clases.CifradorCesar;
import alemar.configuracion.HibernateConexion;
import alemar.entidad.Usuario;
import java.awt.Color;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JOptionPane;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

public class AgregarUsuario extends javax.swing.JPanel {

    //Atributos de la clase
    //Atributo para el uso de Hibernate
    private static SessionFactory sessionFactory = null;
    //Variables que contienen el limite minimo y maximo para el nombre del usuario y la contraseña
    private int limMinimo = 6;
    private int limMaximo = 20;
    //Variable que contendra el estado del bloc mayuscula
    private boolean estadoBM;

    public AgregarUsuario() {
        initComponents();

        //Agregar mensajes de ayuda para el llenado de los campos
        txtNombreUsuario.setToolTipText("Ingrese el nombre del nuevo usuario (Min. 6 caracteres). Unicámente letras, numeros y simbolos (@,$,<,>,_,-)");
        txtContraseña.setToolTipText("Ingrese la contraseña del usuario (Min. 6 caracteres). Unicámente letras, numeros y simbolos (@,$,<,>,_,-)");
        txtConfirmarContraseña.setToolTipText("Repita la contraseña del usuario");
        txtNombreUsuario.setText("Ej: Admin-1");
        txtNombreUsuario.setForeground(Color.LIGHT_GRAY);

        //Ocultamos el label que indica si la contraseña esta o no en mayuscula
        jLabel8.setVisible(false);

        //Limitamos el numero maximo de caracteres para el nombre de usuario
        txtNombreUsuario.addKeyListener(new KeyListener() {
            public void keyTyped(KeyEvent e) {
                if (txtNombreUsuario.getText().length() == limMaximo) {
                    e.consume();
                }
            }

            public void keyPressed(KeyEvent arg0) {
            }

            public void keyReleased(KeyEvent arg0) {
            }
        });

        //Limitamos el numero maximo de caracteres para la contraseña y a su vez comprobamos el nivel de seguridad de la misma
        txtContraseña.addKeyListener(new KeyListener() {
            public void keyTyped(KeyEvent e) {
                //Se elimina el teclado al llegar al valor maximo 
                if (txtContraseña.getText().length() == limMaximo) {
                    e.consume();
                }
            }

            public void keyPressed(KeyEvent arg0) {
                //Verificamos si el Bloq Mayus esta activado
                estadoBM = Toolkit.getDefaultToolkit().getLockingKeyState(KeyEvent.VK_CAPS_LOCK);

                //Si esta activo mostramos el mensaje
                if (estadoBM == true) {
                    jLabel8.setVisible(true);
                } else {
                    jLabel8.setVisible(false);
                }
            }

            public void keyReleased(KeyEvent arg0) {
                //Verificar el nivel de seguriad de la contraseña

                //Si la contraseña esta vacia
                if (txtContraseña.getText().length() == 0) {
                    jLabel6.setText("");
                    barraContraseña.setValue(0); //Poner el porcentaje de la barra en 0
                }
                //Si la contraseña tiene un valor menor a 6 caracteres
                if (txtContraseña.getText().length() > 0 && txtContraseña.getText().length() <= 5) {
                    jLabel6.setText("Inválida");
                    barraContraseña.setValue(0); //Poner el porcentaje de la barra en 0
                }
                //Si la contraseña tiene un valor entre 6 y 9 caracteres
                if (txtContraseña.getText().length() > 5 && txtContraseña.getText().length() <= 9) {
                    jLabel6.setText("Baja");
                    barraContraseña.setValue(33); //Poner el porcentaje de la barra en 0
                }
                //Si la contraseña tiene un valor entre 10 y 14 caracteres
                if (txtContraseña.getText().length() >= 10 && txtContraseña.getText().length() <= 14) {
                    jLabel6.setText("Media");
                    barraContraseña.setValue(66); //Poner el porcentaje de la barra en 0
                }
                //Si la contraseña tiene un valor mayor a 15 caracteres
                if (txtContraseña.getText().length() >= 15) {
                    jLabel6.setText("Alta");
                    barraContraseña.setValue(100); //Poner el porcentaje de la barra en 0
                }
            }
        });

        //Limitamos el numero maximo de caracteres para la confirmación de la contraseña
        txtConfirmarContraseña.addKeyListener(new KeyListener() {
            public void keyTyped(KeyEvent e) {
                if (txtConfirmarContraseña.getText().length() == limMaximo) {
                    e.consume();
                }
            }

            public void keyPressed(KeyEvent arg0) {
                //Verificamos si el Bloq Mayus esta activado
                estadoBM = Toolkit.getDefaultToolkit().getLockingKeyState(KeyEvent.VK_CAPS_LOCK);

                //Si esta activo mostramos el mensaje
                if (estadoBM == true) {
                    jLabel8.setVisible(true);
                } else {
                    jLabel8.setVisible(false);
                }
            }

            public void keyReleased(KeyEvent arg0) {
            }
        });
    }

    //Metodos de la clase
   
    //Metodo que limpia todos los campos de la interfaz
    private void limpiarCampos() {
        txtNombreUsuario.setText("Ej: Admin-1");
        txtNombreUsuario.setForeground(Color.LIGHT_GRAY);
        txtContraseña.setText("");
        txtConfirmarContraseña.setText("");
        cmbTipoUsuario.setSelectedIndex(0);
        barraContraseña.setValue(0);
        jLabel6.setText("");
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnAgregarUsuario = new javax.swing.JButton();
        btnLimpiarUsuario = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        cmbTipoUsuario = new javax.swing.JComboBox();
        jLabel5 = new javax.swing.JLabel();
        txtConfirmarContraseña = new javax.swing.JPasswordField();
        txtNombreUsuario = new javax.swing.JTextField();
        barraContraseña = new javax.swing.JProgressBar();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        txtContraseña = new javax.swing.JPasswordField();
        jLabel8 = new javax.swing.JLabel();

        setBackground(new java.awt.Color(255, 255, 255));
        setMaximumSize(new java.awt.Dimension(794, 432));
        setMinimumSize(new java.awt.Dimension(794, 432));
        setPreferredSize(new java.awt.Dimension(794, 432));

        btnAgregarUsuario.setText("Agregar");
        btnAgregarUsuario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAgregarUsuarioActionPerformed(evt);
            }
        });

        btnLimpiarUsuario.setText("Limpiar");
        btnLimpiarUsuario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLimpiarUsuarioActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Agregar nuevo usuario al sistema");

        jLabel2.setText("Nombre de usuario");

        jLabel3.setText("Contraseña");

        jLabel4.setText("Confirmar contraseña");

        cmbTipoUsuario.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "<--Seleccione un usuario-->", "Administrador", "Gerente de ventas", "Encargado de bodega", "Vendedor", "Cajero" }));

        jLabel5.setText("Rol");

        txtConfirmarContraseña.setBackground(new java.awt.Color(238, 253, 253));
        txtConfirmarContraseña.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtConfirmarContraseñaFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtConfirmarContraseñaFocusLost(evt);
            }
        });
        txtConfirmarContraseña.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtConfirmarContraseñaKeyTyped(evt);
            }
        });

        txtNombreUsuario.setBackground(new java.awt.Color(238, 253, 253));
        txtNombreUsuario.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtNombreUsuarioFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtNombreUsuarioFocusLost(evt);
            }
        });
        txtNombreUsuario.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtNombreUsuarioKeyTyped(evt);
            }
        });

        jLabel6.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        jLabel7.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel7.setText("Nivel de seguridad");

        txtContraseña.setBackground(new java.awt.Color(238, 253, 253));
        txtContraseña.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtContraseñaFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtContraseñaFocusLost(evt);
            }
        });
        txtContraseña.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtContraseñaKeyTyped(evt);
            }
        });

        jLabel8.setText("Bloq. Mayus. Activado");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(24, 24, 24)
                        .addComponent(jLabel8)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnLimpiarUsuario)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnAgregarUsuario))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addGap(134, 134, 134)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel3)
                                    .addComponent(jLabel2)
                                    .addComponent(jLabel4)
                                    .addComponent(jLabel5))
                                .addGap(30, 30, 30)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txtNombreUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, 240, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txtConfirmarContraseña, javax.swing.GroupLayout.PREFERRED_SIZE, 240, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(cmbTipoUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, 240, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(txtContraseña, javax.swing.GroupLayout.PREFERRED_SIZE, 240, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                            .addComponent(jLabel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addComponent(barraContraseña, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                                            .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 134, javax.swing.GroupLayout.PREFERRED_SIZE))))
                                .addGap(122, 122, 122)))))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(87, 87, 87)
                .addComponent(jLabel1)
                .addGap(51, 51, 51)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtNombreUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel2))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtContraseña, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel3)))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(barraContraseña, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(4, 4, 4)
                .addComponent(jLabel7)
                .addGap(1, 1, 1)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtConfirmarContraseña, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cmbTipoUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 103, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnAgregarUsuario)
                    .addComponent(btnLimpiarUsuario)
                    .addComponent(jLabel8))
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnAgregarUsuarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAgregarUsuarioActionPerformed

        //Variables que contendran los valores extraidos de los campos de texto
        String nombreUsuario;
        String contraseña;
        String confirmarContraseña;
        String tipoUsuario;
        //Variable que maneja la respuesta del JOptionPane
        int conf;

        //Extrayendo la informacion de los campos de texto
        nombreUsuario = txtNombreUsuario.getText();
        contraseña = txtContraseña.getText();
        confirmarContraseña = txtConfirmarContraseña.getText();
        tipoUsuario = cmbTipoUsuario.getSelectedItem().toString();

        //Validamos que no haya ningun campo vacio o sin seleccionar
        if (!(nombreUsuario.isEmpty() || contraseña.isEmpty() || confirmarContraseña.isEmpty() || tipoUsuario.equals("<--Seleccione un usuario-->") || nombreUsuario.equals("Ej: Admin-1"))) {
            //Iniciamos sesion en hibernate
            Session session = null;
            try {
                try {
                    //Conexion a la BD
                    sessionFactory = HibernateConexion.getSessionFactory();
                    //Apertura de la sesion
                    session = sessionFactory.openSession();
                    Transaction tx = session.beginTransaction();

                    //Si el nombre de usuario cumple con los estandares de tamaño procedemos a seguir evaluando
                    if (nombreUsuario.length() >= limMinimo) {
                        //Si las contraseñas coinciden procedemos a seguir evaluando
                        if (confirmarContraseña.equals(contraseña)) {
                            //Si la contraseña cumple con los estandares de tamaño minimo entonces se puede insertar
                            if (contraseña.length() >= limMinimo) {
                                //Mensaje de pregunta si quiere guardar los datos proporcionados 
                                conf = JOptionPane.showConfirmDialog(null, "¿Esta seguro de ingresar esos datos?", "Confirmación", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
                                if (conf == JOptionPane.YES_OPTION) { //Si la opción fue si
                                    //Se procede a insertar los valores en la base de datos

                                    //Ciframos la contraseña
                                    CifradorCesar cc = new CifradorCesar(contraseña, 4);
                                    cc.codCesar();
                                    String contraseñaCodificada = cc.getCodificada();

                                    //Instanciamos la clase usuario
                                    Usuario usuario = new Usuario();
                                    //Ingresamos la informacion del usuario al objeto usuario
                                    usuario.setNombreUsuario(nombreUsuario);
                                    usuario.setPassword(contraseñaCodificada);
                                    usuario.setTipoUsuario(tipoUsuario);

                                    //Se guarda el objeto en la base de datos
                                    session.save(usuario);
                                    tx.commit();
                                    //Finalizacion de la sesion de hibernate
                                    session.close();
                                    
                                    //Mensaje de confirmacion de insercion Exitosa
                                    JOptionPane.showMessageDialog(null, "Registro guardado exitosamente", "Información", JOptionPane.INFORMATION_MESSAGE);
                                    //Se limpian todos los campos
                                    limpiarCampos();
                                }
                            } else {
                                JOptionPane.showMessageDialog(null, "Error, la contraseña debe de contener por lo menos 6 caracteres", "Error en los datos", JOptionPane.ERROR_MESSAGE);
                                //Se limpia la pantalla
                                txtContraseña.setText("");
                                txtConfirmarContraseña.setText("");
                                barraContraseña.setValue(0);
                                jLabel6.setText("");
                            }
                        } else { //Si las contraseñas no coinciden
                            JOptionPane.showMessageDialog(null, "Error, las contraseñas no coinciden", "Error en los datos", JOptionPane.ERROR_MESSAGE);
                            //Se limpia la pantalla
                            txtContraseña.setText("");
                            txtConfirmarContraseña.setText("");
                            barraContraseña.setValue(0);
                            jLabel6.setText("");
                        }
                    } else {
                        JOptionPane.showMessageDialog(null, "Error, el nombre de usuario debe de contener por lo menos 6 caracteres", "Error en los datos", JOptionPane.ERROR_MESSAGE);
                        //Si limpia el nombre de usuario
                        txtNombreUsuario.setText("");
                    }
                } catch (org.hibernate.exception.ConstraintViolationException cve) {
                    session.getTransaction().rollback();
                    JOptionPane.showMessageDialog(null, "El nombre de usuario " + nombreUsuario + " ya se encuentra asignado a otro usuario previamente registrado\n"
                            + "Ingrese un nombre distinto para el nuevo usuario a registrar", "Duplicidad en registros", JOptionPane.ERROR_MESSAGE);
                    txtNombreUsuario.setText("");
                }
            } catch (org.hibernate.JDBCException e) {
                JOptionPane.showMessageDialog(null, "El usuario " + nombreUsuario + " no fue agregado debido a un problema de conexión con la base de datos.", "Error de conexión", JOptionPane.ERROR_MESSAGE);
            }
        } else {
            JOptionPane.showMessageDialog(null, "Error, no pueden haber campos vacios o sin seleccionar", "Error en los datos", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btnAgregarUsuarioActionPerformed

    private void btnLimpiarUsuarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLimpiarUsuarioActionPerformed

        //Limpiamos todos los campos
        limpiarCampos();

    }//GEN-LAST:event_btnLimpiarUsuarioActionPerformed

    private void txtConfirmarContraseñaKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtConfirmarContraseñaKeyTyped

        //Validar que el campo solo contenga numeros, letras y simbolos especiales

        //Variables que se encarga de captura el caracter digitado
        char c;
        c = evt.getKeyChar();

        //Si se ingresa un caracter no valido, se ignora
        if ((c < '0' || c > '9') && (c < 'a' || c > 'z') && (c < 'A' || c > 'Z') && (c != 'ñ') && (c != 'Ñ') && (c != '@') && (c != '$') && (c != '<') && (c != '>') && (c != '_') && (c != '-')) {
            evt.consume(); //ignora el caracter digitado 
        }

    }//GEN-LAST:event_txtConfirmarContraseñaKeyTyped

    private void txtNombreUsuarioKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtNombreUsuarioKeyTyped

        //Validar que el campo solo contenga numeros, letras y simbolos especiales

        //Variables que se encarga de captura el caracter digitado
        char c;
        c = evt.getKeyChar();

        //Si se ingresa un caracter no valido, se ignora
        if ((c < '0' || c > '9') && (c < 'a' || c > 'z') && (c < 'A' || c > 'Z') && (c != 'ñ') && (c != 'Ñ') && (c != '@') && (c != '$') && (c != '<') && (c != '>') && (c != '_') && (c != '-')) {
            evt.consume(); //ignora el caracter digitado 
        }

    }//GEN-LAST:event_txtNombreUsuarioKeyTyped

    private void txtContraseñaKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtContraseñaKeyTyped

        //Validar que el campo solo contenga numeros, letras y simbolos especiales

        //Variables que se encarga de captura el caracter digitado
        char c;
        c = evt.getKeyChar();

        //Si se ingresa un caracter no valido, se ignora
        if ((c < '0' || c > '9') && (c < 'a' || c > 'z') && (c < 'A' || c > 'Z') && (c != 'ñ') && (c != 'Ñ') && (c != '@') && (c != '$') && (c != '<') && (c != '>') && (c != '_') && (c != '-')) {
            evt.consume(); //ignora el caracter digitado 
        }

    }//GEN-LAST:event_txtContraseñaKeyTyped

    private void txtNombreUsuarioFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtNombreUsuarioFocusGained

        //Si el campo contiene el valor por defecto
        if (txtNombreUsuario.getText().equals("Ej: Admin-1")) {
            txtNombreUsuario.setText("");
            txtNombreUsuario.setForeground(Color.BLACK);
        }

    }//GEN-LAST:event_txtNombreUsuarioFocusGained

    private void txtNombreUsuarioFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtNombreUsuarioFocusLost

        //Verificamos si el campo esta vacio (de ser asi colocamos el valor por defecto nuevamente)
        if (txtNombreUsuario.getText().equals("")) {
            txtNombreUsuario.setText("Ej: Admin-1");
            txtNombreUsuario.setForeground(Color.LIGHT_GRAY);
        }

    }//GEN-LAST:event_txtNombreUsuarioFocusLost

    private void txtContraseñaFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtContraseñaFocusGained

        //Verificamos si el Bloq Mayus esta activado
        estadoBM = Toolkit.getDefaultToolkit().getLockingKeyState(KeyEvent.VK_CAPS_LOCK);

        //Si esta activo mostramos el mensaje
        if (estadoBM == true) {
            jLabel8.setVisible(true);
        } else {
            jLabel8.setVisible(false);
        }

    }//GEN-LAST:event_txtContraseñaFocusGained

    private void txtConfirmarContraseñaFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtConfirmarContraseñaFocusGained

        //Verificamos si el Bloq Mayus esta activado
        estadoBM = Toolkit.getDefaultToolkit().getLockingKeyState(KeyEvent.VK_CAPS_LOCK);

        //Si esta activo mostramos el mensaje
        if (estadoBM == true) {
            jLabel8.setVisible(true);
        } else {
            jLabel8.setVisible(false);
        }

    }//GEN-LAST:event_txtConfirmarContraseñaFocusGained

    private void txtContraseñaFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtContraseñaFocusLost

        //Ocultamos el label
        jLabel8.setVisible(false);

    }//GEN-LAST:event_txtContraseñaFocusLost

    private void txtConfirmarContraseñaFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtConfirmarContraseñaFocusLost

        //Ocultamos el label
        jLabel8.setVisible(false);

    }//GEN-LAST:event_txtConfirmarContraseñaFocusLost
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JProgressBar barraContraseña;
    private javax.swing.JButton btnAgregarUsuario;
    private javax.swing.JButton btnLimpiarUsuario;
    private javax.swing.JComboBox cmbTipoUsuario;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPasswordField txtConfirmarContraseña;
    private javax.swing.JPasswordField txtContraseña;
    private javax.swing.JTextField txtNombreUsuario;
    // End of variables declaration//GEN-END:variables
}
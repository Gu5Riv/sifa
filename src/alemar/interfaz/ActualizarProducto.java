//Interfaz que se encarga de actualizar los datos de un Producto registrado en el sistema
package alemar.interfaz;

import alemar.configuracion.HibernateConexion;
import alemar.entidad.Area;
import alemar.entidad.Producto;
import java.awt.Color;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.sql.Date;
import java.util.List;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

/**
 *
 * @author Teto
 */
public class ActualizarProducto extends javax.swing.JPanel {

    //Atributos para el uso de Hibernate
    private static SessionFactory sessionFactory = null;
    //Atributo que guarda el id del area al cual se hara referencia
    Area idArea;
    //Atributo que guarda el id del area al cual se hara referencia
    int idA;
    //Atributo que guarda el id del producto al cual se hara referencia
    int idProducto;

    public ActualizarProducto() {
        initComponents();
        limitarCaracteres();
        cargarComboBoxProducto();
        cargarComboBoxArea();
        deshabilitar();
    }

    private void limitarCaracteres() {

        //Limitando el numero de caracteres del campo "Descripcion"
        txtDescripcionProducto.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {
                if (txtDescripcionProducto.getText().length() == 250) {
                    e.consume();
                }
            }

            @Override
            public void keyPressed(KeyEvent arg0) {
            }

            @Override
            public void keyReleased(KeyEvent arg0) {
            }
        });

    }

    //Limpia registros y establece un color de letra mas suave
    private void limpiarCampos() {

        txtPrecioCompra.setText("");
        //txtPrecioCompra.setForeground(Color.LIGHT_GRAY);

        txtPrecioVenta.setText("");
        //txtPrecioVenta.setForeground(Color.LIGHT_GRAY);

        FechaVencimiento.setDate(null);
        //FechaVencimiento.setForeground(Color.LIGHT_GRAY);

        txtDescripcionProducto.setText("");
        //txtDescripcionProducto.setForeground(Color.LIGHT_GRAY);

        txtCantidadProducto.setText("");
        //txtCantidadProducto.setForeground(Color.LIGHT_GRAY);

        cmbAreaProducto.setSelectedIndex(0);
        cmbNombreProducto.setSelectedIndex(0);
        
    }

    //Habilita los campos para poder editar datos
    private void habilitar(){
        
        txtPrecioCompra.setEditable(true);
        txtPrecioVenta.setEditable(true);
        FechaVencimiento.setEnabled(true);
        txtDescripcionProducto.setEditable(true);
        txtCantidadProducto.setEditable(true);
        cmbAreaProducto.setEnabled(true);
        
    }
    
    //Deshabilita campos para que no se puedan editar
    private void deshabilitar(){
        
        txtPrecioCompra.setEditable(false);
        txtPrecioVenta.setEditable(false);
        FechaVencimiento.setEnabled(false);
        txtDescripcionProducto.setEditable(false);
        txtCantidadProducto.setEditable(false);
        cmbAreaProducto.setEnabled(false);
        limpiarCampos();
    }
    
    //Carga el comboBox ccon el nombre de las areas
    //de los productos a asignar
    private void cargarComboBoxArea() {

        //Obtencion de todos las areas de la base de datos
        //para llenar el comboBox de manera dinamica

        //Variable que contendra la lista de las areas
        List<Area> listaArea;

        //Inicializacion de session de hibernate
        Session session = null;

        try {
            //Chunces que ocupa Hibernate
            sessionFactory = HibernateConexion.getSessionFactory();
            session = sessionFactory.openSession();
            session.beginTransaction();

            //Obteniendo de la BD todos las areas de los productos
            //registrados en el sistema
            listaArea = session.createQuery("from Area").list();

            //Creando un modelo de comboBox                
            DefaultComboBoxModel modeloCombo = new DefaultComboBoxModel();

            //Poniendole un mensaje de seleccion (Pirmer elemento)
            modeloCombo.addElement("<--Seleccione una area de producto-->");

            //Llenando el modelo de comboBox con los nombres de las areas
            for (Area a : listaArea) {
                modeloCombo.addElement(a.getNombreArea());
            }

            //Agregando el modelo del comboBox al comboBox del panel
            cmbAreaProducto.setModel(modeloCombo);

        } catch (org.hibernate.JDBCException e) {
            JOptionPane.showMessageDialog(null, "No se puede establecer conexión con la base de datos.", "Error de conexión", JOptionPane.ERROR_MESSAGE);
        }

        //Cierre de sesion de Hibernate
        session.close();
    }

    //Carga el comboBox con el nombre de las areas
    //de los productos a asignar
    private void cargarComboBoxProducto() {

        //Obtencion de todos las areas de la base de datos
        //para llenar el comboBox de manera dinamica

        //Variable que contendra la lista de las areas
        List<Producto> listaProducto;

        //Inicializacion de session de hibernate
        Session session = null;

        try {
            //Chunces que ocupa Hibernate
            sessionFactory = HibernateConexion.getSessionFactory();
            session = sessionFactory.openSession();
            session.beginTransaction();

            //Obteniendo de la BD todos los nombres de los productos
            //registrados en el sistema
            listaProducto = session.createQuery("from Producto").list();

            //Creando un modelo de comboBox                
            DefaultComboBoxModel modeloCombo = new DefaultComboBoxModel();

            //Poniendole un mensaje de seleccion (Pirmer elemento)
            modeloCombo.addElement("<--Seleccione un producto-->");

            //Llenando el modelo de comboBox con los nombres de los productos
            for (Producto p : listaProducto) {
                modeloCombo.addElement(p.getNombreProducto());
            }

            //Agregando el modelo del comboBox al comboBox del panel
            cmbNombreProducto.setModel(modeloCombo);

        } catch (org.hibernate.JDBCException e) {
            JOptionPane.showMessageDialog(null, "No se puede establecer conexión con la base de datos.", "Error de conexión", JOptionPane.ERROR_MESSAGE);
        }

        //Cierre de sesion de Hibernate
        session.close();
    }

   
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        cmbNombreProducto = new javax.swing.JComboBox();
        jSeparator1 = new javax.swing.JSeparator();
        jPanel1 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        cmbAreaProducto = new javax.swing.JComboBox();
        jLabel5 = new javax.swing.JLabel();
        txtPrecioCompra = new javax.swing.JFormattedTextField();
        jLabel6 = new javax.swing.JLabel();
        txtPrecioVenta = new javax.swing.JFormattedTextField();
        jPanel2 = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        txtCantidadProducto = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtDescripcionProducto = new javax.swing.JTextArea();
        FechaVencimiento = new com.toedter.calendar.JDateChooser();
        btnActualizar = new javax.swing.JButton();
        btnLimpiar = new javax.swing.JButton();

        setBackground(new java.awt.Color(255, 255, 255));
        setMaximumSize(new java.awt.Dimension(794, 432));
        setMinimumSize(new java.awt.Dimension(794, 432));
        setPreferredSize(new java.awt.Dimension(794, 432));

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        jLabel1.setText("Actualizar información del producto");

        jLabel2.setText("Nombre del producto");

        cmbNombreProducto.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        cmbNombreProducto.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbNombreProductoItemStateChanged(evt);
            }
        });

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 102, 0)), "Información"));
        jPanel1.setMaximumSize(new java.awt.Dimension(372, 257));
        jPanel1.setMinimumSize(new java.awt.Dimension(372, 257));
        jPanel1.setPreferredSize(new java.awt.Dimension(372, 257));

        jLabel4.setText("Área del producto");

        cmbAreaProducto.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        cmbAreaProducto.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbAreaProductoItemStateChanged(evt);
            }
        });

        jLabel5.setText("Precio de compra");

        txtPrecioCompra.setBackground(new java.awt.Color(238, 253, 253));
        txtPrecioCompra.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtPrecioCompraFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtPrecioCompraFocusLost(evt);
            }
        });
        txtPrecioCompra.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtPrecioCompraKeyTyped(evt);
            }
        });

        jLabel6.setText("Precio de venta");

        txtPrecioVenta.setBackground(new java.awt.Color(238, 253, 253));
        txtPrecioVenta.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtPrecioVentaFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtPrecioVentaFocusLost(evt);
            }
        });
        txtPrecioVenta.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtPrecioVentaKeyTyped(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtPrecioCompra)
                    .addComponent(txtPrecioVenta)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel4)
                            .addComponent(jLabel5)
                            .addComponent(jLabel6))
                        .addGap(0, 256, Short.MAX_VALUE))
                    .addComponent(cmbAreaProducto, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cmbAreaProducto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addComponent(jLabel5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtPrecioCompra, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(35, 35, 35)
                .addComponent(jLabel6)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtPrecioVenta, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 102, 0)), "Detalles del producto"));

        jLabel7.setText("Cantidad de unidades a ingresar al sistema");

        txtCantidadProducto.setBackground(new java.awt.Color(238, 253, 253));
        txtCantidadProducto.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtCantidadProductoFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtCantidadProductoFocusLost(evt);
            }
        });
        txtCantidadProducto.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtCantidadProductoKeyTyped(evt);
            }
        });

        jLabel8.setText("Descripción del producto");

        jLabel9.setText("Fecha de vencimiento (NO aplica a productos NO perecederos)");

        txtDescripcionProducto.setColumns(20);
        txtDescripcionProducto.setRows(5);
        txtDescripcionProducto.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtDescripcionProductoFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtDescripcionProductoFocusLost(evt);
            }
        });
        jScrollPane1.setViewportView(txtDescripcionProducto);

        FechaVencimiento.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                FechaVencimientoMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel7)
                            .addComponent(jLabel8)
                            .addComponent(jLabel9))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(FechaVencimiento, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtCantidadProducto, javax.swing.GroupLayout.Alignment.LEADING))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel7)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtCantidadProducto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel8)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel9)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(FechaVencimiento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        btnActualizar.setText("Actualizar");
        btnActualizar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnActualizarActionPerformed(evt);
            }
        });

        btnLimpiar.setText("Limpiar");
        btnLimpiar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLimpiarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnLimpiar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnActualizar))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(35, 35, 35)
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 40, Short.MAX_VALUE)))
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel1)
                .addGap(259, 259, 259))
            .addGroup(layout.createSequentialGroup()
                .addGap(239, 239, 239)
                .addComponent(jLabel2)
                .addGap(18, 18, 18)
                .addComponent(cmbNombreProducto, javax.swing.GroupLayout.PREFERRED_SIZE, 241, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jSeparator1)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(16, 16, 16)
                .addComponent(jLabel1)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(cmbNombreProducto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(15, 15, 15)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 31, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnActualizar)
                    .addComponent(btnLimpiar))
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void cmbAreaProductoItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbAreaProductoItemStateChanged

        String nombreArea = "";

        List<Area> Larea;

        //Se extrae el area seleccionado
        nombreArea = cmbAreaProducto.getSelectedItem().toString();

        if (!(nombreArea.equalsIgnoreCase("<--Seleccione una area de producto-->"))) {

            //Inicio de sesion de hibernate
            Session session = null;
            //Cadena de seleccion de HQL
            String hql = "FROM alemar.entidad.Area AS a WHERE a.nombreArea = :nombreArea";
            try {
                try {
                    //Conexion a la BD
                    sessionFactory = HibernateConexion.getSessionFactory();
                    //Apertura de la sesion
                    session = sessionFactory.openSession();
                    Transaction txt = session.beginTransaction();

                    //Extraccion de los datos del area de la BD en base al nombre proporcionado                    
                    Query query = session.createQuery(hql).setString("nombreArea", nombreArea);

                    //Se pone en una lista el registro sacado
                    Larea = query.list();

                    //Guardando el id del area en una variable global
                    idA = Larea.get(0).getIdArea();

                    txt.commit();

                } catch (org.hibernate.JDBCException e) {
                    JOptionPane.showMessageDialog(null, "No se puede establecer conexión con la base de datos.", "Error de conexión", JOptionPane.ERROR_MESSAGE);
                }

            } finally {
                //Finalizacion de la sesion de hibernate
                session.close();
            }
        }



    }//GEN-LAST:event_cmbAreaProductoItemStateChanged

    private void txtCantidadProductoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCantidadProductoKeyTyped

        // SOLO NUMEROS
        char c;
        //capturar el caracter digitado
        c = evt.getKeyChar();
        if (c < '0' || c > '9') {
            evt.consume();//ignora el caracter digitado 
        }

    }//GEN-LAST:event_txtCantidadProductoKeyTyped

    private void btnLimpiarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLimpiarActionPerformed

        limpiarCampos();

    }//GEN-LAST:event_btnLimpiarActionPerformed

    private void txtPrecioCompraKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPrecioCompraKeyTyped

        // SOLO NUMEROS
        char c;
        //capturar el caracter digitado
        c = evt.getKeyChar();
        if ((c < '0' || c > '9') && (c != '.')) {
            evt.consume();//ignora el caracter digitado 
        }

    }//GEN-LAST:event_txtPrecioCompraKeyTyped

    private void txtPrecioVentaKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPrecioVentaKeyTyped

        // SOLO NUMEROS
        char c;
        //capturar el caracter digitado
        c = evt.getKeyChar();
        if ((c < '0' || c > '9') && (c != '.')) {
            evt.consume();//ignora el caracter digitado 
        }

    }//GEN-LAST:event_txtPrecioVentaKeyTyped

    private void txtPrecioCompraFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtPrecioCompraFocusLost

        //Verificamos si el campo esta vacio (de ser asi colocamos el valor por defecto nuevamente)
       // if (txtPrecioCompra.getText().equals("")) {
         //   txtPrecioCompra.setText("Ejemplo: 25.55");
           // txtPrecioCompra.setForeground(Color.LIGHT_GRAY);
        //}

    }//GEN-LAST:event_txtPrecioCompraFocusLost

    private void txtPrecioCompraFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtPrecioCompraFocusGained

        //Si el campo contiene el valor por defecto
       // if (txtPrecioCompra.getText().equals("Ejemplo: 25.55")) {
         //   txtPrecioCompra.setText("");
           txtPrecioCompra.setForeground(Color.BLACK);
        //}

    }//GEN-LAST:event_txtPrecioCompraFocusGained

    private void txtPrecioVentaFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtPrecioVentaFocusLost

        //Verificamos si el campo esta vacio (de ser asi colocamos el valor por defecto nuevamente)
      //  if (txtPrecioVenta.getText().equals("")) {
       //     txtPrecioVenta.setText("Ejemplo: 30.00");
        //    txtPrecioVenta.setForeground(Color.LIGHT_GRAY);
       // }

    }//GEN-LAST:event_txtPrecioVentaFocusLost

    private void txtPrecioVentaFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtPrecioVentaFocusGained

        //Si el campo contiene el valor por defecto
      //  if (txtPrecioVenta.getText().equals("Ejemplo: 30.00")) {
       //     txtPrecioVenta.setText("");
           txtPrecioVenta.setForeground(Color.BLACK);
        //}

    }//GEN-LAST:event_txtPrecioVentaFocusGained

    private void txtDescripcionProductoFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtDescripcionProductoFocusLost

        //Verificamos si el campo esta vacio (de ser asi colocamos el valor por defecto nuevamente)
      //  if (txtDescripcionProducto.getText().equals("")) {
       //     txtDescripcionProducto.setText("Ejemplo: Es una llave para tuercas #10");
        //    txtDescripcionProducto.setForeground(Color.LIGHT_GRAY);
       // }

    }//GEN-LAST:event_txtDescripcionProductoFocusLost

    private void txtDescripcionProductoFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtDescripcionProductoFocusGained

        //Si el campo contiene el valor por defecto
      //  if (txtDescripcionProducto.getText().equals("Ejemplo: Es una llave para tuercas #10")) {
       //     txtDescripcionProducto.setText("");
            txtDescripcionProducto.setForeground(Color.BLACK);
       // }

    }//GEN-LAST:event_txtDescripcionProductoFocusGained

    private void txtCantidadProductoFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtCantidadProductoFocusGained

        //Si el campo contiene el valor por defecto
    //    if (txtCantidadProducto.getText().equals("Ejemplo: 500")) {
      //      txtCantidadProducto.setText("");
            txtCantidadProducto.setForeground(Color.BLACK);
       // }


    }//GEN-LAST:event_txtCantidadProductoFocusGained

    private void txtCantidadProductoFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtCantidadProductoFocusLost

        //Verificamos si el campo esta vacio (de ser asi colocamos el valor por defecto nuevamente)
    //    if (txtCantidadProducto.getText().equals("")) {
      //      txtCantidadProducto.setText("Ejemplo: 500");
        //    txtCantidadProducto.setForeground(Color.LIGHT_GRAY);
        //}

    }//GEN-LAST:event_txtCantidadProductoFocusLost

    private void FechaVencimientoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_FechaVencimientoMouseClicked

        FechaVencimiento.setDate(null);

    }//GEN-LAST:event_FechaVencimientoMouseClicked

    //Cuando seleccione de manera dinamica el nombre del producto
    //se van a almacenar los registros en el correspondiente
    //campo de registro al cual le fue asignado en primera instancia
    private void cmbNombreProductoItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbNombreProductoItemStateChanged

        String nombreProduc;

        List<Producto> Lproducto;

        //Se extrae el nombre del producto seleccionado
        nombreProduc = cmbNombreProducto.getSelectedItem().toString();

        if (!(nombreProduc.equals("<--Seleccione una area de producto-->"))) {

            //Chunche de Hibernate (Inicio de sesion de hibernate)
            Session session = null;
            //Cadena de seleccion de HQL
            String hql = "FROM alemar.entidad.Producto AS p WHERE p.nombreProducto = :nombreProducto";
            try {
                try {
                    //Conexion a la BD
                    sessionFactory = HibernateConexion.getSessionFactory();
                    //Apertura de la sesion
                    session = sessionFactory.openSession();
                    Transaction txt = session.beginTransaction();

                    //Extraccion de los datos del proveedor de la BD en base al nombre proporcionado                    
                    Query query = session.createQuery(hql).setString("nombreProducto", nombreProduc);

                    //Se pone en una lista el registro sacado
                    Lproducto = query.list();

                    //Guardando el id del proveedor en una variable global
                    idProducto = Lproducto.get(0).getIdProducto();

                    //Guardando el nombre del area en una variable global
                    idArea = Lproducto.get(0).getArea();

                    //Se ponen los valores en los campos de texto correspondientes
                    //para que se modifiquen

                    txtPrecioCompra.setText(String.valueOf(Lproducto.get(0).getPrecioCompra()));
                    txtPrecioVenta.setText(String.valueOf(Lproducto.get(0).getPrecioVenta()));
                    txtCantidadProducto.setText(String.valueOf(Lproducto.get(0).getCantidad()));
                    txtDescripcionProducto.setText(Lproducto.get(0).getDescripcion());
                    FechaVencimiento.setDate(null);
                    FechaVencimiento.setDate(Lproducto.get(0).getVencimiento());
                    
                    habilitar();

                    txt.commit();

                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }
            } finally {
                //Finalizacion de la sesion de hibernate
                session.close();
            }
        }else{
           deshabilitar();
        }
    }//GEN-LAST:event_cmbNombreProductoItemStateChanged

    private void btnActualizarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnActualizarActionPerformed

        //Variables que almacenaran los registros insertados por el usuario
        String nombreArea;
        Double precioCompra;
        Double precioVenta;
        int cantidad;
        String descripcion;
        Date fechaVencimiento;

        //Variable que maneja la respuesta del JOptionPane
        int conf;

        //Extrayendo la informacion de los campos de texto
        nombreArea = cmbAreaProducto.getSelectedItem().toString();
        descripcion = txtDescripcionProducto.getText();

        //OJO con esta forma de sacar la fecha, tiene el formato de MySql
        if (FechaVencimiento.getDate() == null) {
            fechaVencimiento = null;
        } else {
            fechaVencimiento = new java.sql.Date(FechaVencimiento.getDate().getTime());
        }

        //Si solo se presiona el boton sin haber ingresado ningun dato
        //se tomaran como datos vacios
        if (txtPrecioCompra.getText().toString().equals("")) {
            precioCompra = 00.00;
        } else {
            precioCompra = Double.parseDouble(txtPrecioCompra.getText());
        }

        if (txtPrecioVenta.getText().toString().equals("")) {
            precioVenta = 00.00;
        } else {
            precioVenta = Double.parseDouble(txtPrecioVenta.getText());
        }

        if (txtCantidadProducto.getText().toString().equals("")) {
            cantidad = 0;
        } else {
            cantidad = Integer.parseInt(txtCantidadProducto.getText());
        }

        //Validamos que no haya ningun campo vacio o sin seleccionar
        if (!(nombreArea.equals("<--Seleccione una area de producto-->") || precioCompra == 00.00
                || precioVenta == 00.00 || cantidad == 0 
                || cmbNombreProducto.getSelectedItem().toString().equals("<--Seleccione un producto-->"))) {

            //Iniciamos sesion en hibernate
            Session session = null;
            try {
                try {
                    //Conexion a la BD
                    sessionFactory = HibernateConexion.getSessionFactory();
                    //Apertura de la sesion
                    session = sessionFactory.openSession();
                    Transaction tx = session.beginTransaction();

                    //Mensaje de pregunta si quiere guardar los datos proporcionados 
                    conf = JOptionPane.showConfirmDialog(null, "¿Esta seguro de ingresar esos datos?", "Confirmación", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
                    if (conf == JOptionPane.YES_OPTION) { //Si la opción fue si
                        //Se procede a insertar los valores en la base de datos

                        //Instanciamos la clase Producto
                        Producto producto = (Producto) session.load(Producto.class,idProducto);
                        
                        //Instanciamos la clase Area 
                        //(Esta es para guardar la referencia del registro de la tabla Area
                        Area area;
                        
                        //Clase temporal de Area
                        Area areat = new Area ();

                        //Obtenemos el registro del area que selecciono el usuario
                        //para referenciar a este registro
                        area = (Area) session.load(Area.class, idA);
                        
                        areat.setIdArea(area.getIdArea());
                        areat.setNombreArea(area.getNombreArea());
                        areat.setIdentificacion(area.getIdentificacion());
                        
                        //Ingresamos la informacion del producto al objeto Producto

                        producto.setArea(areat); //Con esto establezco la relacion del nuevo producto con el area
                        producto.setPrecioCompra(precioCompra);
                        producto.setPrecioVenta(precioVenta);
                        producto.setCantidad(cantidad);
                        producto.setDescripcion(descripcion);
                        producto.setVencimiento(fechaVencimiento);
                        //Valor por default para que no tenga problemas gustavo con las notificaciones XD
                        producto.setLimiteProducto(0);

                        //Se guarda el objeto en la base de datos
                        session.update(producto);
                        tx.commit();
                        //Finalizacion de la sesion de hibernate
                        session.close();

                        //Mensaje de confirmacion de insercion Exitosa
                        JOptionPane.showMessageDialog(null, "Registro guardado exitosamente", "Información", JOptionPane.INFORMATION_MESSAGE);
                        //Se limpian todos los campos
                        limpiarCampos();
                        deshabilitar();
                        
                    }
                } catch (org.hibernate.exception.ConstraintViolationException cve) {
                    session.getTransaction().rollback();
                    JOptionPane.showMessageDialog(null, "El nombre del producto  ya se encuentra asignado a otro producto previamente registrado\n"
                            + "Ingrese un nombre distinto para el nuevo producto a registrar", "Duplicidad en registros", JOptionPane.ERROR_MESSAGE);
                    
                }
            } catch (org.hibernate.JDBCException e) {
                JOptionPane.showMessageDialog(null, "No se puede establecer conexión con la base de datos.", "Error de conexión", JOptionPane.ERROR_MESSAGE);
            }

        } else {
            JOptionPane.showMessageDialog(null, "Error, no pueden haber campos vacios o sin seleccionar", "Error en los datos", JOptionPane.ERROR_MESSAGE);
        }//Fin de la validacion de campos vacios
                
    }//GEN-LAST:event_btnActualizarActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private com.toedter.calendar.JDateChooser FechaVencimiento;
    private javax.swing.JButton btnActualizar;
    private javax.swing.JButton btnLimpiar;
    private javax.swing.JComboBox cmbAreaProducto;
    private javax.swing.JComboBox cmbNombreProducto;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JTextField txtCantidadProducto;
    private javax.swing.JTextArea txtDescripcionProducto;
    private javax.swing.JFormattedTextField txtPrecioCompra;
    private javax.swing.JFormattedTextField txtPrecioVenta;
    // End of variables declaration//GEN-END:variables
}

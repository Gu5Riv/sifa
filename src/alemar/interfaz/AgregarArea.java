//Interfaz que se encarga de Ingresar un area  en el sistema
package alemar.interfaz;

import alemar.configuracion.HibernateConexion;
import alemar.entidad.Area;
import java.awt.Color;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.List;
import javax.swing.JOptionPane;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

/**
 *
 * @author Teto
 */
public class AgregarArea extends javax.swing.JPanel {

    //Variable que ocupa hibernate
    private static SessionFactory sessionFactory = null;
    private int limMinimo = 25;
    private int limMaximo = 100;

    public AgregarArea() {
        initComponents();
        txtNombreArea.setToolTipText("Ingrese el nombre del àrea.Unicámente letra, por ejemplo: Jardineria.");
        txtIdentificacionArea.setToolTipText("Ej: Es el conjunto de herramientas, accesorios y adornos relacionados a la jardinería.");
        txtNombreArea.setText("Ej: Jardinería");
        txtIdentificacionArea.setText("Ej: Es el conjunto de herramientas, accesorios y adornos relacionados a la jardinería.");
        txtNombreArea.setForeground(Color.LIGHT_GRAY);
        txtIdentificacionArea.setForeground(Color.LIGHT_GRAY);

        txtNombreArea.addKeyListener(new KeyListener() {
            public void keyTyped(KeyEvent e) {
                if (txtNombreArea.getText().length() == limMinimo) {
                    e.consume();
                }
            }

            public void keyPressed(KeyEvent arg0) {
            }

            public void keyReleased(KeyEvent arg0) {
            }
        });

        txtIdentificacionArea.addKeyListener(new KeyListener() {
            public void keyTyped(KeyEvent e) {
                if (txtIdentificacionArea.getText().length() == limMaximo) {
                    e.consume();
                }
            }

            public void keyPressed(KeyEvent arg0) {
            }

            public void keyReleased(KeyEvent arg0) {
            }
        });



        limpiar();

        //Limitando el numero de caracteres
        txtIdentificacionArea.addKeyListener(new KeyListener() {
            public void keyTyped(KeyEvent e) {
                if (txtIdentificacionArea.getText().length() == 100) {
                    e.consume();
                }
            }

            public void keyPressed(KeyEvent arg0) {
            }

            public void keyReleased(KeyEvent arg0) {
            }
        });
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        btnAregarArea = new javax.swing.JButton();
        btnLimpiar = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        txtIdentificacionArea = new javax.swing.JTextArea();
        txtNombreArea = new javax.swing.JTextField();

        setBackground(new java.awt.Color(255, 255, 255));
        setMaximumSize(new java.awt.Dimension(745, 400));
        setMinimumSize(new java.awt.Dimension(745, 400));
        setPreferredSize(new java.awt.Dimension(745, 400));

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Agregar nueva área al sistema");

        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel2.setText("Nombre de área");

        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel3.setText("Descripción");

        btnAregarArea.setText("Agregar");
        btnAregarArea.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAregarAreaActionPerformed(evt);
            }
        });

        btnLimpiar.setText("Limpiar");
        btnLimpiar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLimpiarActionPerformed(evt);
            }
        });

        txtIdentificacionArea.setBackground(new java.awt.Color(238, 253, 253));
        txtIdentificacionArea.setColumns(20);
        txtIdentificacionArea.setLineWrap(true);
        txtIdentificacionArea.setRows(5);
        txtIdentificacionArea.setToolTipText("Ingrese la descripción del área, por ejemplo: Pala, Tijera para podar, Navaja para injerto, etc");
        txtIdentificacionArea.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                txtIdentificacionAreaMouseClicked(evt);
            }
        });
        txtIdentificacionArea.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtIdentificacionAreaKeyTyped(evt);
            }
        });
        jScrollPane2.setViewportView(txtIdentificacionArea);

        txtNombreArea.setBackground(new java.awt.Color(238, 253, 253));
        txtNombreArea.setToolTipText("");
        txtNombreArea.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                txtNombreAreaMouseClicked(evt);
            }
        });
        txtNombreArea.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtNombreAreaKeyTyped(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnLimpiar)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnAregarArea)
                .addContainerGap())
            .addGroup(layout.createSequentialGroup()
                .addGap(190, 190, 190)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 240, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtNombreArea, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 240, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 366, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(189, 189, 189))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(70, 70, 70)
                .addComponent(jLabel1)
                .addGap(56, 56, 56)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(txtNombreArea, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel3)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 86, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnAregarArea)
                    .addComponent(btnLimpiar))
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnLimpiarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLimpiarActionPerformed

        limpiar();

    }//GEN-LAST:event_btnLimpiarActionPerformed

    private void btnAregarAreaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAregarAreaActionPerformed
        // TODO add your handling code here:

        String nombreArea = "";
        String identificacion = "";

        //Variable que maneja la respuesta del JOptionPane
        int conf;

        //Extrayendo la informacion de los campos de texto
        nombreArea = txtNombreArea.getText();
        identificacion = txtIdentificacionArea.getText();

       if (nombreArea.equals("Ej: Jardinería.") || "".equals(nombreArea.replaceAll(" ",""))) {
            nombreArea = "";
       }
        
        if(identificacion.equals("Ej: Es el conjunto de herramientas, accesorios y adornos relacionados a la jardinería.")){
            identificacion = "";
        }

        if (!(nombreArea.isEmpty() || identificacion.isEmpty() || nombreArea.equals("Ej: Jardinería.")) || identificacion.equals("Ej: Es el conjunto de herramientas, accesorios y adornos relacionados a la jardinería.") ) { //Si no esta vacio algun campo           

            //Chunche de Hibernate (Inicio de sesion de hibernate)
            Session session = null;
            try {
                try {
                    //Conexion a la BD
                    sessionFactory = HibernateConexion.getSessionFactory();
                    //Apertura de la sesion
                    session = sessionFactory.openSession();
                    Transaction tx = session.beginTransaction();
                    //if (nombreArea.length() >= limMinimo) {
                    //Creando el objeto Proveedor 
                    Area area = new Area();
                    //Ingresando la informacion del proveedor al 
                    //objeto Proveedor
                    area.setNombreArea(nombreArea);
                    area.setIdentificacion(identificacion);
                    //Mensaje de pregunta si quiere guardar los datos proporcionados 
                    conf = JOptionPane.showConfirmDialog(null, "¿Esta seguro de ingresar estos datos?", "Confirmación", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);

                    if (conf == JOptionPane.YES_OPTION) {//Si la opcion fue SI
                        if (!(verificarNombreArea(nombreArea))) {
                            //Guardando el objeto en la base de datos
                            session.save(area);
                            tx.commit();
                            session.close();
                            //Mensaje de confirmacion de insercion Exitosa
                            JOptionPane.showMessageDialog(null, "Registro guardado exitosamente", "Información", JOptionPane.INFORMATION_MESSAGE);
                            limpiar();
                      
                      } else {
                            JOptionPane.showMessageDialog(null, "El nombre de Area " + nombreArea + " ya se encuentra asignado a otra Area previamente registrada\n"
                            + "Ingrese un nombre distinto para la nueva Area a registrar", "Duplicidad en registros", JOptionPane.ERROR_MESSAGE);
                    txtNombreArea.setText("");
                          
                            limpiar();
                        }
                    }
                 
                }catch (org.hibernate.exception.ConstraintViolationException cve) {
                    session.getTransaction().rollback();
                    JOptionPane.showMessageDialog(null, "El nombre de Area " + nombreArea + " ya se encuentra asignado a otra Area previamente registrada\n"
                            + "Ingrese un nombre distinto para la nueva Area a registrar", "Duplicidad en registros", JOptionPane.ERROR_MESSAGE);
                    txtNombreArea.setText("");
                }
            } catch (org.hibernate.JDBCException e) {
                JOptionPane.showMessageDialog(null, "Error: No se puede establecer conexión con la base de datos.", "Error de conexión", JOptionPane.ERROR_MESSAGE);
            }
        } else {//Si hay campos vacios, mensaje de error
            JOptionPane.showMessageDialog(null, "Error, no pueden haber campos vacios o sin seleccionar", "Error en los datos", JOptionPane.ERROR_MESSAGE);
            limpiar();
        }

    }//GEN-LAST:event_btnAregarAreaActionPerformed

    private void txtIdentificacionAreaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtIdentificacionAreaMouseClicked
        
        txtIdentificacionArea.setText("");
        txtIdentificacionArea.setForeground(Color.BLACK);
        
    }//GEN-LAST:event_txtIdentificacionAreaMouseClicked

    private void txtNombreAreaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtNombreAreaMouseClicked
        
        txtNombreArea.setText("");
        txtNombreArea.setForeground(Color.BLACK);
        
    }//GEN-LAST:event_txtNombreAreaMouseClicked

    private void txtNombreAreaKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtNombreAreaKeyTyped
        
        char c;
        c=evt.getKeyChar();
        if ((c < '0' || c > '9') && (c < 'a' || c > 'z') && (c < 'A' || c > 'Z') && (c != 'ñ') && (c != 'Ñ') && (c != 32))
        evt.consume();
        
    }//GEN-LAST:event_txtNombreAreaKeyTyped

    private void txtIdentificacionAreaKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtIdentificacionAreaKeyTyped
        
        char c;
        c=evt.getKeyChar();
        if ((c < '0' || c > '9') && (c < 'a' || c > 'z') && (c < 'A' || c > 'Z') && (c != 'ñ') && (c != 'Ñ') && (c != ',') && (c != 32))
        evt.consume();
        
    }//GEN-LAST:event_txtIdentificacionAreaKeyTyped

    //Metodo para verificar la existencia de un proveedor
    private boolean verificarNombreArea(String nombreArea) {

        //Variable que contendra la lista de proveedores
        List<Area> listaArea = null;

        boolean resultado;

        //Chunche de Hibernate (Inicio de sesion de hibernate)
        Session session = null;
        try {
            try {
                //Conexion a la BD
                sessionFactory = HibernateConexion.getSessionFactory();
                //Apertura de la sesion
                session = sessionFactory.openSession();
                Transaction tx = session.beginTransaction();

                //Ingresando la informacion del proveedor al 
                //Obteniendo de la BD todos los proveedores                
                listaArea = session.createQuery("from Area").list();

                //Verificando los nombre de los proveedores
                for (Area a : listaArea) {
                    if (nombreArea.equals(a.getNombreArea())) {
                        return resultado = true;
                    }
                }

            } catch (org.hibernate.JDBCException e) {
                JOptionPane.showMessageDialog(null, "No se puede establecer conexión con la base de datos.", "Error de conexión", JOptionPane.ERROR_MESSAGE);
            }
        } finally {
            //Finalizacion de la sesion de hibernate
            session.close();
        }

        return resultado = false;
    }

    //Metodo que limpia los campos de texto.
    private void limpiar() {

        txtNombreArea.setText("Ej: Jardinería.");
        txtNombreArea.setForeground(Color.LIGHT_GRAY);

        txtIdentificacionArea.setText("Ej: Es el conjunto de herramientas, accesorios y adornos relacionados a la jardinería.");
        txtIdentificacionArea.setForeground(Color.LIGHT_GRAY);
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAregarArea;
    private javax.swing.JButton btnLimpiar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTextArea txtIdentificacionArea;
    private javax.swing.JTextField txtNombreArea;
    // End of variables declaration//GEN-END:variables
}

//Interfaz que se encarga de actualizar los datos de un Proveedro registrado en el sistema
package alemar.interfaz;

import alemar.clases.CargarCMBProveedor;
import alemar.configuracion.HibernateConexion;
import alemar.entidad.Proveedor;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.List;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

public class ActualizarProveedor extends javax.swing.JPanel {

    //Atributos para el uso de Hibernate
    private static SessionFactory sessionFactory = null;
    //Para limitar el numero de caracteres de un campo de texto
    private int limite = 9;
    private int limMinimo = 6;
    private int limMaximo = 20;
    private int limmax = 30;
    private int max=70;
    private int maximo=50;
    //Lista que contendra los Proveedores registrados en el sistema
    List<Proveedor> listaProveedor = null;
    //Para guardar el id del proveedor
    private long id;
    //Para guardar el nombre del proveedor
    private String nom = "";

    public ActualizarProveedor() {
        initComponents();

        //Se carga el combobox con los datos de los Proveedores registaros en la base de datos
        cargarComboBox();

        //Deshabilitando edicion
        txtNuevoNombreProveedor.setEditable(false);
        txtDireccionProveedor.setEditable(false);
        txtTelefonoProveedor.setEditable(false);
        txtProductosProveedor.setEditable(false);
        txtEmail.setEditable(false);

         //Agregando mensajes de ayuda para el llenado de los campos
        txtTelefonoProveedor.setToolTipText("Ingrese el número de teléfono sin guiones");
        txtEmail.setToolTipText("Ingrese el E-mail del proveedor. Unicámente letras, numeros y simbolos (@,$,<,>,_,-)");
        txtNuevoNombreProveedor.setToolTipText("Ingrese el nuevo nombre del proveedor. Unicámente letras");
        txtDireccionProveedor.setToolTipText("Ingrese la dirección del proveedor, Unicámente letras, numeros y simbolos (@,$,<,>,_,-)");
        txtProductosProveedor.setToolTipText("Ingrese los productos que oferta el proveedor. Unicámente letras");

        //Limitando el numero de caracteres
        txtTelefonoProveedor.addKeyListener(new KeyListener() {
            public void keyTyped(KeyEvent e) {
                if (txtTelefonoProveedor.getText().length() == 8) {
                    e.consume();
                }
            }

            public void keyPressed(KeyEvent arg0) {
            }

            public void keyReleased(KeyEvent arg0) {
            }
        });


        //Limitando el numero de caracteres
        txtProductosProveedor.addKeyListener(new KeyListener() {
            public void keyTyped(KeyEvent e) {
                if (txtProductosProveedor.getText().length() == max) {
                    e.consume();
                }
            }

            public void keyPressed(KeyEvent arg0) {
            }

            public void keyReleased(KeyEvent arg0) {
            }
        });

        //Limitando el numero de caracteres
        txtNuevoNombreProveedor.addKeyListener(new KeyListener() {
            public void keyTyped(KeyEvent e) {
                if (txtNuevoNombreProveedor.getText().length() == 25) {
                    e.consume();
                }
            }

            public void keyPressed(KeyEvent arg0) {
            }

            public void keyReleased(KeyEvent arg0) {
            }
        });

        //Limitando el numero de caracteres
        txtDireccionProveedor.addKeyListener(new KeyListener() {
            public void keyTyped(KeyEvent e) {
                if (txtDireccionProveedor.getText().length() == max) {
                    e.consume();
                }
            }

            public void keyPressed(KeyEvent arg0) {
            }

            public void keyReleased(KeyEvent arg0) {
            }
        });
        //Limitando el numero de caracteres
        txtEmail.addKeyListener(new KeyListener() {
            public void keyTyped(KeyEvent e) {
                if (txtEmail.getText().length() == maximo) {
                    e.consume();
                }
            }

            public void keyPressed(KeyEvent arg0) {
            }

            public void keyReleased(KeyEvent arg0) {
            }
        });
    }

    //Metodos de la clase
    //Metodo que se encarga de cargar los usuarios registrados en el sistema en un combo box
    private void cargarComboBox() {
        //Se inicializa la lista a null
        listaProveedor = null;

        //Instanciamos la clase CargarCMBUsuario para cargar todos los usuarios registrados en una lista
        CargarCMBProveedor cargarProveedores = new CargarCMBProveedor();
        listaProveedor = cargarProveedores.cargarProveedores();

        //Creando un modelo de comboBox                
        DefaultComboBoxModel modeloCombo = new DefaultComboBoxModel();

        //Poniendole un mensaje de seleccion (Primer elemento) al combo box
        modeloCombo.addElement("<--Seleccione un Proveedor-->");

        //Llenando el modelo de comboBox con los nombres de los usuarios
        for (Proveedor proveedor : listaProveedor) {
            //Si el usuario es el superadministrador no lo cargamos

            modeloCombo.addElement(proveedor.getNombre());

        }
        //Agregando el modelo del comboBox al comboBox del panel
        cmbNombreProveedor.setModel(modeloCombo);
    }

    //Metodo que limpia todos los campos de la interfaz
    private void limpiarCampos() {
        txtNuevoNombreProveedor.setText("");
        txtDireccionProveedor.setText("");
        txtTelefonoProveedor.setText("");
        txtProductosProveedor.setText("");
        txtEmail.setText("");
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnActualizarProveedor = new javax.swing.JButton();
        btnLimpiarProveedor = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        cmbNombreProveedor = new javax.swing.JComboBox();
        jLabel2 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        txtNuevoNombreProveedor = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        txtDireccionProveedor = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtProductosProveedor = new javax.swing.JTextArea();
        jLabel = new javax.swing.JLabel();
        txtEmail = new javax.swing.JTextField();
        txtTelefonoProveedor = new javax.swing.JTextField();

        setBackground(new java.awt.Color(255, 255, 255));
        setMaximumSize(new java.awt.Dimension(745, 400));
        setMinimumSize(new java.awt.Dimension(745, 400));
        setPreferredSize(new java.awt.Dimension(745, 400));

        btnActualizarProveedor.setText("Actualizar");
        btnActualizarProveedor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnActualizarProveedorActionPerformed(evt);
            }
        });

        btnLimpiarProveedor.setText("Limpiar");
        btnLimpiarProveedor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLimpiarProveedorActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Actualizar información de proveedor");

        cmbNombreProveedor.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        cmbNombreProveedor.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbNombreProveedorItemStateChanged(evt);
            }
        });

        jLabel2.setText("Nombre del proveedor");

        txtNuevoNombreProveedor.setBackground(new java.awt.Color(238, 253, 253));
        txtNuevoNombreProveedor.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtNuevoNombreProveedorKeyTyped(evt);
            }
        });

        jLabel3.setText("Nuevo nombre del proveedor");

        txtDireccionProveedor.setBackground(new java.awt.Color(238, 253, 253));
        txtDireccionProveedor.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtDireccionProveedorKeyTyped(evt);
            }
        });

        jLabel4.setText("Direccción");

        jLabel5.setText("Teléfono");

        jLabel6.setText("Productos que oferta");

        txtProductosProveedor.setBackground(new java.awt.Color(238, 253, 253));
        txtProductosProveedor.setColumns(20);
        txtProductosProveedor.setRows(5);
        txtProductosProveedor.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtProductosProveedorKeyTyped(evt);
            }
        });
        jScrollPane1.setViewportView(txtProductosProveedor);

        jLabel.setText("E-mail");

        txtEmail.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtEmailKeyTyped(evt);
            }
        });

        txtTelefonoProveedor.setBackground(new java.awt.Color(238, 253, 253));
        txtTelefonoProveedor.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtTelefonoProveedorKeyTyped(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 148, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(btnLimpiarProveedor, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnActualizarProveedor)
                                .addContainerGap())
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(jLabel2)
                                .addGap(28, 28, 28)
                                .addComponent(cmbNombreProveedor, javax.swing.GroupLayout.PREFERRED_SIZE, 240, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(212, 212, 212))))))
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(103, 103, 103)
                        .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 493, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(124, 124, 124)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(jLabel3)
                                .addGap(18, 18, 18)
                                .addComponent(txtNuevoNombreProveedor, javax.swing.GroupLayout.PREFERRED_SIZE, 240, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(jLabel5)
                                .addGap(18, 18, 18)
                                .addComponent(txtTelefonoProveedor, javax.swing.GroupLayout.PREFERRED_SIZE, 240, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(jLabel4)
                                .addGap(18, 18, 18)
                                .addComponent(txtDireccionProveedor, javax.swing.GroupLayout.PREFERRED_SIZE, 240, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(jLabel)
                                .addGap(18, 18, 18)
                                .addComponent(txtEmail, javax.swing.GroupLayout.PREFERRED_SIZE, 240, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(jLabel6)
                                .addGap(18, 18, 18)
                                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 240, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addComponent(jLabel1)
                .addGap(37, 37, 37)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cmbNombreProveedor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addGap(13, 13, 13)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(txtNuevoNombreProveedor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(14, 14, 14)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtDireccionProveedor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4))
                .addGap(13, 13, 13)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(txtTelefonoProveedor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel)
                    .addComponent(txtEmail, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnActualizarProveedor)
                    .addComponent(btnLimpiarProveedor))
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnLimpiarProveedorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLimpiarProveedorActionPerformed

        //Limpiamos todos los campos
        limpiarCampos();
        cmbNombreProveedor.setSelectedIndex(0);

    }//GEN-LAST:event_btnLimpiarProveedorActionPerformed

    private void cmbNombreProveedorItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbNombreProveedorItemStateChanged

        String nombre = "";

        //Se extrae el proveedor seleccionado
        nombre = cmbNombreProveedor.getSelectedItem().toString();

        //Si se selecciona el primer elemento del combobox
        if (nombre.equalsIgnoreCase("<--Seleccione un Proveedor-->")) {
            limpiarCampos();
        } else {
            //Chunche de Hibernate (Inicio de sesion de hibernate)
            Session session = null;
            //Cadena de seleccion de HQL
            String hql = "FROM alemar.entidad.Proveedor AS p WHERE p.nombre = :nombre";
            try {
                //Conexion a la BD
                sessionFactory = HibernateConexion.getSessionFactory();
                //Apertura de la sesion
                session = sessionFactory.openSession();
                Transaction txt = session.beginTransaction();

                //Extraccion de los datos del proveedor de la BD en base al nombre proporcionado                    
                Query query = session.createQuery(hql).setString("nombre", nombre);

                listaProveedor = query.list();
                //Guardando el id del usuario en una variable global
                id = listaProveedor.get(0).getIdProveedor();

                //Se ponen los valores en los campos de texto correspondientes para que se modifiquen
                txtNuevoNombreProveedor.setText(listaProveedor.get(0).getNombre());
                txtDireccionProveedor.setText(listaProveedor.get(0).getDireccion());
                txtTelefonoProveedor.setText(listaProveedor.get(0).getTelefono());
                txtProductosProveedor.setText(listaProveedor.get(0).getProductosOferta());
                txtEmail.setText(listaProveedor.get(0).getEmail());

                //habilitando edicion
                txtNuevoNombreProveedor.setEditable(true);
                txtDireccionProveedor.setEditable(true);
                txtTelefonoProveedor.setEditable(true);
                txtProductosProveedor.setEditable(true);
                txtEmail.setEditable(true);
                txt.commit();
                //Finalizacion de la sesion de hibernate
                session.close();
            } catch (org.hibernate.JDBCException e) {
                JOptionPane.showMessageDialog(null, "No se puedieron cargar los datos del proveedor " + nombre + " debido a un problema de conexión con la base de datos.", "Error de conexión", JOptionPane.ERROR_MESSAGE);
            }
        }

    }//GEN-LAST:event_cmbNombreProveedorItemStateChanged

    private void btnActualizarProveedorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnActualizarProveedorActionPerformed

        //Variables que contendran los valores de los campos de texto 
        String nombre;
        String direccion;
        String productos;
        String telefono;
        String email;

        //Variable que maneja la respuesta del JOptionpane
        int conf;

        //Extrayendo la informacion de los campos de texto
        nombre = txtNuevoNombreProveedor.getText();
        direccion = txtDireccionProveedor.getText();
        telefono = txtTelefonoProveedor.getText();
        productos = txtProductosProveedor.getText();
        email = txtEmail.getText();


        //Validaciones
        if (!(nombre.isEmpty() || direccion.isEmpty() || telefono.isEmpty() || productos.isEmpty())) { //Si no esta vacio algun campo           

            //Chunche de Hibernate (Inicio de sesion de hibernate)
            Session session = null;
            try {
                try {
                    //Conexion a la BD
                    sessionFactory = HibernateConexion.getSessionFactory();
                    //Apertura de la sesion
                    session = sessionFactory.openSession();
                    Transaction tx = session.beginTransaction();

                    //Extrayendo el regitro del proveedor seleccionado
                    //en base al ID guardado previamente de manera global
                    Proveedor proveedor = (Proveedor) session.get(Proveedor.class, id);

                    //Ingresando la nueva informacion del proveedor al  objeto Proveedor
                    proveedor.setNombre(nombre);
                    proveedor.setDireccion(direccion);
                    proveedor.setTelefono(telefono);
                    proveedor.setProductosOferta(productos);
                    proveedor.setEmail(email);
                    //Validacion de campos
                    if (telefono.length() == 8) {
                        
                    //Mensaje de pregunta si quiere guardar los datos proporcionados 
                    conf = JOptionPane.showConfirmDialog(null, "¿Esta seguro de actualizar estos datos?", "Confirmación", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);

                    if (conf == JOptionPane.YES_OPTION) {
                        //Guardando el objeto en la base de datos
                        session.update(proveedor);
                        tx.commit();
                        //Finalizacion de la sesion de hibernate
                        session.close();

                        //Limpiamos todos los campos
                        limpiarCampos();
                        //Poner en el primer item el comboBox
                        cmbNombreProveedor.setSelectedIndex(0);

                        //Mensaje de confirmacion de insercion Exitosa
                        JOptionPane.showMessageDialog(null, "Registro actualizado exitosamente", "Información", JOptionPane.INFORMATION_MESSAGE);

                    }
                    }
                    else {
                        JOptionPane.showMessageDialog(null, "Error, el telefono debe de contener  8 numeros", "Error en los datos", JOptionPane.ERROR_MESSAGE);
                        //Se limpia la pantalla
                        txtTelefonoProveedor.setText("");

                    }
                }
                //catch (Exception cve ) { 
            catch (org.hibernate.exception.ConstraintViolationException cve) {
                session.getTransaction().rollback();
                JOptionPane.showMessageDialog(null, "El nombre " + nombre + " ya se encuentra asignado a otro proveedor", "Duplicidad en registros", JOptionPane.ERROR_MESSAGE);
                    txtNuevoNombreProveedor.setText("");

                }

            } catch (org.hibernate.JDBCException e) {
                JOptionPane.showMessageDialog(null, "No se pueden actualizar los datos del proveedor " + nombre + " debido a un problema de conexión con la base de datos: ", "Error de conexión", JOptionPane.ERROR_MESSAGE);
            } finally {
                //Se vuelve a cargar el comboBox
                cargarComboBox();
            }
        } else {//Si hay campos vacios, mensaje de error
            JOptionPane.showMessageDialog(null, "Error, no pueden haber campos vacios", "Error en los datos", JOptionPane.ERROR_MESSAGE);
            //Poner en el primer item el comboBox
            cmbNombreProveedor.setSelectedIndex(0);
        }

    }//GEN-LAST:event_btnActualizarProveedorActionPerformed

    private void txtNuevoNombreProveedorKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtNuevoNombreProveedorKeyTyped
 //Validar que el campo solo contenga letras.
          //Validar que el campo solo contenga  letras 
        
        char c;
        c = evt.getKeyChar();
        if (!(c < '0' || c > '9')) {
            evt.consume();
        }

        
    }//GEN-LAST:event_txtNuevoNombreProveedorKeyTyped

    private void txtDireccionProveedorKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtDireccionProveedorKeyTyped
 //Validar que el campo solo contenga numeros, letras y simbolos especiales
        //Variables que se encarga de captura el caracter digitado
        char c;
        c = evt.getKeyChar();
        //Si se ingresa un caracter no valido, se ignora
        if ((c < '0' || c > '9') && (c < 'a' || c > 'z') && (c < 'A' || c > 'Z') && (c != 'ñ') && (c != 'Ñ') && (c != '@') && (c != '$') && (c != '<') && (c != '>') && (c != '_') && (c != '-')) {
            evt.consume(); //ignora el caracter digitado 
        }
    }//GEN-LAST:event_txtDireccionProveedorKeyTyped

    private void txtEmailKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtEmailKeyTyped
  //Validar que el campo solo contenga numeros, letras y simbolos especiales
        //Variables que se encarga de captura el caracter digitado
        char c;
        c = evt.getKeyChar();
        //Si se ingresa un caracter no valido, se ignora
        if ((c < '0' || c > '9') && (c < 'a' || c > 'z') && (c < 'A' || c > 'Z') && (c != 'ñ') && (c != 'Ñ') && (c != '@') && (c != '$') && (c != '<') && (c != '>') && (c != '_') && (c != '-')) {
            evt.consume(); //ignora el caracter digitado 
        }
    }//GEN-LAST:event_txtEmailKeyTyped

    private void txtProductosProveedorKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtProductosProveedorKeyTyped
  //Validar que el campo solo contenga  letras 
        //SOLO LETRAS
        char c;
        c = evt.getKeyChar();
        if (!(c < '0' || c > '9')) {
            evt.consume();
        }

    }//GEN-LAST:event_txtProductosProveedorKeyTyped

    private void txtTelefonoProveedorKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtTelefonoProveedorKeyTyped
        // SOLO NUMEROS
        char c;
//capturar el caracter digitado
        c = evt.getKeyChar();
        if (c < '0' || c > '9') {
            evt.consume();//ignora el caracter digitado 
        }

    }//GEN-LAST:event_txtTelefonoProveedorKeyTyped

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnActualizarProveedor;
    private javax.swing.JButton btnLimpiarProveedor;
    private javax.swing.JComboBox cmbNombreProveedor;
    private javax.swing.JLabel jLabel;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JTextField txtDireccionProveedor;
    private javax.swing.JTextField txtEmail;
    private javax.swing.JTextField txtNuevoNombreProveedor;
    private javax.swing.JTextArea txtProductosProveedor;
    private javax.swing.JTextField txtTelefonoProveedor;
    // End of variables declaration//GEN-END:variables
}

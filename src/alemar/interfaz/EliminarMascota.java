//Interfaz que se encarga de eliminar una Mascota registrada en el sistema
package alemar.interfaz;

import alemar.configuracion.HibernateConexion;
import alemar.entidad.Mascota;
import java.util.List;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

/**
 *
 * @author Teto
 */
public class EliminarMascota extends javax.swing.JPanel {

    //Atributos para el uso de Hibernate
    private static SessionFactory sessionFactory = null;

    public EliminarMascota() {
        initComponents();

        //Se carga el comboBox al inicializar el panel
        cargarComboBox();

    }

    private void cargarComboBox() {

        //Obtencion de todos los proveedores de la base de datos
        //para llenar el comboBox de manera dinamica

        //Variable que contendra la lista de proveedores
        List<Mascota> listaMascota = null;

        //Inicializacion de session de hibernate
        Session session = null;
        try {
            try {
                //Chunces que ocupa Hibernate
                sessionFactory = HibernateConexion.getSessionFactory();
                session = sessionFactory.openSession();
                Transaction tx = session.beginTransaction();

                //Obteniendo de la BD todos los proveedores                
                listaMascota = session.createQuery("from Mascota").list();

                //Creando un modelo de comboBox                
                DefaultComboBoxModel modeloCombo = new DefaultComboBoxModel();

                //Poniendole un mensaje de seleccion (Pirmer elemento)
                modeloCombo.addElement("<-- Seleccione una opción -->");

                //Llenando el modelo de comboBox con los nombres de los proveedores
                for (Mascota p : listaMascota) {
                    modeloCombo.addElement(p.getRaza());
                }

                //Agregando el modelo del comboBox al comboBox del panel
                cmbMascota.setModel(modeloCombo);

            } catch (org.hibernate.JDBCException e) {
                JOptionPane.showMessageDialog(null, "No se puede establecer conexión con la base de datos.", "Error de conexión", JOptionPane.ERROR_MESSAGE);
            }
        } finally {
            session.close();
        }

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        cmbMascota = new javax.swing.JComboBox();
        jLabel2 = new javax.swing.JLabel();
        btnEliminarMascota = new javax.swing.JButton();

        setBackground(new java.awt.Color(255, 255, 255));
        setMaximumSize(new java.awt.Dimension(745, 400));
        setMinimumSize(new java.awt.Dimension(745, 400));
        setPreferredSize(new java.awt.Dimension(745, 400));

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        jLabel1.setText("Eliminar mascota del sistema");

        cmbMascota.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        jLabel2.setText("Raza");

        btnEliminarMascota.setText("Eliminar");
        btnEliminarMascota.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEliminarMascotaActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(267, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addGap(273, 273, 273))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(btnEliminarMascota)
                        .addContainerGap())))
            .addGroup(layout.createSequentialGroup()
                .addGap(227, 227, 227)
                .addComponent(jLabel2)
                .addGap(18, 18, 18)
                .addComponent(cmbMascota, javax.swing.GroupLayout.PREFERRED_SIZE, 240, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(105, 105, 105)
                .addComponent(jLabel1)
                .addGap(80, 80, 80)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cmbMascota, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 141, Short.MAX_VALUE)
                .addComponent(btnEliminarMascota)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnEliminarMascotaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEliminarMascotaActionPerformed
        // TODO add your handling code here:

        int conf;

        String raza = cmbMascota.getSelectedItem().toString();

        if (raza.equalsIgnoreCase("<-- Seleccione una Raza de Mascota -->")) {
            JOptionPane.showMessageDialog(null, "Seleccione una raza", "Selección de datos", JOptionPane.ERROR_MESSAGE);
        } else {

            //Chunche de Hibernate (Inicio de sesion de hibernate)
            Session session = null;
            //Cadena de eliminacion de HQL
            String hql = "delete alemar.entidad.Mascota p where p.raza = :raza";
            try {
                try {
                    //Conexion a la BD
                    sessionFactory = HibernateConexion.getSessionFactory();
                    //Apertura de la sesion
                    session = sessionFactory.openSession();
                    Transaction txt = session.beginTransaction();

                    //Mensaje de pregunta si quiere eliminar el registro 
                    conf = JOptionPane.showConfirmDialog(null, "¿Esta seguro que quiere eliminar esta mascota?\n" + raza, "Confirmación", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);

                    if (conf == JOptionPane.YES_OPTION) {//Si la opcion fue SI                    
                        //Eliminacion de la mascota de la BD en base a la raza
                        int resultado = session.createQuery(hql).setString("raza", raza).executeUpdate();
                        txt.commit();
                        //Mensaje de confirmacion de insercion Exitosa
                        JOptionPane.showMessageDialog(null, "La Mascota " + raza + " ha sido eliminado exitosamente", "Información", JOptionPane.INFORMATION_MESSAGE);
                        cmbMascota.setSelectedIndex(0);
                    }
                } catch (org.hibernate.JDBCException e) {
                    JOptionPane.showMessageDialog(null, " Error: La mascota " + raza + " no pudo ser eliminado debido a un problema de conexión con la base de datos. ", "Error de conexión", JOptionPane.ERROR_MESSAGE);
                }
            } finally {
                //Finalizacion de la sesion de hibernate
                session.close();
                //Se carga nuevamente el comboBox
                cargarComboBox();
            }
        }

    }//GEN-LAST:event_btnEliminarMascotaActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnEliminarMascota;
    private javax.swing.JComboBox cmbMascota;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    // End of variables declaration//GEN-END:variables
}

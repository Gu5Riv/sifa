//Interfaz que se encarga de eliminar Proveeedores registrados en el sistema
package alemar.interfaz;

import alemar.clases.CargarCMBProveedor;
import alemar.configuracion.HibernateConexion;
import alemar.entidad.Proveedor;
import java.util.List;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

public class EliminarProveedor extends javax.swing.JPanel {

    //Atributos para el uso de Hibernate
    private static SessionFactory sessionFactory = null;
     //Lista que contendra los usuarios registrados en el sistema
    List<Proveedor> listaProveedor = null;

    public EliminarProveedor() {
        initComponents();
        //Se carga el combo box con los Proveedores registrados en el sistema
        cargarComboBox();
    }

   //Metodos de la clase
    //Metodo que se encarga de cargar los Proveedores registrados en el sistema en un combo box
    private void cargarComboBox() {
//Se inicializa la lista a null
        listaProveedor = null;

        //Instanciamos la clase CargarCMBUsuario para cargar todos los usuarios registrados en una lista
        CargarCMBProveedor cargarProveedores = new CargarCMBProveedor();
        listaProveedor = cargarProveedores.cargarProveedores();

        //Creando un modelo de comboBox                
        DefaultComboBoxModel modeloCombo = new DefaultComboBoxModel();

        //Poniendole un mensaje de seleccion (Primer elemento) al combo box
        modeloCombo.addElement("<--Seleccione un Proveedor-->");

        //Llenando el modelo de comboBox con los nombres de los usuarios
        for (Proveedor proveedor : listaProveedor) {
            //Si el usuario es el superadministrador no lo cargamos
            
                modeloCombo.addElement(proveedor.getNombre());
            
        }
        //Agregando el modelo del comboBox al comboBox del panel
        cmbNombreProveedor.setModel(modeloCombo);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnEliminarProveedor = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        cmbNombreProveedor = new javax.swing.JComboBox();
        jLabel2 = new javax.swing.JLabel();

        setBackground(new java.awt.Color(255, 255, 255));
        setMaximumSize(new java.awt.Dimension(745, 400));
        setMinimumSize(new java.awt.Dimension(745, 400));
        setPreferredSize(new java.awt.Dimension(745, 400));

        btnEliminarProveedor.setText("Eliminar");
        btnEliminarProveedor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEliminarProveedorActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        jLabel1.setText("Eliminar proveedor del sistema");

        cmbNombreProveedor.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        jLabel2.setText("Nombre del proveedor");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(178, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(btnEliminarProveedor)
                        .addContainerGap())
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addComponent(cmbNombreProveedor, javax.swing.GroupLayout.PREFERRED_SIZE, 240, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(214, 214, 214))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(113, 113, 113)
                .addComponent(jLabel1)
                .addGap(55, 55, 55)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cmbNombreProveedor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 158, Short.MAX_VALUE)
                .addComponent(btnEliminarProveedor)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnEliminarProveedorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEliminarProveedorActionPerformed

         //Variable que guardara el nombre de Proveedor
         String nombre;
         //Variable que contedra la confirmación del usuario para eleminar los datos
        int conf;
        //Se extrae el valor seleccionado en el combobox
        nombre = cmbNombreProveedor.getSelectedItem().toString();
        
         //Si se selecciona el primer valor del combobox no se elimina nada
        if (nombre.equalsIgnoreCase("<--Seleccione un proveedor-->")) {
            JOptionPane.showMessageDialog(null, "Seleccione un nombre de proveedor", "Selección de datos", JOptionPane.ERROR_MESSAGE);
        } else {//Se elmina el usuario

            //Inicio de sesion en Hibernate
            Session session = null;
            //Cadena de eliminacion de HQL
            String hql = "delete alemar.entidad.Proveedor p where p.nombre = :nombre";
            try {
                try {
                    //Conexion a la BD
                    sessionFactory = HibernateConexion.getSessionFactory();
                    //Apertura de la sesion
                    session = sessionFactory.openSession();
                    Transaction txt = session.beginTransaction();
                    //Mensaje de pregunta si quiere eliminar el registro 
                    conf = JOptionPane.showConfirmDialog(null, "¿Esta seguro que quiere eliminar este proveedor?\n" + nombre, "Confirmación", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);

                    if (conf == JOptionPane.YES_OPTION) {//Si la opcion fue SI Eliminacion del proveedor de la BD en base al nombre
                        //Eliminacion del Usuario de la BD en base al nombre de usuario
                        session.createQuery(hql).setString("nombre", nombre).executeUpdate();
                        txt.commit();
                        //Mensaje de confirmacion de insercion Exitosa
                        JOptionPane.showMessageDialog(null, "El proveedor " + nombre + " ha sido eliminado exitosamente", "Información", JOptionPane.INFORMATION_MESSAGE);
                        //Finalizacion de la sesion de hibernate
                        session.close();
                    }
                } catch (org.hibernate.JDBCException e) {
                    JOptionPane.showMessageDialog(null, " Error: El Proveedor " + nombre + " no pudo ser eliminado debido a un problema de conexión con la base de datos. ", "Error de conexión", JOptionPane.ERROR_MESSAGE);
                }
            } finally {
                ///Se carga nuevamente el comboBox
                cargarComboBox();
                //Finalizacion de la sesion de hibernate
                        session.close();
            }
        }

    }//GEN-LAST:event_btnEliminarProveedorActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnEliminarProveedor;
    private javax.swing.JComboBox cmbNombreProveedor;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    // End of variables declaration//GEN-END:variables
}

//Interfaz de logeo y cambio de sesión de usuarios
package alemar.interfaz;

import alemar.clases.CifradorCesar;
import alemar.clases.RelojVisual;
import alemar.clases.VariablesGlobales;
import alemar.configuracion.HibernateConexion;
import alemar.entidad.Usuario;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.List;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.jvnet.substance.SubstanceLookAndFeel;

public class CambiarUsuario extends javax.swing.JDialog {

    //Atributos de la clase
    //Variables para inicio de sesión en Hibernate
    private static SessionFactory sessionFactory = null;
    //Variable que limita el numero maximo de caracteres
    private int limMaximo = 20;
    //Variable que verifica si el bloq mayus esta activado
    private boolean estadoBM;

    public CambiarUsuario(java.awt.Frame parent, boolean modal) {

        //Se carga el panel de login con los elementos de inicio necesarios y se inician los componentes
        super(parent, modal);
        JDialog.setDefaultLookAndFeelDecorated(true);
        SubstanceLookAndFeel.setSkin("org.jvnet.substance.skin.OfficeSilver2007Skin");
        initComponents();
        super.setLocationRelativeTo(null);

        //Ocultamos el label que muestra si el bloq mayus esta activado 
        jLabel5.setVisible(false);

        //Limitamos el numero maximo de caracteres para el nombre de usuario
        txtNombreUsuario.addKeyListener(new KeyListener() {
            public void keyTyped(KeyEvent e) {
                if (txtNombreUsuario.getText().length() == limMaximo) {
                    e.consume();
                }
            }

            public void keyPressed(KeyEvent arg0) {
            }

            public void keyReleased(KeyEvent arg0) {
            }
        });

        //Limitamos el numero maximo de caracteres para la contraseña
        txtContraseña.addKeyListener(new KeyListener() {
            public void keyTyped(KeyEvent e) {
                if (txtContraseña.getText().length() == limMaximo) {
                    e.consume();
                }
            }

            public void keyPressed(KeyEvent arg0) {
                //Verificamos si el Bloq Mayus esta activado
                estadoBM = Toolkit.getDefaultToolkit().getLockingKeyState(KeyEvent.VK_CAPS_LOCK);

                //Si esta activo mostramos el mensaje
                if (estadoBM == true) {
                    jLabel5.setVisible(true);
                } else {
                    jLabel5.setVisible(false);
                }
            }

            public void keyReleased(KeyEvent arg0) {
            }
        });

        //Se agregan los mensajes de ayuda para el llenado de los campos del panel de login
        txtNombreUsuario.setToolTipText("Ingrese el nombre de un usuario registrado en el sistema");
        txtContraseña.setToolTipText("Ingrese la contraseña del usuario");

        //Se intancia la clase VisualReloj par agregar la fecha y el reloj en el sistema
        RelojVisual reloj = new RelojVisual();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        txtContraseña = new javax.swing.JPasswordField();
        btnCancelar = new javax.swing.JButton();
        btnAcceder = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        txtNombreUsuario = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setAutoRequestFocus(false);
        setPreferredSize(new java.awt.Dimension(485, 247));
        setResizable(false);

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Cambiar Usuario");

        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/alemar/imagen/Cambiar  sesion.png"))); // NOI18N

        jLabel3.setText("Nombre de usuario");

        jLabel4.setText("Contraseña");

        txtContraseña.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtContraseñaFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtContraseñaFocusLost(evt);
            }
        });

        btnCancelar.setText("Cerrar");
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });

        btnAcceder.setText("Accesar");
        btnAcceder.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAccederActionPerformed(evt);
            }
        });

        jPanel1.setBackground(new java.awt.Color(204, 204, 204));

        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel5.setText("Bloq. Mayus. Activado");

        jLabel6.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 176, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel6, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jLabel5, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jSeparator1)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 123, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(52, 52, 52)
                                .addComponent(jLabel4))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(18, 18, 18)
                                .addComponent(jLabel3)))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(btnCancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 88, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 20, Short.MAX_VALUE)
                                .addComponent(btnAcceder, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(txtContraseña)
                            .addComponent(txtNombreUsuario))))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 6, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 30, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel3)
                            .addComponent(txtNombreUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtContraseña, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel4))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 33, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(btnCancelar, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(btnAcceder, javax.swing.GroupLayout.Alignment.TRAILING))
                        .addGap(13, 13, 13))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 124, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)))
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed

        //Salir del sistema
        this.dispose();

    }//GEN-LAST:event_btnCancelarActionPerformed

    private void btnAccederActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAccederActionPerformed

        //Recuperar de los campos de texto los datos ingresados por el usuarios

        //Variables que contendran los datos ingresados por el usuario
        String nombreUsuario;
        String contraseña;
        boolean usuarioRegistrado = false; //Variable que determina si un usuario esta registrado en el sistema (originalemnte es falso)

        //Se obtienen los valores de los campos
        nombreUsuario = txtNombreUsuario.getText();
        contraseña = txtContraseña.getText();

        //Comparar los datos ingresados por el usuario para ver si coinciden a los datos de un usuario registrado

        //Si ninguno de los campos estan vacios
        if (!(nombreUsuario.isEmpty() || contraseña.isEmpty() || nombreUsuario.equals("Ej: Admin-1"))) {
            //Recuperamos todos los usuarios registrados en la base de datos para compararlos con los valores ingresados por el usuario

            //Lista que contendra los registros de cada uno de los usuarios registrados en la base de datos
            List<Usuario> usuariosLemar;

            //Variable que inicializa sesion en hibernate
            Session session = null;
            try {
                //Se crea la conexion con la base de datos
                sessionFactory = HibernateConexion.getSessionFactory();
                session = sessionFactory.openSession();
                Transaction tx = session.beginTransaction();

                //Se extraen los datos de todos los usuarios registrados en el sistema y se compraran con los datos ingresados por el usuario
                usuariosLemar = session.createQuery("from Usuario").list();
                for (Usuario usuarios : usuariosLemar) {
                    //Si el nombre de usuario ingresado coincide con el nombre de un usuario registrado verificamos si la contraseña tambien coincide
                    if (usuarios.getNombreUsuario().equals(nombreUsuario)) {

                        //Desencriptamos los datos
                        CifradorCesar dc = new CifradorCesar(usuarios.getPassword(), 4);
                        dc.decodCesar();
                        String contraseñaDecodificada = dc.getDecodificada();

                        //Verificamos si la contraseña coincide con la contraseña del usuario registrado
                        if (contraseñaDecodificada.equals(contraseña)) {
                            //El usuario es un usuario registrado por lo tanto permitimos su acceso cambiando el valor de la variable de acceso
                            usuarioRegistrado = true;
                            //Guardamos el nombre de usuario y el rol del usuario para dar acceso unicamente a las pestañas a las cuales posea acceso segun su cuenta de usuario
                            VariablesGlobales.nombreUsuario = usuarios.getNombreUsuario();
                            VariablesGlobales.rol = usuarios.getTipoUsuario();
                            //Colocamos el nombre y el rol del usuario que se encuentra logueado
                            alemar.interfaz.Principal.jLabel2.setText("Usuario:  " + VariablesGlobales.nombreUsuario);
                            alemar.interfaz.Principal.jLabel3.setText("Rol:  " + VariablesGlobales.rol);
                            break; //Para que finalice el ciclo y no realice mas comparaciones
                        }
                    }
                }

                //Si el usuario fue entontrado entonces se deja acceder al usuario al sistema
                if (usuarioRegistrado == true) {
                    this.dispose();
                } else //Indicamos al usuario que los datos ingresados no corresponden a los datos de un usuario registrado
                {
                    JOptionPane.showMessageDialog(null, "Los datos ingresados no coinciden con las credenciales de un usuario registrado\nVuelva a intentarlo", "Error de autorización", JOptionPane.ERROR_MESSAGE);
                    //Se limpian los campos de texto
                    txtNombreUsuario.setText("");
                    txtContraseña.setText("");
                }

                //Finalizacion de la sesion de hibernate
                session.close();

            } catch (org.hibernate.JDBCException e) { //Manejo de las excepciones de hibernate
                JOptionPane.showMessageDialog(null, "No se puedo establecer la conexión con la base de datos", "Error de conexión", JOptionPane.ERROR_MESSAGE);
            }
        } else //Si alguno de los campos se encuentra vacio
        {
            JOptionPane.showMessageDialog(null, "Error, no deben de haber campos vacios", "Error en los datos", JOptionPane.ERROR_MESSAGE);
            txtNombreUsuario.setText("");
            txtContraseña.setText("");
        }

    }//GEN-LAST:event_btnAccederActionPerformed

    private void txtContraseñaFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtContraseñaFocusGained

        //Verificamos si el Bloq Mayus esta activado
        estadoBM = Toolkit.getDefaultToolkit().getLockingKeyState(KeyEvent.VK_CAPS_LOCK);

        //Si esta activo mostramos el mensaje
        if (estadoBM == true) {
            jLabel5.setVisible(true);
        } else {
            jLabel5.setVisible(false);
        }

    }//GEN-LAST:event_txtContraseñaFocusGained

    private void txtContraseñaFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtContraseñaFocusLost

        //Ocultamos el jLabel6
        jLabel5.setVisible(false);

    }//GEN-LAST:event_txtContraseñaFocusLost
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAcceder;
    private javax.swing.JButton btnCancelar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    public static javax.swing.JLabel jLabel6;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JPasswordField txtContraseña;
    private javax.swing.JTextField txtNombreUsuario;
    // End of variables declaration//GEN-END:variables
}
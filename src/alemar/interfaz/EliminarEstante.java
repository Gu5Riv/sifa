//Interfaz que se encarga de eliminar un Estante  registrado en el sistema
package alemar.interfaz;

import alemar.configuracion.HibernateConexion;
import alemar.entidad.Estante;
import java.util.List;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

/**
 *
 * @author Teto
 */
public class EliminarEstante extends javax.swing.JPanel {

    //Atributos para el uso de Hibernate
    private static SessionFactory sessionFactory = null;
    
    public EliminarEstante() {
       initComponents();
        
        //Se carga el comboBox al inicializar el panel
        cargarComboBox();
        
    }
    
    private void cargarComboBox(){
        
        //Obtencion de todos los proveedores de la base de datos
        //para llenar el comboBox de manera dinamica
        
        //Variable que contendra la lista de proveedores
        List<Estante> listaEstante = null;
        
        //Inicializacion de session de hibernate
        Session session = null;
        
            try{
                //Chunces que ocupa Hibernate
                sessionFactory = HibernateConexion.getSessionFactory();
                session = sessionFactory.openSession();
                Transaction tx = session.beginTransaction();
                
                //Obteniendo de la BD todos los proveedores                
                listaEstante = session.createQuery("from Estante").list();
                
                //Creando un modelo de comboBox                
                DefaultComboBoxModel modeloCombo = new DefaultComboBoxModel();
                
                //Poniendole un mensaje de seleccion (Pirmer elemento)
                 modeloCombo.addElement("<--Seleccione un estante-->");
                
                //Llenando el modelo de comboBox con los nombres de los proveedores
                for(Estante p : listaEstante) 
                { 
                    modeloCombo.addElement(p.getCodigoEstante());                     
                }
                
                //Agregando el modelo del comboBox al comboBox del panel
                cmbCodigoEstante.setModel(modeloCombo);
                
            }catch (org.hibernate.JDBCException e) {
                JOptionPane.showMessageDialog(null, "No se puede establecer conexión con la base de datos.", "Error de conexión", JOptionPane.ERROR_MESSAGE);
            }finally {
                session.close();
        }
        
    }
   
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        cmbCodigoEstante = new javax.swing.JComboBox();
        jLabel2 = new javax.swing.JLabel();
        btnEliminarEstante = new javax.swing.JButton();

        setBackground(new java.awt.Color(255, 255, 255));
        setMaximumSize(new java.awt.Dimension(745, 400));
        setMinimumSize(new java.awt.Dimension(745, 400));
        setPreferredSize(new java.awt.Dimension(745, 400));

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        jLabel1.setText("Eliminar estante del sistema");

        cmbCodigoEstante.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        jLabel2.setText("Código del estante");

        btnEliminarEstante.setText("Eliminar");
        btnEliminarEstante.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEliminarEstanteActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(226, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addGap(18, 18, 18)
                        .addComponent(cmbCodigoEstante, javax.swing.GroupLayout.PREFERRED_SIZE, 240, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(171, 171, 171))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addGap(273, 273, 273))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(btnEliminarEstante)
                        .addContainerGap())))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(105, 105, 105)
                .addComponent(jLabel1)
                .addGap(48, 48, 48)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cmbCodigoEstante, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 173, Short.MAX_VALUE)
                .addComponent(btnEliminarEstante)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnEliminarEstanteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEliminarEstanteActionPerformed
        // TODO add your handling code here:
        
        String codigoEstante = "";
        int conf;
        
        codigoEstante = cmbCodigoEstante.getSelectedItem().toString();
        
        if(codigoEstante.equalsIgnoreCase("<--Seleccione un estante-->")){
            JOptionPane.showMessageDialog(null,"Seleccione un código de estante","Selección de datos",JOptionPane.ERROR_MESSAGE);
        }
        else{
            
            //Chunche de Hibernate (Inicio de sesion de hibernate)
            Session session = null;
            //Cadena de eliminacion de HQL
            String hql = "delete alemar.entidad.Estante p where p.codigoEstante = :codigoEstante";
            try{
                try{
                    //Conexion a la BD
                    sessionFactory = HibernateConexion.getSessionFactory();
                    //Apertura de la sesion
                    session = sessionFactory.openSession();
                    Transaction txt = session.beginTransaction();
                    
                    //Mensaje de pregunta si quiere eliminar el registro 
                    conf=JOptionPane.showConfirmDialog(null,"¿Esta seguro que quiere eliminar este estante?\n" + codigoEstante,"Confirmación",JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE);
                    
                    if(conf == JOptionPane.YES_OPTION){//Si la opcion fue SI                    
                        //Eliminacion del proveedor de la BD en base al codigo_estante
                        int resultado = session.createQuery(hql).setString("codigoEstante",codigoEstante).executeUpdate();
                        txt.commit();
                        //Mensaje de confirmacion de insercion Exitosa
                        JOptionPane.showMessageDialog(null,"El estante "+ codigoEstante +" ha sido eliminado exitosamente","Información",JOptionPane.INFORMATION_MESSAGE);                        
                        cmbCodigoEstante.setSelectedIndex(0);
                          //Finalizacion de la sesion de hibernate
                        session.close();
                     }
                    }catch (org.hibernate.JDBCException e) {
                    JOptionPane.showMessageDialog(null, " Error: El Estante " + codigoEstante + " no pudo ser eliminado debido a un problema de conexión con la base de datos. ", "Error de conexión", JOptionPane.ERROR_MESSAGE);
                }
            } finally {
                //Finalizacion de la sesion de hibernate
                session.close();
                //Se carga nuevamente el comboBox
                cargarComboBox();
           }
      }
        
    }//GEN-LAST:event_btnEliminarEstanteActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnEliminarEstante;
    private javax.swing.JComboBox cmbCodigoEstante;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    // End of variables declaration//GEN-END:variables
}

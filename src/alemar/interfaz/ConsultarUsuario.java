//Interfaz que se encarga de consultar los datos de un usuarios previamente registrado
package alemar.interfaz;

import alemar.clases.CargarCMBUsuario;
import java.util.List;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;
import alemar.configuracion.HibernateConexion;
import alemar.clases.CifradorCesar;
import alemar.entidad.Usuario;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

public class ConsultarUsuario extends javax.swing.JPanel {
    //Atributos de la clase

    //Atributos para el uso de Hibernate
    private static SessionFactory sessionFactory = null;
    //Lista que contendra los usuarios registrados en el sistema
    List<Usuario> listaUsuario = null;

    public ConsultarUsuario() {
        initComponents();
        //Se carga el comboBox Dinamicamente de la BD
        cargarComboBox();
    }

    //Metodos de la clase
    //Metodo que se encarga de cargar los usuarios registrados en el sistema en un combo box
    private void cargarComboBox() {
        //Se inicializa la lista a null
        listaUsuario = null;

        //Instanciamos la clase CargarCMBUsuario para cargar todos los usuarios registrados en una lista
        CargarCMBUsuario cargarUsuarios = new CargarCMBUsuario();
        listaUsuario = cargarUsuarios.cargarUsuarios();

        //Creando un modelo de comboBox                
        DefaultComboBoxModel modeloCombo = new DefaultComboBoxModel();

        //Poniendole un mensaje de seleccion (Primer elemento) al combo box
        modeloCombo.addElement("<--Seleccione un usuario-->");

        //Llenando el modelo de comboBox con los nombres de los usuarios
        for (Usuario usuario : listaUsuario) {
            //Si el usuario es el superadministrador no lo cargamos
            if (!usuario.getNombreUsuario().equals("SUPERADMIN")) {
                modeloCombo.addElement(usuario.getNombreUsuario());
            }
        }
        //Agregando el modelo del comboBox al comboBox del panel
        cmbNombreUsuario.setModel(modeloCombo);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        cmbNombreUsuario = new javax.swing.JComboBox();
        jSeparator1 = new javax.swing.JSeparator();
        jLabel3 = new javax.swing.JLabel();
        txtContraseña = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        txtTipoUsuario = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();

        setBackground(new java.awt.Color(255, 255, 255));
        setMaximumSize(new java.awt.Dimension(794, 432));
        setMinimumSize(new java.awt.Dimension(794, 432));
        setPreferredSize(new java.awt.Dimension(794, 432));

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Consultar datos de usuario");

        jLabel2.setText("Nombre del Usuario ");

        cmbNombreUsuario.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        cmbNombreUsuario.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbNombreUsuarioItemStateChanged(evt);
            }
        });

        jSeparator1.setMaximumSize(new java.awt.Dimension(40, 10));
        jSeparator1.setMinimumSize(new java.awt.Dimension(40, 10));
        jSeparator1.setPreferredSize(new java.awt.Dimension(40, 10));

        jLabel3.setText("Contraseña");

        txtContraseña.setEditable(false);

        jLabel4.setText("Tipo de usuario");

        txtTipoUsuario.setEditable(false);

        jButton1.setText("Consultar");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnConsUsuaActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addComponent(jButton1))
                            .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(197, 197, 197)
                                .addComponent(jLabel2)
                                .addGap(26, 26, 26)
                                .addComponent(cmbNombreUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, 237, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(219, 219, 219)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel3)
                                    .addComponent(jLabel4))
                                .addGap(27, 27, 27)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(txtContraseña)
                                    .addComponent(txtTipoUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, 238, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
            .addGroup(layout.createSequentialGroup()
                .addGap(146, 146, 146)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 493, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(155, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(99, Short.MAX_VALUE)
                .addComponent(jLabel1)
                .addGap(54, 54, 54)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(cmbNombreUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtContraseña, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(txtTipoUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(99, 99, 99)
                .addComponent(jButton1)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnConsUsuaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnConsUsuaActionPerformed

        //Variables que contendran los valores de los campos
        String nombreUsuario;

        //Se extrae el nombre del usuario seleccionado
        nombreUsuario = cmbNombreUsuario.getSelectedItem().toString();

        if (nombreUsuario.equalsIgnoreCase("<--Seleccione un usuario-->")) {
            JOptionPane.showMessageDialog(null, "Seleccione un nombre de usuario", "Selección de datos", JOptionPane.ERROR_MESSAGE);
        } else {
            //Inicio de sesion de hibernate
            Session session = null;

            //Cadena de consulta en base a nombre de usuario de hql
            String hql = "FROM alemar.entidad.Usuario AS usuario WHERE usuario.nombreUsuario = :nombreUsuario";

            try {
                //Conexion a la BD
                sessionFactory = HibernateConexion.getSessionFactory();
                //Apertura de la sesion
                session = sessionFactory.openSession();
                Transaction txt = session.beginTransaction();

                //Extraccion de los datos del usuario de la BD                    
                Query query = session.createQuery(hql).setString("nombreUsuario", nombreUsuario);

                //Se extrae los registros del usuario
                listaUsuario = query.list();

                //Desencriptando la contraseña
                CifradorCesar cc2 = new CifradorCesar(listaUsuario.get(0).getPassword(), 4);
                cc2.decodCesar();
                String contraseñaDesifrada = cc2.getDecodificada();

                //Se ponen los datos del usuario en los campos de texto para su respectiva consulta
                txtContraseña.setText(contraseñaDesifrada);
                txtTipoUsuario.setText(listaUsuario.get(0).getTipoUsuario());
                txt.commit();
                //Finalizacion de la sesion de hibernate
                session.close();
                
            } catch (org.hibernate.JDBCException e) {
                JOptionPane.showMessageDialog(null, "Los datos del usuario " + nombreUsuario + " no pueden ser consultados debido a un problema de conexión con la base de datos.", "Error de conexión", JOptionPane.ERROR_MESSAGE);
            }
        }
    }//GEN-LAST:event_btnConsUsuaActionPerformed

    private void cmbNombreUsuarioItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbNombreUsuarioItemStateChanged

       //Limpio todos los campos 
       txtContraseña.setText("");
       txtTipoUsuario.setText("");
              
    }//GEN-LAST:event_cmbNombreUsuarioItemStateChanged
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox cmbNombreUsuario;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JTextField txtContraseña;
    private javax.swing.JTextField txtTipoUsuario;
    // End of variables declaration//GEN-END:variables
}

//Super dragon

//Interfaz que se encarga de actualizar los datos de un usuario registrado en el sistema
package alemar.interfaz;

import alemar.clases.CargarCMBUsuario;
import alemar.clases.CifradorCesar;
import alemar.configuracion.HibernateConexion;
import alemar.entidad.Usuario;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.List;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

public class ActualizarUsuario extends javax.swing.JPanel {
    //Atributos de la clase

    //Atributo para el uso de Hibernate
    private static SessionFactory sessionFactory = null;
    //Variables que contienen el limite minimo y maximo para el nombre del usuario y la contraseña
    private int limMinimo = 6;
    private int limMaximo = 20;
    //Lista que contendra los usuarios registrados en el sistema
    List<Usuario> listaUsuario = null;
    //Para guardar el id del usuario
    private int id;
    //Variable que contendra el estado del bloc mayuscula
    private boolean estadoBM;

    public ActualizarUsuario() {
        initComponents();

        //Se carga el combobox con los datos de los usuarios registaros en la base de datos
        cargarComboBox();

        //Por default los campos estaran desabilitados hasta que se seleccione el nombre de un usuario
        txtNuevoNombreUsuario.setEditable(false);
        txtContraseñaUsuario.setEditable(false);
        txtConfirmarContraseña.setEditable(false);
        cmbTipoUsuario.setEnabled(false);

        //Ocultamos el label que indica si la contraseña esta o no en mayuscula
        jLabel9.setVisible(false);

        //Agregar mensajes de ayuda para el llenado de los campos
        txtContraseñaUsuario.setToolTipText("Ingrese la contraseña del usuario (Min. 6 caracteres). Unicámente letras, numeros y simbolos (@,$,<,>,_,-)");
        txtConfirmarContraseña.setToolTipText("Repita la contraseña del usuario");

        //Limitamos el numero maximo de caracteres para la contraseña y a su vez comprobamos el nivel de seguridad de la misma
        txtContraseñaUsuario.addKeyListener(new KeyListener() {
            public void keyTyped(KeyEvent e) {
                //Se elimina el teclado al llegar al valor maximo 
                if (txtContraseñaUsuario.getText().length() == limMaximo) {
                    e.consume();
                }
            }

            public void keyPressed(KeyEvent arg0) {
                //Verificamos si el Bloq Mayus esta activado
                estadoBM = Toolkit.getDefaultToolkit().getLockingKeyState(KeyEvent.VK_CAPS_LOCK);

                //Si esta activo mostramos el mensaje
                if (estadoBM == true) {
                    jLabel9.setVisible(true);
                } else {
                    jLabel9.setVisible(false);
                }
            }

            public void keyReleased(KeyEvent arg0) {

                //Verificar el nivel de seguriad de la contraseña

                //Si la contraseña esta vacia
                if (txtContraseñaUsuario.getText().length() == 0) {
                    jLabel8.setText("");
                    barraContraseña.setValue(0); //Poner el porcentaje de la barra en 0
                }
                //Si la contraseña tiene un valor menor a 6 caracteres
                if (txtContraseñaUsuario.getText().length() > 0 && txtContraseñaUsuario.getText().length() <= 5) {
                    jLabel8.setText("Inválida");
                    barraContraseña.setValue(0); //Poner el porcentaje de la barra en 0
                }
                //Si la contraseña tiene un valor entre 6 y 9 caracteres
                if (txtContraseñaUsuario.getText().length() > 5 && txtContraseñaUsuario.getText().length() <= 9) {
                    jLabel8.setText("Baja");
                    barraContraseña.setValue(33); //Poner el porcentaje de la barra en 33%
                }

                //Si la contraseña tiene un valor entre 10 y 14 caracteres
                if (txtContraseñaUsuario.getText().length() >= 10 && txtContraseñaUsuario.getText().length() <= 14) {
                    jLabel8.setText("Media");
                    barraContraseña.setValue(66); //Poner el porcentaje de la barra en 66%
                }

                //Si la contraseña tiene un valor mayor a 15 caracteres
                if (txtContraseñaUsuario.getText().length() >= 15) {
                    jLabel8.setText("Alta");
                    barraContraseña.setValue(100); //Poner el porcentaje de la barra en 100%
                }
            }
        });

        txtConfirmarContraseña.addKeyListener(new KeyListener() {
            public void keyTyped(KeyEvent e) {
                if (txtConfirmarContraseña.getText().length() == limMaximo) {
                    e.consume();
                }
            }

            public void keyPressed(KeyEvent arg0) {
                //Verificamos si el Bloq Mayus esta activado
                estadoBM = Toolkit.getDefaultToolkit().getLockingKeyState(KeyEvent.VK_CAPS_LOCK);

                //Si esta activo mostramos el mensaje
                if (estadoBM == true) {
                    jLabel9.setVisible(true);
                } else {
                    jLabel9.setVisible(false);
                }
            }

            public void keyReleased(KeyEvent arg0) {
            }
        });
    }

    //Metodos de la clase
    
    //Metodo que se encarga de cargar los usuarios registrados en el sistema en un combo box
    private void cargarComboBox() {
        //Se inicializa la lista a null
        listaUsuario = null;

        //Instanciamos la clase CargarCMBUsuario para cargar todos los usuarios registrados en una lista
        CargarCMBUsuario cargarUsuarios = new CargarCMBUsuario();
        listaUsuario = cargarUsuarios.cargarUsuarios();
        
        //Creando un modelo de comboBox                
        DefaultComboBoxModel modeloCombo = new DefaultComboBoxModel();

        //Poniendole un mensaje de seleccion (Primer elemento) al combo box
        modeloCombo.addElement("<--Seleccione un usuario-->");

        //Llenando el modelo de comboBox con los nombres de los usuarios
        for (Usuario usuario : listaUsuario) {
            //Si el usuario es el superadministrador no lo cargamos
            if (!usuario.getNombreUsuario().equals("SUPERADMIN")) {
                modeloCombo.addElement(usuario.getNombreUsuario());
            }
        }
        //Agregando el modelo del comboBox al comboBox del panel
        cmbNombreUsuario.setModel(modeloCombo);
    }

    //Metodo que limpia todos los campos de la interfaz
    private void limpiarCampos() {
        txtNuevoNombreUsuario.setText("");
        txtContraseñaUsuario.setText("");
        txtContraseñaUsuario.setEditable(false);
        txtConfirmarContraseña.setText("");
        txtConfirmarContraseña.setEditable(false);
        cmbTipoUsuario.setSelectedIndex(0);
        cmbTipoUsuario.setEnabled(false);
        barraContraseña.setValue(0);
        jLabel8.setText("");
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnActualizarUsuario = new javax.swing.JButton();
        btnLimpiarUsuario = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        cmbNombreUsuario = new javax.swing.JComboBox();
        jLabel2 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        txtNuevoNombreUsuario = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        txtContraseñaUsuario = new javax.swing.JPasswordField();
        txtConfirmarContraseña = new javax.swing.JPasswordField();
        jLabel5 = new javax.swing.JLabel();
        cmbTipoUsuario = new javax.swing.JComboBox();
        jLabel6 = new javax.swing.JLabel();
        barraContraseña = new javax.swing.JProgressBar();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();

        setBackground(new java.awt.Color(255, 255, 255));
        setMaximumSize(new java.awt.Dimension(794, 432));
        setMinimumSize(new java.awt.Dimension(794, 432));
        setPreferredSize(new java.awt.Dimension(794, 432));

        btnActualizarUsuario.setText("Actualizar");
        btnActualizarUsuario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnActualizarUsuarioActionPerformed(evt);
            }
        });

        btnLimpiarUsuario.setText("Limpiar");
        btnLimpiarUsuario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLimpiarUsuarioActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Actualizar información de usuario");

        cmbNombreUsuario.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        cmbNombreUsuario.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbNombreUsuarioItemStateChanged(evt);
            }
        });

        jLabel2.setText("Nombre de usuario");

        txtNuevoNombreUsuario.setBackground(new java.awt.Color(238, 253, 253));

        jLabel3.setText("Nombre de usuario");

        jLabel4.setText("Nueva contraseña");

        txtContraseñaUsuario.setBackground(new java.awt.Color(238, 253, 253));
        txtContraseñaUsuario.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtContraseñaUsuarioFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtContraseñaUsuarioFocusLost(evt);
            }
        });
        txtContraseñaUsuario.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtContraseñaUsuarioKeyTyped(evt);
            }
        });

        txtConfirmarContraseña.setBackground(new java.awt.Color(238, 253, 253));
        txtConfirmarContraseña.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtConfirmarContraseñaFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtConfirmarContraseñaFocusLost(evt);
            }
        });
        txtConfirmarContraseña.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtConfirmarContraseñaKeyTyped(evt);
            }
        });

        jLabel5.setText("Confirmar contraseña");

        cmbTipoUsuario.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "<--Seleccione un usuario-->", "Administrador", "Gerente de ventas", "Encargado de bodega", "Vendedor", "Cajero" }));

        jLabel6.setText("Rol");

        jLabel7.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel7.setText("Nivel de seguridad");
        jLabel7.setToolTipText("");

        jLabel8.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel8.setMaximumSize(new java.awt.Dimension(21, 22));
        jLabel8.setMinimumSize(new java.awt.Dimension(21, 22));
        jLabel8.setPreferredSize(new java.awt.Dimension(21, 22));

        jLabel9.setText("Bloq. Mayus. Activado");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(24, 24, 24)
                        .addComponent(jLabel9)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnLimpiarUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnActualizarUsuario))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(65, 65, 65)
                        .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 666, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(53, 53, 53))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addGap(31, 31, 31)
                        .addComponent(cmbNombreUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, 240, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(211, 211, 211))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel3)
                                    .addComponent(jLabel4)
                                    .addComponent(jLabel5))
                                .addGap(29, 29, 29)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txtConfirmarContraseña, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 240, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txtContraseñaUsuario, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 240, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txtNuevoNombreUsuario, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 240, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel6)
                                .addGap(29, 29, 29)
                                .addComponent(cmbTipoUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, 240, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(jLabel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(barraContraseña, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                            .addComponent(jLabel8, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 134, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(133, 133, 133))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(61, 61, 61)
                .addComponent(jLabel1)
                .addGap(33, 33, 33)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cmbNombreUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addGap(18, 18, 18)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(32, 32, 32)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtNuevoNombreUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(barraContraseña, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txtContraseñaUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel4)))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addComponent(jLabel7))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtConfirmarContraseña, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel5))))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cmbTipoUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 70, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnActualizarUsuario)
                    .addComponent(btnLimpiarUsuario)
                    .addComponent(jLabel9))
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void txtConfirmarContraseñaKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtConfirmarContraseñaKeyTyped

        //Validar que el campo solo contenga numeros, letras y simbolos especiales

        //Variables que se encarga de captura el caracter digitado
        char c;
        c = evt.getKeyChar();

        //Si se ingresa un caracter no valido, se ignora
        if ((c < '0' || c > '9') && (c < 'a' || c > 'z') && (c < 'A' || c > 'Z') && (c != 'ñ') && (c != 'Ñ') && (c != '@') && (c != '$') && (c != '<') && (c != '>') && (c != '_') && (c != '-')) {
            evt.consume(); //ignora el caracter digitado 
        }

    }//GEN-LAST:event_txtConfirmarContraseñaKeyTyped

    private void txtContraseñaUsuarioKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtContraseñaUsuarioKeyTyped

        //Validar que el campo solo contenga numeros, letras y simbolos especiales

        //Variables que se encarga de captura el caracter digitado
        char c;
        c = evt.getKeyChar();

        //Si se ingresa un caracter no valido, se ignora
        if ((c < '0' || c > '9') && (c < 'a' || c > 'z') && (c < 'A' || c > 'Z') && (c != 'ñ') && (c != 'Ñ') && (c != '@') && (c != '$') && (c != '<') && (c != '>') && (c != '_') && (c != '-')) {
            evt.consume(); //ignora el caracter digitado 
        }

    }//GEN-LAST:event_txtContraseñaUsuarioKeyTyped

    private void btnLimpiarUsuarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLimpiarUsuarioActionPerformed

        //Limpiamos todos los campos
        limpiarCampos();
        cmbNombreUsuario.setSelectedIndex(0);

    }//GEN-LAST:event_btnLimpiarUsuarioActionPerformed

    private void cmbNombreUsuarioItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbNombreUsuarioItemStateChanged

        //Variable que contendra el nombre del usuario seleccionado
        String nombreUsuario;

        //Se extrae el usuario seleccionado
        nombreUsuario = cmbNombreUsuario.getSelectedItem().toString();

        //Si se selecciona el primer elemento del combobox
        if (nombreUsuario.equalsIgnoreCase("<--Seleccione un usuario-->")) {
            limpiarCampos();
        } else {
            //Iniciamos sesion en hibernate
            Session session = null;

            //Cadena de seleccion de datos en base al nombre del usuario seleccionado por el usuario
            String hql = "FROM alemar.entidad.Usuario AS usuario WHERE usuario.nombreUsuario = :nombreUsuario";

            try {
                //Conexion a la BD
                sessionFactory = HibernateConexion.getSessionFactory();
                //Apertura de la sesion
                session = sessionFactory.openSession();
                Transaction txt = session.beginTransaction();

                //Extraccion de los datos del usuario de la BD en base al nombre proporcionado                    
                Query query = session.createQuery(hql).setString("nombreUsuario", nombreUsuario);
                //Se pone en una lista el registro sacado
                listaUsuario = query.list();
                //Guardando el id del usuario en una variable global
                id = listaUsuario.get(0).getIdUsuario();

                //Se ponen los valores en los campos de texto correspondientes para que se modifiquen
                txtNuevoNombreUsuario.setText(listaUsuario.get(0).getNombreUsuario());
                //Se desencripta la contraseña del usuario
                CifradorCesar dc = new CifradorCesar(listaUsuario.get(0).getPassword(), 4);
                dc.decodCesar();
                String contraseñaDecodificada = dc.getDecodificada();
                txtContraseñaUsuario.setText(contraseñaDecodificada);
                txtConfirmarContraseña.setText(contraseñaDecodificada);
                cmbTipoUsuario.setSelectedItem(listaUsuario.get(0).getTipoUsuario());

                //habilitando campos de texto para edicion 
                txtContraseñaUsuario.setEditable(true);
                txtConfirmarContraseña.setEditable(true);
                cmbTipoUsuario.setEnabled(true);
                txt.commit();

                //Finalizacion de la sesion de hibernate
                session.close();

            } catch (org.hibernate.JDBCException e) {
                JOptionPane.showMessageDialog(null, "No se puedieron cargar los datos del usuario " + nombreUsuario + " debido a un problema de conexión con la base de datos.", "Error de conexión", JOptionPane.ERROR_MESSAGE);
            }
        }
    }//GEN-LAST:event_cmbNombreUsuarioItemStateChanged

    private void btnActualizarUsuarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnActualizarUsuarioActionPerformed

        //Variables que contendran los valores extraidos de los campos de texto 
        String nombreUsuario;
        String contraseña;
        String confirmarContraseña;
        String tipoUsuario;

        //Variable que maneja la respuesta del JOptionpane
        int conf;

        //Extraemos la informacion de los campos de texto
        nombreUsuario = txtNuevoNombreUsuario.getText();
        contraseña = txtContraseñaUsuario.getText();
        confirmarContraseña = txtConfirmarContraseña.getText();
        tipoUsuario = cmbTipoUsuario.getSelectedItem().toString();

        //Validamos que no haya ningun campo vacio o sin seleccionar
        if (!(nombreUsuario.isEmpty() || contraseña.isEmpty() || tipoUsuario.equals("<--Seleccione un usuario-->"))) {
            //Inicamos sesion en hibernate
            Session session = null;
            try {
                try {
                    //Conexion a la BD
                    sessionFactory = HibernateConexion.getSessionFactory();
                    //Apertura de la sesion
                    session = sessionFactory.openSession();
                    Transaction tx = session.beginTransaction();

                    //Selector de caso el cual verifica cuel es el rol del que el usuario seleccionada tiene asignado y se asigna en el combo box
                    switch (tipoUsuario) {
                        case "Administrador":
                            cmbTipoUsuario.setSelectedIndex(1);
                            break;
                        case "Gerente de Ventas":
                            cmbTipoUsuario.setSelectedIndex(2);
                            break;
                        case "Encargado de Bodega":
                            cmbTipoUsuario.setSelectedIndex(3);
                            break;
                        case "Vendedor":
                            cmbTipoUsuario.setSelectedIndex(4);
                            break;
                        case "Cajero":
                            cmbTipoUsuario.setSelectedIndex(5);
                            break;
                        default:
                        case "Seleccione un Rol":
                            break;
                    }

                    //Extrayendo el regitro del usuario seleccionado en  base al ID
                    Usuario usuario = (Usuario) session.get(Usuario.class, id);

                    //Desencriptamos la contraseña
                    CifradorCesar dc = new CifradorCesar(contraseña, 4);
                    dc.codCesar();
                    String contraseñaCodificada = dc.getCodificada();

                    //Si las contraseñas coinciden procedemos a seguir evaluando
                    if (confirmarContraseña.equals(contraseña)) {
                        //Si la contraseña cumple con los estandares de tamaño minimo entonces se puede insertar
                        if (contraseña.length() >= limMinimo) {
                            //Mensaje de pregunta si quiere guardar los datos proporcionados 
                            conf = JOptionPane.showConfirmDialog(null, "¿Esta seguro de actualizar esos datos?", "Confirmación", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
                            if (conf == JOptionPane.YES_OPTION) {//Si la opcion fue SI
                                //Se procede a actualizar los datos del usuario

                                //Ingresando la nueva informacion del usuario al objeto Usuario
                                usuario.setNombreUsuario(nombreUsuario);
                                usuario.setPassword(contraseñaCodificada);
                                usuario.setTipoUsuario(tipoUsuario);

                                //Actualiza los datos del usuario
                                session.update(usuario);
                                tx.commit();
                                //Finalizacion de la sesion de hibernate
                                session.close();
                                
                                //Limpiamos todos los campos
                                limpiarCampos();
                                cmbNombreUsuario.setSelectedIndex(0);
                                //Mensaje de confirmacion de actualización exitosa
                                JOptionPane.showMessageDialog(null, "Registro actualizado exitosamente", "Información", JOptionPane.INFORMATION_MESSAGE);
                            }
                        } else {
                            JOptionPane.showMessageDialog(null, "Error, la contraseña debe de contener por lo menos 6 caracteres", "Error en los datos", JOptionPane.ERROR_MESSAGE);
                            txtContraseñaUsuario.setText("");
                            txtConfirmarContraseña.setText("");
                            barraContraseña.setValue(0);
                            jLabel8.setText("");
                        }
                    } else {
                        JOptionPane.showMessageDialog(null, "Error, las contraseñas no coinciden", "Error en los datos", JOptionPane.ERROR_MESSAGE);
                        txtContraseñaUsuario.setText("");
                        txtConfirmarContraseña.setText("");
                        barraContraseña.setValue(0);
                        jLabel8.setText("");
                    }
                } catch (org.hibernate.JDBCException e) {
                    JOptionPane.showMessageDialog(null, "No se pueden actualizar los datos del usuario " + nombreUsuario +" debido a un problema de conexión con la base de datos: ", "Error de conexión", JOptionPane.ERROR_MESSAGE);
                }
            } finally {
                //Se vuelve a cargar el comboBox
                cargarComboBox();
            }
        } else {//Si hay campos vacios, mensaje de error
            JOptionPane.showMessageDialog(null, "Error, no pueden haber campos vacios o sin seleccionar.", "Error en los datos", JOptionPane.ERROR_MESSAGE);
        }

    }//GEN-LAST:event_btnActualizarUsuarioActionPerformed

    private void txtContraseñaUsuarioFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtContraseñaUsuarioFocusGained

        //Verificamos si el Bloq Mayus esta activado
        estadoBM = Toolkit.getDefaultToolkit().getLockingKeyState(KeyEvent.VK_CAPS_LOCK);

        //Si esta activo mostramos el mensaje
        if (estadoBM == true) {
            jLabel9.setVisible(true);
        } else {
            jLabel9.setVisible(false);
        }

    }//GEN-LAST:event_txtContraseñaUsuarioFocusGained

    private void txtConfirmarContraseñaFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtConfirmarContraseñaFocusGained

        //Verificamos si el Bloq Mayus esta activado
        estadoBM = Toolkit.getDefaultToolkit().getLockingKeyState(KeyEvent.VK_CAPS_LOCK);

        //Si esta activo mostramos el mensaje
        if (estadoBM == true) {
            jLabel9.setVisible(true);
        } else {
            jLabel9.setVisible(false);
        }

    }//GEN-LAST:event_txtConfirmarContraseñaFocusGained

    private void txtContraseñaUsuarioFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtContraseñaUsuarioFocusLost

        //Ocultamos el label
        jLabel9.setVisible(false);

    }//GEN-LAST:event_txtContraseñaUsuarioFocusLost

    private void txtConfirmarContraseñaFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtConfirmarContraseñaFocusLost

        //Ocultamos el label
        jLabel9.setVisible(false);

    }//GEN-LAST:event_txtConfirmarContraseñaFocusLost
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JProgressBar barraContraseña;
    private javax.swing.JButton btnActualizarUsuario;
    private javax.swing.JButton btnLimpiarUsuario;
    private javax.swing.JComboBox cmbNombreUsuario;
    private javax.swing.JComboBox cmbTipoUsuario;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JPasswordField txtConfirmarContraseña;
    private javax.swing.JPasswordField txtContraseñaUsuario;
    private javax.swing.JTextField txtNuevoNombreUsuario;
    // End of variables declaration//GEN-END:variables
}
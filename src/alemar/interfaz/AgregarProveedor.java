//Interfaz para agregar nuevos Proveedores al sistema
package alemar.interfaz;

import alemar.configuracion.HibernateConexion;
import alemar.entidad.Proveedor;
import java.awt.Color;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JOptionPane;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

public class AgregarProveedor extends javax.swing.JPanel {
    //Atributos de la clase
    //Atributos para el uso de Hibernate

    private static SessionFactory sessionFactory = null;
    //Para limitar el numero de caracteres de un campo de texto
    private int limMinimo = 6;
    private int limMaximo = 20;
    private int lim1 = 8;
    private int limmax=30;
    private int max=70;
    private int maximo=50;

    public AgregarProveedor() {
        initComponents();

        //Agregando mensajes de ayuda para el llenado de los campos
        txtTelefonoProveedor.setToolTipText("Ingrese el número de teléfono.Ej: 22222222");
        txtemail.setToolTipText("Ingrese el E-mail del proveedor. Unicámente letras, numeros y simbolos (@,$,<,>,_,-)");
        txtNombreProveedor.setToolTipText("Ingrese el nombre del proveedor. Unicámente letras");
        txtDireccionProveedor.setToolTipText("Ingrese la dirección del proveedor, Unicámente letras, numeros y simbolos (@,$,<,>,_,-)");
        txtProductosProveedor.setToolTipText("Ingrese los productos que oferta el proveedor. Unicámente letras");
        txtNombreProveedor.setText("Ej: Luis Hernandez");
        txtNombreProveedor.setForeground(Color.LIGHT_GRAY);
        txtemail.setText("");
        txtemail.setForeground(Color.LIGHT_GRAY);
        txtDireccionProveedor.setText("Ej: calle los abetos #50, San Salvador");
        txtDireccionProveedor.setForeground(Color.LIGHT_GRAY);
        txtTelefonoProveedor.setText("");
        //txtTelefonoProveedor.setForeground(Color.LIGHT_GRAY);
        txtProductosProveedor.setText("Ej: martillos, pintura, clavos, etc");
        txtProductosProveedor.setForeground(Color.LIGHT_GRAY);
        //Limitando el numero de caracteres para el telfono de proveedor
        txtTelefonoProveedor.addKeyListener(new KeyListener() {
            public void keyTyped(KeyEvent e) {
                if (txtTelefonoProveedor.getText().length() == lim1) {
                    e.consume();
                }
            }

            public void keyPressed(KeyEvent arg0) {
            }

            public void keyReleased(KeyEvent arg0) {
            }
        });

        //Limitando el numero de caracteres para los productos que vende Proveedor
        txtProductosProveedor.addKeyListener(new KeyListener() {
            public void keyTyped(KeyEvent e) {
                if (txtProductosProveedor.getText().length() == 100) {
                    e.consume();
                }
            }

            public void keyPressed(KeyEvent arg0) {
            }

            public void keyReleased(KeyEvent arg0) {
            }
        });
        //Limitando el numero de caracteres para los productos que vende Proveedor
        txtemail.addKeyListener(new KeyListener() {
            public void keyTyped(KeyEvent e) {
                if (txtemail.getText().length() == 50) {
                    e.consume();
                }
            }

            public void keyPressed(KeyEvent arg0) {
            }

            public void keyReleased(KeyEvent arg0) {
            }
        });

        //Limitando el numero de caracteres para Direccion de Proveedor
        txtNombreProveedor.addKeyListener(new KeyListener() {
            public void keyTyped(KeyEvent e) {
                if (txtDireccionProveedor.getText().length() == 25) {
                    e.consume();
                }
            }

            public void keyPressed(KeyEvent arg0) {
            }

            public void keyReleased(KeyEvent arg0) {
            }
        });
    }
    //Metodos de la clase
    //Metodo que limpia todos los campos de la interfaz

    private void limpiarCampos() {

        txtNombreProveedor.setText("Ej: Luis Hernandez");
        txtNombreProveedor.setForeground(Color.LIGHT_GRAY);
        txtDireccionProveedor.setText("Ej: calle los abetos #50, San Salvador");
        txtDireccionProveedor.setForeground(Color.LIGHT_GRAY);
        txtTelefonoProveedor.setText("");
        txtemail.setText("");
        //txtTelefonoProveedor.setForeground(Color.LIGHT_GRAY);
        txtProductosProveedor.setText("Ej: martillos, pintura, clavos, etc");
        txtProductosProveedor.setForeground(Color.LIGHT_GRAY);
        
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnAgregarProveedor = new javax.swing.JButton();
        btnLimpiar = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        txtNombreProveedor = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        txtDireccionProveedor = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtProductosProveedor = new javax.swing.JTextArea();
        txtemail = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        txtTelefonoProveedor = new javax.swing.JTextField();

        setBackground(new java.awt.Color(255, 255, 255));
        setMaximumSize(new java.awt.Dimension(745, 400));
        setMinimumSize(new java.awt.Dimension(745, 400));
        setPreferredSize(new java.awt.Dimension(745, 400));

        btnAgregarProveedor.setText("Agregar");
        btnAgregarProveedor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAgregarProveedorActionPerformed(evt);
            }
        });

        btnLimpiar.setText("Limpiar");
        btnLimpiar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLimpiarProveedorActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        jLabel1.setText("Agregar nuevo proveedor al sistema");

        txtNombreProveedor.setBackground(new java.awt.Color(238, 253, 253));
        txtNombreProveedor.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtNombreProveedorFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtNombreProveedorFocusLost(evt);
            }
        });
        txtNombreProveedor.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtNombreProveedorKeyTyped(evt);
            }
        });

        jLabel2.setText("Nombre del proveedor");

        txtDireccionProveedor.setBackground(new java.awt.Color(238, 253, 253));
        txtDireccionProveedor.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtDireccionProveedorFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtDireccionProveedorFocusLost(evt);
            }
        });

        jLabel3.setText("Dirección");

        jLabel4.setText("Teléfono");

        jLabel5.setText("Productos que oferta");

        txtProductosProveedor.setBackground(new java.awt.Color(238, 253, 253));
        txtProductosProveedor.setColumns(20);
        txtProductosProveedor.setRows(5);
        txtProductosProveedor.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtProductosProveedorFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtProductosProveedorFocusLost(evt);
            }
        });
        txtProductosProveedor.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtProductosProveedorKeyTyped(evt);
            }
        });
        jScrollPane1.setViewportView(txtProductosProveedor);

        txtemail.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtemailFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtemailFocusLost(evt);
            }
        });

        jLabel6.setText("E-mail");

        jLabel7.setText("*");

        txtTelefonoProveedor.setBackground(new java.awt.Color(238, 253, 253));
        txtTelefonoProveedor.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtTelefonoProveedorKeyTyped(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(171, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(btnLimpiar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnAgregarProveedor)
                        .addContainerGap())
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel1)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel5)
                                    .addComponent(jLabel3)
                                    .addComponent(jLabel2)
                                    .addComponent(jLabel4)
                                    .addComponent(jLabel6))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txtNombreProveedor, javax.swing.GroupLayout.PREFERRED_SIZE, 240, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txtDireccionProveedor, javax.swing.GroupLayout.PREFERRED_SIZE, 240, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 240, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                            .addComponent(txtTelefonoProveedor, javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(txtemail, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 240, Short.MAX_VALUE))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jLabel7)))))
                        .addGap(207, 207, 207))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(54, 54, 54)
                .addComponent(jLabel1)
                .addGap(40, 40, 40)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtNombreProveedor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(txtDireccionProveedor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(txtTelefonoProveedor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(txtemail, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel7))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 11, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnAgregarProveedor)
                    .addComponent(btnLimpiar))
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents
    private void txtDireccionProveedorKeyTyped(java.awt.event.KeyEvent evt) {
        //Validar que el campo solo contenga numeros, letras y simbolos especiales
        //Variables que se encarga de captura el caracter digitado
        char c;
        c = evt.getKeyChar();

        //Si se ingresa un caracter no valido, se ignora
        if ((c < '0' || c > '9') && (c < 'a' || c > 'z') && (c < 'A' || c > 'Z') && (c != 'ñ') && (c != 'Ñ') && (c != '@') && (c != '$') && (c != '<') && (c != '>') && (c != '_') && (c != '-')) {
            evt.consume(); //ignora el caracter digitado 
        }

    }

    
    private void txtEmailProveedorKeyTyped(java.awt.event.KeyEvent evt) {
        //Validar que el campo solo contenga numeros, letras y simbolos especiales
        //Variables que se encarga de captura el caracter digitado
        char c;
        c = evt.getKeyChar();
        //Si se ingresa un caracter no valido, se ignora
        if ((c < '0' || c > '9') && (c < 'a' || c > 'z') && (c < 'A' || c > 'Z') && (c != 'ñ') && (c != 'Ñ') && (c != '@') && (c != '$') && (c != '<') && (c != '>') && (c != '_') && (c != '-')) {
            evt.consume(); //ignora el caracter digitado 
        }
    }

    private void btnLimpiarProveedorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLimpiarProveedorActionPerformed
        //Limpiamos todos los campos
        limpiarCampos();

    }//GEN-LAST:event_btnLimpiarProveedorActionPerformed

    private void btnAgregarProveedorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAgregarProveedorActionPerformed

        //Variables que contendran los valores de los campos de texto
        String direccion = "";
        String email = "";
        String nombre = "";
        String productos = "";
        String telefono = "";

        //Variable que maneja la respuesta del JOptionPane
        int conf;

        //Extrayendo la informacion de los campos de texto
        nombre = txtNombreProveedor.getText();
        email = txtemail.getText();
        direccion = txtDireccionProveedor.getText();
        telefono = txtTelefonoProveedor.getText();
        productos = txtProductosProveedor.getText();


        //Validaciones
        if (!(nombre.isEmpty() || direccion.isEmpty() || telefono.isEmpty() || productos.isEmpty() || nombre.equals("Ej: Luis Hernandez") || direccion.equals("Ej: calle los abetos #50, San Salvador") || productos.equals("Ej: martillos, pintura, clavos, etc"))) { //Si no esta vacio algun campo           

            //Chunche de Hibernate (Inicio de sesion de hibernate)
            Session session = null;
            try {
                try {
                    //Conexion a la BD
                    sessionFactory = HibernateConexion.getSessionFactory();
                    //Apertura de la sesion
                    session = sessionFactory.openSession();
                    Transaction tx = session.beginTransaction();
                    
                    //Si el nombre de proveedor cumple con los estandares de tamaño procedemos a seguir evaluando
                    
                    //Creando el objeto Proveedor 
                    Proveedor proveedor = new Proveedor();
                    //Ingresando la informacion del proveedor al 
                    //objeto Proveedor
                    proveedor.setNombre(nombre);
                    proveedor.setEmail(email);
                    proveedor.setDireccion(direccion);
                    proveedor.setTelefono(telefono);
                    proveedor.setProductosOferta(productos);
                   if (telefono.length() == 8) {
                    //Mensaje de pregunta si quiere guardar los datos proporcionados 
                    conf = JOptionPane.showConfirmDialog(null, "¿Esta seguro de ingresar esos datos?", "Confirmación", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);

                    if (conf == JOptionPane.YES_OPTION) {//Si la opcion fue SI

                        //Guardando el objeto en la base de datos
                        session.save(proveedor);
                        tx.commit();
                        //Finalizacion de la sesion de hibernate
                        session.close();
                        //Mensaje de confirmacion de insercion Exitosa
                        JOptionPane.showMessageDialog(null, "Registro guardado exitosamente", "Información", JOptionPane.INFORMATION_MESSAGE);
                        limpiarCampos();
                    
                    }
                   }
                    else {
                        JOptionPane.showMessageDialog(null, "Error, el telefono debe de contener  8 numeros", "Error en los datos", JOptionPane.ERROR_MESSAGE);
                        //Se limpia la pantalla
                        txtTelefonoProveedor.setText("");

                    }
                    

                } catch (org.hibernate.exception.ConstraintViolationException cve) {
                    session.getTransaction().rollback();
                    JOptionPane.showMessageDialog(null, "El nombre: " + nombre + " ya se encuentra asignado a otro proveeedor previamente registrado\n"
                            + "Ingrese un nombre distinto para el nuevo Proveedor a registrar", "Duplicidad en registros", JOptionPane.ERROR_MESSAGE);
                    txtNombreProveedor.setText("");
                }
            } catch (org.hibernate.JDBCException e) {
                JOptionPane.showMessageDialog(null, "No se puede establecer conexión con la base de datos.", "Error de conexión", JOptionPane.ERROR_MESSAGE);
            }


        } else {//Si hay campos vacios, mensaje de error
            JOptionPane.showMessageDialog(null, "Error, no pueden haber campos vacios", "Error en los datos", JOptionPane.ERROR_MESSAGE);
        }

    }//GEN-LAST:event_btnAgregarProveedorActionPerformed

    private void txtNombreProveedorFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtNombreProveedorFocusGained

        //Si el campo contiene el valor por defecto
        if (txtNombreProveedor.getText().equals("Ej: Luis Hernandez")) {
            txtNombreProveedor.setText("");
            txtNombreProveedor.setForeground(Color.BLACK);
        }
    }//GEN-LAST:event_txtNombreProveedorFocusGained

    private void txtNombreProveedorFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtNombreProveedorFocusLost

        //Verificamos si el campo esta vacio (de ser asi colocamos el valor por defecto nuevamente)

        if (txtNombreProveedor.getText().equals("")) {
            txtNombreProveedor.setText("Ej: Luis Hernandez");
            txtNombreProveedor.setForeground(Color.LIGHT_GRAY);
        }

    }//GEN-LAST:event_txtNombreProveedorFocusLost

    private void txtDireccionProveedorFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtDireccionProveedorFocusGained

        //Si el campo contiene el valor por defecto
        if (txtDireccionProveedor.getText().equals("Ej: calle los abetos #50, San Salvador")) {
            txtDireccionProveedor.setText("");
            txtDireccionProveedor.setForeground(Color.BLACK);
        }
    }//GEN-LAST:event_txtDireccionProveedorFocusGained

    private void txtDireccionProveedorFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtDireccionProveedorFocusLost

        //Verificamos si el campo esta vacio (de ser asi colocamos el valor por defecto nuevamente)
        if (txtDireccionProveedor.getText().equals("")) {
            txtDireccionProveedor.setText("Ej: calle los abetos #50, San Salvador");
            txtDireccionProveedor.setForeground(Color.LIGHT_GRAY);
        }

    }//GEN-LAST:event_txtDireccionProveedorFocusLost

    private void txtProductosProveedorFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtProductosProveedorFocusGained

        //Si el campo contiene el valor por defecto
        if (txtProductosProveedor.getText().equals("Ej: martillos, pintura, clavos, etc")) {
            txtProductosProveedor.setText("");
            txtProductosProveedor.setForeground(Color.BLACK);
        }
    }//GEN-LAST:event_txtProductosProveedorFocusGained

    private void txtProductosProveedorFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtProductosProveedorFocusLost
        
        //Verificamos si el campo esta vacio (de ser asi colocamos el valor por defecto nuevamente)
        if (txtProductosProveedor.getText().equals("")) {
            txtProductosProveedor.setText("Ej: martillos, pintura, clavos, etc");
            txtProductosProveedor.setForeground(Color.LIGHT_GRAY);
        }
   
    }//GEN-LAST:event_txtProductosProveedorFocusLost

    private void txtemailFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtemailFocusGained
        
        //Si el campo contiene el valor por defecto
        if (txtemail.getText().equals("")) {
            txtemail.setText("");
            txtemail.setForeground(Color.BLACK);
        }
    }//GEN-LAST:event_txtemailFocusGained

    private void txtemailFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtemailFocusLost
         
        //Verificamos si el campo esta vacio (de ser asi colocamos el valor por defecto nuevamente)
        if (txtemail.getText().equals("")) {
            txtemail.setText("");
            txtemail.setForeground(Color.LIGHT_GRAY);
        }
    }//GEN-LAST:event_txtemailFocusLost
private void txtTelefonoProveedorFocusGained(java.awt.event.FocusEvent evt) {                                                  

        //Si el campo contiene el valor por defecto
        if (txtTelefonoProveedor.getText().equals("")) {
            txtTelefonoProveedor.setText("");
            txtTelefonoProveedor.setForeground(Color.BLACK);
        }
    }                                                 

    private void txtTelefonoProveedorFocusLost(java.awt.event.FocusEvent evt) {                                                

        //Verificamos si el campo esta vacio (de ser asi colocamos el valor por defecto nuevamente)
        if (txtTelefonoProveedor.getText().equals("")) {
            txtTelefonoProveedor.setText("");
            txtTelefonoProveedor.setForeground(Color.LIGHT_GRAY);
        }

    }                                               

    private void txtProductosProveedorKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtProductosProveedorKeyTyped
  //Validar que el campo solo contenga  letras 
        //SOLO LETRAS
        char c;
        c = evt.getKeyChar();
        if (!(c < '0' || c > '9')) {
            evt.consume();
        }

    }//GEN-LAST:event_txtProductosProveedorKeyTyped

    private void txtTelefonoProveedorKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtTelefonoProveedorKeyTyped
        // SOLO NUMEROS
        char c;
//capturar el caracter digitado
        c = evt.getKeyChar();
        if (c < '0' || c > '9') {
            evt.consume();//ignora el caracter digitado 
        }

    }//GEN-LAST:event_txtTelefonoProveedorKeyTyped

    private void txtNombreProveedorKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtNombreProveedorKeyTyped
        //Validar que el campo solo contenga  letras 
        //SOLO LETRAS
        char c;
        c = evt.getKeyChar();
        if (!(c < '0' || c > '9')) {
            evt.consume();
        }

    }//GEN-LAST:event_txtNombreProveedorKeyTyped
                      
  
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAgregarProveedor;
    private javax.swing.JButton btnLimpiar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextField txtDireccionProveedor;
    private javax.swing.JTextField txtNombreProveedor;
    private javax.swing.JTextArea txtProductosProveedor;
    private javax.swing.JTextField txtTelefonoProveedor;
    private javax.swing.JTextField txtemail;
    // End of variables declaration//GEN-END:variables
}

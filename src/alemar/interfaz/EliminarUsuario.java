//Interfaz que se encarga de eliminar usuarios registrados en el sistema
package alemar.interfaz;

import alemar.clases.CargarCMBUsuario;
import alemar.clases.VariablesGlobales;
import alemar.configuracion.HibernateConexion;
import alemar.entidad.Usuario;
import java.util.List;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

public class EliminarUsuario extends javax.swing.JPanel {
    //Atributos de la clase

    //Atributos para el uso de Hibernate
    private static SessionFactory sessionFactory = null;
    //Lista que contendra los usuarios registrados en el sistema
    List<Usuario> listaUsuario = null;

    public EliminarUsuario() {
        initComponents();
        //Se carga el combo box con los usuarios registrados en el sistema
        cargarComboBox();
    }

    //Metodos de la clase
    //Metodo que se encarga de cargar los usuarios registrados en el sistema en un combo box
    private void cargarComboBox() {
        //Se inicializa la lista a null
        listaUsuario = null;

        //Instanciamos la clase CargarCMBUsuario para cargar todos los usuarios registrados en una lista
        CargarCMBUsuario cargarUsuarios = new CargarCMBUsuario();
        listaUsuario = cargarUsuarios.cargarUsuarios();

        //Creando un modelo de comboBox                
        DefaultComboBoxModel modeloCombo = new DefaultComboBoxModel();

        //Poniendole un mensaje de seleccion (Primer elemento) al combo box
        modeloCombo.addElement("<--Seleccione un usuario-->");

        //Llenando el modelo de comboBox con los nombres de los usuarios
        for (Usuario usuario : listaUsuario) {
            //Si el usuario es el superadministrador no lo cargamos
            if (!usuario.getNombreUsuario().equals("SUPERADMIN")) {
                modeloCombo.addElement(usuario.getNombreUsuario());
            }
        }
        //Agregando el modelo del comboBox al comboBox del panel
        cmbNombreUsuario.setModel(modeloCombo);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnEliminarUsuario = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        cmbNombreUsuario = new javax.swing.JComboBox();

        setBackground(new java.awt.Color(255, 255, 255));
        setMaximumSize(new java.awt.Dimension(794, 432));
        setMinimumSize(new java.awt.Dimension(794, 432));
        setPreferredSize(new java.awt.Dimension(794, 432));

        btnEliminarUsuario.setText("Eliminar");
        btnEliminarUsuario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEliminarUsuarioActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Eliminar usuario");

        jLabel2.setText("Nombre de usuario");

        cmbNombreUsuario.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 743, Short.MAX_VALUE)
                        .addComponent(btnEliminarUsuario)))
                .addContainerGap())
            .addGroup(layout.createSequentialGroup()
                .addGap(199, 199, 199)
                .addComponent(jLabel2)
                .addGap(26, 26, 26)
                .addComponent(cmbNombreUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, 240, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(126, 126, 126)
                .addComponent(jLabel1)
                .addGap(51, 51, 51)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cmbNombreUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 201, Short.MAX_VALUE)
                .addComponent(btnEliminarUsuario)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnEliminarUsuarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEliminarUsuarioActionPerformed

        //Variable que guardara el nombre de usuario
        String nombreUsuario;

        //Variable que contedra la confirmación del usuario para eleminar los datos
        int conf;

        //Se extrae el valor seleccionado en el combobox
        nombreUsuario = cmbNombreUsuario.getSelectedItem().toString();

        //Si se selecciona el primer valor del combobox no se elimina nada
        if (nombreUsuario.equals("<--Seleccione un usuario-->")) {
            JOptionPane.showMessageDialog(null, "Seleccione un nombre de usuario", "Selección de datos", JOptionPane.ERROR_MESSAGE);
        } else { //Se elmina el usuario
            //Inicio de sesion en Hibernate
            Session session = null;

            //Cadena de eliminacion de HQL
            String hql = "delete alemar.entidad.Usuario usuario where usuario.nombreUsuario = :nombreUsuario";

            try {
                try {
                    //Realizamos la conexion a la BD
                    sessionFactory = HibernateConexion.getSessionFactory();
                    //Apertura de la sesion
                    session = sessionFactory.openSession();
                    Transaction txt = session.beginTransaction();

                    //Mensaje de pregunta si quiere eliminar el registro 
                    conf = JOptionPane.showConfirmDialog(null, "¿Esta seguro que quiere eliminar este Usuario?\n" + nombreUsuario, "Confirmación", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);

                    if (conf == JOptionPane.YES_OPTION) {//Si la opcion fue SI                    
                        if (VariablesGlobales.nombreUsuario.equals(nombreUsuario)) {
                           JOptionPane.showMessageDialog(null, "No se puede eliminar al usuario " +  nombreUsuario + " mientras este no haya cerrado sesión", "Información", JOptionPane.ERROR_MESSAGE); 
                        } else {
                            //Eliminacion del Usuario de la BD en base al nombre de usuario
                            session.createQuery(hql).setString("nombreUsuario", nombreUsuario).executeUpdate();
                            txt.commit();
                            //Mensaje de confirmacion de eliminación existosa
                            JOptionPane.showMessageDialog(null, "El usuario " + nombreUsuario + " ha sido eliminado exitosamente", "Información", JOptionPane.INFORMATION_MESSAGE);
                            //Finalizacion de la sesion de hibernate
                            session.close();
                        }

                    }
                } catch (org.hibernate.JDBCException e) {
                    JOptionPane.showMessageDialog(null, "El usuario " + nombreUsuario + " no pudo ser eliminado debido a un problema de conexión con la base de datos.", "Error de conexión", JOptionPane.ERROR_MESSAGE);
                }
            } finally {
                //Se carga nuevamente el comboBox
                cargarComboBox();
            }
        }

    }//GEN-LAST:event_btnEliminarUsuarioActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnEliminarUsuario;
    private javax.swing.JComboBox cmbNombreUsuario;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    // End of variables declaration//GEN-END:variables
}

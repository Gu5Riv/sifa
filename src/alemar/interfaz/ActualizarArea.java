//Interfaz que se encarga de actualizar los datos de un area registrada en el sistema
package alemar.interfaz;

import alemar.clases.CargarComboArea;
import alemar.configuracion.HibernateConexion;
import alemar.entidad.Area;
import java.awt.Color;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.List;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

/**
 *
 * @author Teto
 */
public class ActualizarArea extends javax.swing.JPanel {

    private static SessionFactory sessionFactory = null;
    private int limMinimo = 25;
    private int limMaximo = 100;
    private int id;
    private String nom;
    List<Area> listaArea = null;

    public ActualizarArea() {
        initComponents();
        cargarComboBox();
        limpiar();

        txtNuevoNombreArea.setEnabled(false);
        txtIdentificacionArea.setEnabled(false);

        txtNuevoNombreArea.setToolTipText("Ingrese el nuevo nombre del àrea.Unicámente letra, por ejemplo: Jardineria.");
        txtIdentificacionArea.setToolTipText("Digite la descripción del área");
        txtNuevoNombreArea.setForeground(Color.LIGHT_GRAY);
        txtIdentificacionArea.setForeground(Color.LIGHT_GRAY);
        txtNuevoNombreArea.addKeyListener(new KeyListener() {
            public void keyTyped(KeyEvent e) {
                if (txtNuevoNombreArea.getText().length() == limMinimo) {
                    e.consume();
                }
            }

            public void keyPressed(KeyEvent arg0) {
            }

            public void keyReleased(KeyEvent arg0) {
            }
        });
        
        txtIdentificacionArea.addKeyListener(new KeyListener() {
            public void keyTyped(KeyEvent e) {
                if (txtIdentificacionArea.getText().length() == limMaximo) {
                    e.consume();
                }
            }

            public void keyPressed(KeyEvent arg0) {
            }

            public void keyReleased(KeyEvent arg0) {
            }
        });
        
        

        limpiar();

        //Limitando el numero de caracteres
        txtIdentificacionArea.addKeyListener(new KeyListener() {
            public void keyTyped(KeyEvent e) {
                if (txtIdentificacionArea.getText().length() == 100) {
                    e.consume();
                }
            }

            public void keyPressed(KeyEvent arg0) {
            }

            public void keyReleased(KeyEvent arg0) {
            }
        });
    
    }
        
    private void cargarComboBox() {
        //Se inicializa la lista a null
        listaArea = null;

        //Instanciamos la clase CargarCMBUsuario para cargar todos los usuarios registrados en una lista
        CargarComboArea cargarArea = new CargarComboArea();
        listaArea = cargarArea.cargarArea();

        //Creando un modelo de comboBox                
        DefaultComboBoxModel modeloCombo = new DefaultComboBoxModel();

        //Poniendole un mensaje de seleccion (Primer elemento) al combo box
        modeloCombo.addElement("<--Seleccione un Area de Producto-->");

        //Llenando el modelo de comboBox con los nombres de los usuarios
        for (Area area : listaArea) {
           modeloCombo.addElement(area.getNombreArea()); 
        }
        //Agregando el modelo del comboBox al comboBox del panel
        cmbNombreArea.setModel(modeloCombo);
    }


    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        cmbNombreArea = new javax.swing.JComboBox();
        jSeparator1 = new javax.swing.JSeparator();
        txtNuevoNombreArea = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        btnActualizarArea = new javax.swing.JButton();
        btnCancelarArea = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtIdentificacionArea = new javax.swing.JTextArea();

        setBackground(new java.awt.Color(255, 255, 255));
        setMaximumSize(new java.awt.Dimension(745, 400));
        setMinimumSize(new java.awt.Dimension(745, 400));
        setPreferredSize(new java.awt.Dimension(745, 400));

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Actualizar datos de área");

        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel2.setText("Nombre de área");

        cmbNombreArea.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        cmbNombreArea.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbNombreAreaItemStateChanged(evt);
            }
        });
        cmbNombreArea.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbNombreAreaActionPerformed(evt);
            }
        });

        txtNuevoNombreArea.setEditable(false);
        txtNuevoNombreArea.setBackground(new java.awt.Color(238, 253, 253));
        txtNuevoNombreArea.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtNuevoNombreAreaKeyTyped(evt);
            }
        });

        jLabel3.setText("Nuevo nombre de área");

        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel4.setText("Descripción");

        btnActualizarArea.setText("Actualizar");
        btnActualizarArea.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnActualizarAreaActionPerformed(evt);
            }
        });

        btnCancelarArea.setText("Limpiar");
        btnCancelarArea.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarAreaActionPerformed(evt);
            }
        });

        txtIdentificacionArea.setEditable(false);
        txtIdentificacionArea.setBackground(new java.awt.Color(238, 253, 253));
        txtIdentificacionArea.setColumns(20);
        txtIdentificacionArea.setRows(5);
        txtIdentificacionArea.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                txtIdentificacionAreaMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(txtIdentificacionArea);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(0, 81, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 569, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnCancelarArea))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnActualizarArea)
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 240, Short.MAX_VALUE)
                    .addComponent(txtNuevoNombreArea))
                .addGap(187, 187, 187))
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 366, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addGap(190, 190, 190)
                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(cmbNombreArea, javax.swing.GroupLayout.PREFERRED_SIZE, 240, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(44, 44, 44)
                .addComponent(jLabel1)
                .addGap(39, 39, 39)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(cmbNombreArea, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(27, 27, 27)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtNuevoNombreArea, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel4)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 54, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnActualizarArea)
                    .addComponent(btnCancelarArea))
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void txtNuevoNombreAreaKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtNuevoNombreAreaKeyTyped

        char c;
        c = evt.getKeyChar();
        if (!(c < '0' || c > '9')) {
            evt.consume();
        }

    }//GEN-LAST:event_txtNuevoNombreAreaKeyTyped

    private void cmbNombreAreaItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbNombreAreaItemStateChanged

        String nombreArea = "";

        List<Area> Larea = null;

        //Se extrae el proveedor seleccionado
        nombreArea = cmbNombreArea.getSelectedItem().toString();

        if (nombreArea.equalsIgnoreCase("<--Seleccione una area de producto-->")) {
            
            limpiar();
        } 
        else {
            //Chunche de Hibernate (Inicio de sesion de hibernate)
            Session session = null;
            //Cadena de seleccion de HQL
            String hql = "FROM alemar.entidad.Area AS a WHERE a.nombreArea = :nombreArea";
           // try {
                try {
                    //Conexion a la BD
                    sessionFactory = HibernateConexion.getSessionFactory();
                    //Apertura de la sesion
                    session = sessionFactory.openSession();
                    Transaction txt = session.beginTransaction();

                    //Extraccion de los datos del proveedor de la BD en base al nombre proporcionado                    
                    Query query = session.createQuery(hql).setString("nombreArea", nombreArea);

                    //Se pone en una lista el registro sacado
                    Larea = query.list();

                    //Guardando el id del proveedor en una variable global
                    id = Larea.get(0).getIdArea();
                    
                    //Guardando el nombre del area en una variable global
                    nom = Larea.get(0).getNombreArea();

                    //Se ponen los valores en los campos de texto correspondientes
                    //para que se modifiquen
                    txtNuevoNombreArea.setText(Larea.get(0).getNombreArea());
                    txtIdentificacionArea.setText(Larea.get(0).getIdentificacion());

                    //habilitando campos de texto para edicion 
                    txtNuevoNombreArea.setEditable(true);
                    txtIdentificacionArea.setEditable(true);

                    txtNuevoNombreArea.setEnabled(true);
                    txtIdentificacionArea.setEnabled(true);
                    
                    txtNuevoNombreArea.setForeground(Color.BLACK);
                    txtIdentificacionArea.setForeground(Color.BLACK);

                    txt.commit();
                     session.close();

                } catch (org.hibernate.JDBCException e) {
                JOptionPane.showMessageDialog(null, "No se puede establecer conexión con la base de datos.", "Error de conexión", JOptionPane.ERROR_MESSAGE);

                }
            }// finally {
                //Finalizacion de la sesion de hibernate
               
            //}

    }//GEN-LAST:event_cmbNombreAreaItemStateChanged

    private void btnCancelarAreaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarAreaActionPerformed

        limpiar();
        cmbNombreArea.setSelectedIndex(0);

    }//GEN-LAST:event_btnCancelarAreaActionPerformed

    private void btnActualizarAreaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnActualizarAreaActionPerformed

        //Variables que contendran los valores de los campos de texto 
        String nombre = "";
        String identificacion = "";
        String auxiliar = "";

        //Variable que maneja la respuesta del JOptionpane
        int conf;

        //Extrayendo la informacion de los campos de texto
        nombre = txtNuevoNombreArea.getText();
        identificacion = txtIdentificacionArea.getText();
        auxiliar = cmbNombreArea.getSelectedItem().toString();

        //Validaciones
        if (!(nombre.isEmpty() || auxiliar.equals("<--Seleccione una Area de producto-->"))) { //Si no esta vacio algun campo           

            //Chunche de Hibernate (Inicio de sesion de hibernate)
            Session session = null;
            try {
                try {
                    //Conexion a la BD
                    sessionFactory = HibernateConexion.getSessionFactory();
                    //Apertura de la sesion
                    session = sessionFactory.openSession();
                    Transaction tx = session.beginTransaction();

                    //Extrayendo el regitro del proveedor seleccionado
                    //en base al ID guardado previamente de manera global
                    Area area = (Area) session.get(Area.class, id);

                    //Ingresando la nueva informacion del proveedor al 
                    //objeto Proveedor
                    area.setNombreArea(nombre);
                    area.setIdentificacion(identificacion);

                    //Mensaje de pregunta si quiere guardar los datos proporcionados 
                    conf = JOptionPane.showConfirmDialog(null, "¿Esta seguro de ingresar esos datos?", "Confirmación", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);

                    if (conf == JOptionPane.YES_OPTION) {//Si la opcion fue SI
                       
                        if(!(nombre.isEmpty() || identificacion.isEmpty() || nombre.equals("<--Seleccione un usuario-->"))){
                            
                            //Actualiza los datos del proveedor
                            session.update(area);
                            tx.commit();
                            session.close();
                            //Mensaje de confirmacion de insercion Exitosa
                            JOptionPane.showMessageDialog(null, "Registro guardado exitosamente", "Información", JOptionPane.INFORMATION_MESSAGE);

                            limpiar();
                            cmbNombreArea.setSelectedIndex(0);
                            txtNuevoNombreArea.setEnabled(false);
                            txtIdentificacionArea.setEnabled(false);
                        
                        }else{
                            JOptionPane.showMessageDialog(null,"Error, Nombre de area ya existe","Error en los datos",JOptionPane.ERROR_MESSAGE);
                                //Poner en el primer item el comboBox
                                cmbNombreArea.setSelectedIndex(0);
                        }                            

                    } else {
                        limpiar();
                    }

                } catch (org.hibernate.exception.ConstraintViolationException cve) {
                    session.getTransaction().rollback();
                    JOptionPane.showMessageDialog(null, "Error: El nombre " + nombre + " ya se encuentra asignado a otra Area", "Duplicidad en registros", JOptionPane.ERROR_MESSAGE);
                    txtNuevoNombreArea.setText("");
                }

            } catch (org.hibernate.JDBCException e) {
                JOptionPane.showMessageDialog(null, "Error: No se pueden actualizar los datos del Area " + nombre + " debido a un problema de conexión con la base de datos: ", "Error de conexión", JOptionPane.ERROR_MESSAGE);
            } finally {
                //Finalizacion de la sesion de hibernate

                //Se vuelve a cargar el comboBox
                cargarComboBox();
            }
        } else {//Si hay campos vacios, mensaje de error
            JOptionPane.showMessageDialog(null, "Error, no pueden haber campos vacios", "Error en los datos", JOptionPane.ERROR_MESSAGE);

        }

    }//GEN-LAST:event_btnActualizarAreaActionPerformed

    private void txtIdentificacionAreaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtIdentificacionAreaMouseClicked

        //txtIdentificacionArea.setForeground(Color.BLACK);

    }//GEN-LAST:event_txtIdentificacionAreaMouseClicked

    private void cmbNombreAreaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbNombreAreaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cmbNombreAreaActionPerformed

    
    //Metodo que limpia los campos de texto.
    private void limpiar() {

        txtNuevoNombreArea.setText("Herramientas.");
        txtNuevoNombreArea.setForeground(Color.LIGHT_GRAY);

        txtIdentificacionArea.setText("Son instrumentos que facilitan ciertas tareas.");
        txtIdentificacionArea.setForeground(Color.LIGHT_GRAY);

        txtNuevoNombreArea.setEditable(false);
        txtIdentificacionArea.setEditable(false);


    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnActualizarArea;
    private javax.swing.JButton btnCancelarArea;
    private javax.swing.JComboBox cmbNombreArea;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JTextArea txtIdentificacionArea;
    private javax.swing.JTextField txtNuevoNombreArea;
    // End of variables declaration//GEN-END:variables
}

//Interfaz de logeo y cambio de sesión de usuarios
package alemar.interfaz; 

import alemar.clases.CifradorCesar;
import alemar.clases.RelojVisual;
import alemar.clases.VariablesGlobales;
import alemar.configuracion.HibernateConexion;
import alemar.entidad.Usuario;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.List;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.jvnet.substance.SubstanceLookAndFeel;

public class Login extends javax.swing.JDialog {

    //Atributos de la clase
    //Variables para inicio de sesión en Hibernate
    private static SessionFactory sessionFactory = null;
    //Variable que limita el numero maximo de caracteres
    private int limMaximo = 20;
    //Variable que verifica si el bloq mayus esta activado
    private boolean estadoBM;

    public Login(java.awt.Frame parent, boolean modal) {

        //Se carga el panel de login con los elementos de inicio necesarios y se inician los componentes
        super(parent, modal);
        JDialog.setDefaultLookAndFeelDecorated(true);
        SubstanceLookAndFeel.setSkin("org.jvnet.substance.skin.OfficeSilver2007Skin");
        
        initComponents();
        super.setLocationRelativeTo(null);

        //Ocultamos el label que muestra si el bloq mayus esta activado
        jLabel6.setVisible(false);

        //Limitamos el numero maximo de caracteres para el nombre de usuario
        txtNombreUsuario.addKeyListener(new KeyListener() {
            public void keyTyped(KeyEvent e) {
                if (txtNombreUsuario.getText().length() == limMaximo) {
                    e.consume();
                }
            }

            public void keyPressed(KeyEvent arg0) {
            }

            public void keyReleased(KeyEvent arg0) {
            }
        });

        //Limitamos el numero maximo de caracteres para la contraseña
        txtContraseña.addKeyListener(new KeyListener() {
            public void keyTyped(KeyEvent e) {
                if (txtContraseña.getText().length() == limMaximo) {
                    e.consume();
                }
            }

            public void keyPressed(KeyEvent arg0) {
                //Verificamos si el Bloq Mayus esta activado
                estadoBM = Toolkit.getDefaultToolkit().getLockingKeyState(KeyEvent.VK_CAPS_LOCK);

                //Si esta activo mostramos el mensaje
                if (estadoBM == true) {
                    jLabel6.setVisible(true);
                } else {
                    jLabel6.setVisible(false);
                }
            }

            public void keyReleased(KeyEvent arg0) {
            }
        });

        //Se agregan los mensajes de ayuda para el llenado de los campos del panel de login
        txtNombreUsuario.setToolTipText("Ingrese el nombre de un usuario registrado en el sistema");
        txtContraseña.setToolTipText("Ingrese la contraseña del usuario");

        //Se intancia la clase VisualReloj par agregar la fecha y el reloj en el sistema
        RelojVisual reloj = new RelojVisual();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        txtContraseña = new javax.swing.JPasswordField();
        txtNombreUsuario = new javax.swing.JTextField();
        btnCancelar = new javax.swing.JButton();
        btnAcceder = new javax.swing.JButton();
        panel2 = new java.awt.Panel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setTitle("SIFA");
        setUndecorated(true);
        setResizable(false);

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Acceso al sistema");

        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/alemar/imagen/login.png"))); // NOI18N

        jLabel3.setText("Nombre de usuario");

        jLabel4.setText("Contraseña");

        txtContraseña.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtContraseñaFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtContraseñaFocusLost(evt);
            }
        });

        btnCancelar.setText("Salir");
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });

        btnAcceder.setText("Accesar");
        btnAcceder.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAccederActionPerformed(evt);
            }
        });

        panel2.setBackground(new java.awt.Color(225, 224, 224));

        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);

        jLabel6.setText("Bloq. Mayus. Activado");

        javax.swing.GroupLayout panel2Layout = new javax.swing.GroupLayout(panel2);
        panel2.setLayout(panel2Layout);
        panel2Layout.setHorizontalGroup(
            panel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 158, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 205, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(19, 19, 19))
        );
        panel2Layout.setVerticalGroup(
            panel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel5, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jLabel6, javax.swing.GroupLayout.DEFAULT_SIZE, 22, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jSeparator1, javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 123, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(btnCancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 88, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(btnAcceder, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel3)
                                    .addComponent(jLabel4, javax.swing.GroupLayout.Alignment.TRAILING))
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(txtContraseña, javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(txtNombreUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, 197, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addGap(0, 8, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 2, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(30, 30, 30)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel3)
                            .addComponent(txtNombreUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(15, 15, 15)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel4)
                            .addComponent(txtContraseña, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(39, 39, 39)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnCancelar)
                            .addComponent(btnAcceder)))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(13, 13, 13)
                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 143, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(panel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed

        //Salir del sistema
        System.exit(1);

    }//GEN-LAST:event_btnCancelarActionPerformed

    private void btnAccederActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAccederActionPerformed
        
        //Recuperar de los campos de texto los datos ingresados por el usuarios

        //Variables que contendran los datos ingresados por el usuario
        String nombreUsuario;
        String contraseña;
        boolean usuarioRegistrado = false; //Variable que determina si un usuario esta registrado en el sistema (originalemnte es falso)

        //Se obtienen los valores de los campos
        nombreUsuario = txtNombreUsuario.getText();
        contraseña = txtContraseña.getText();

        //Comparar los datos ingresados por el usuario para ver si coinciden a los datos de un usuario registrado

        //Si ninguno de los campos estan vacios
        if (!(nombreUsuario.isEmpty() || contraseña.isEmpty())) {
            //Recuperamos todos los usuarios registrados en la base de datos para compararlos con los valores ingresados por el usuario

            //Lista que contendra los registros de cada uno de los usuarios registrados en la base de datos
            List<Usuario> usuariosLemar;

            //Variable que inicializa sesion en hibernate
            Session session = null;
            try {
                //Se crea la conexion con la base de datos
                sessionFactory = HibernateConexion.getSessionFactory();
                session = sessionFactory.openSession();
                Transaction tx = session.beginTransaction();

                //Se extraen los datos de todos los usuarios registrados en el sistema y se compraran con los datos ingresados por el usuario
                usuariosLemar = session.createQuery("from Usuario").list();
                for (Usuario usuarios : usuariosLemar) {
                    //Si el nombre de usuario ingresado coincide con el nombre de un usuario registrado verificamos si la contraseña tambien coincide
                    if (usuarios.getNombreUsuario().equals(nombreUsuario)) {

                        //Desencriptamos los datos
                        CifradorCesar dc = new CifradorCesar(usuarios.getPassword(), 4);
                        dc.decodCesar();
                        String contraseñaDecodificada = dc.getDecodificada();

                        //Verificamos si la contraseña coincide con la contraseña del usuario registrado
                        if (contraseñaDecodificada.equals(contraseña)) {
                            //El usuario es un usuario registrado por lo tanto permitimos su acceso cambiando el valor de la variable de acceso
                            usuarioRegistrado = true;
                            //Guardamos el nombre de usuario y el rol del usuario para dar acceso unicamente a las pestañas a las cuales posea acceso segun su cuenta de usuario
                            VariablesGlobales.nombreUsuario = usuarios.getNombreUsuario();
                            VariablesGlobales.rol = usuarios.getTipoUsuario();
                            break; //Para que finalice el ciclo y no realice mas comparaciones
                        }
                    }
                }

                //Si el usuario fue entontrado entonces se deja acceder al usuario al sistema
                if (usuarioRegistrado == true) {
                    this.dispose();
                } else //Indicamos al usuario que los datos ingresados no corresponden a los datos de un usuario registrado
                {
                    JOptionPane.showMessageDialog(null, "Los datos ingresados no coinciden con las credenciales de un usuario registrado\nVuelva a intentarlo", "Error de autorización", JOptionPane.ERROR_MESSAGE);
                    //Se limpian los campos de texto
                    txtNombreUsuario.setText("");
                    txtContraseña.setText("");
                }
               
                //Finalizacion de la sesion de hibernate
                session.close();
                
            } catch (org.hibernate.JDBCException e) { //Manejo de las excepciones de hibernate
                JOptionPane.showMessageDialog(null, "No se pudo establecer la conexión con la base de datos", "Error de conexión", JOptionPane.ERROR_MESSAGE);
            }
        } else //Si alguno de los campos se encuentra vacio
        {
            JOptionPane.showMessageDialog(null, "Error, no deben de haber campos vacios", "Error en los datos", JOptionPane.ERROR_MESSAGE);
            txtNombreUsuario.setText("");
            txtContraseña.setText("");
        }

    }//GEN-LAST:event_btnAccederActionPerformed

    private void txtContraseñaFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtContraseñaFocusGained

        //Verificamos si el Bloq Mayus esta activado
        estadoBM = Toolkit.getDefaultToolkit().getLockingKeyState(KeyEvent.VK_CAPS_LOCK);

        //Si esta activo mostramos el mensaje
        if (estadoBM == true) {
            jLabel6.setVisible(true);
        } else {
            jLabel6.setVisible(false);
        }

    }//GEN-LAST:event_txtContraseñaFocusGained

    private void txtContraseñaFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtContraseñaFocusLost

        //Ocultamos el jLabel6
        jLabel6.setVisible(false);

    }//GEN-LAST:event_txtContraseñaFocusLost
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAcceder;
    private javax.swing.JButton btnCancelar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    public static javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JSeparator jSeparator1;
    private java.awt.Panel panel2;
    private javax.swing.JPasswordField txtContraseña;
    private javax.swing.JTextField txtNombreUsuario;
    // End of variables declaration//GEN-END:variables
}
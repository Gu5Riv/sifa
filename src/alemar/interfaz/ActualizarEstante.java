//Interfaz que se encarga de actualizar los datos de un Estante registrado en el sistema
package alemar.interfaz;

import alemar.configuracion.HibernateConexion;
import alemar.entidad.Estante;
import java.util.List;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

public class ActualizarEstante extends javax.swing.JPanel {

    //Atributos para el uso de Hibernate
    private static SessionFactory sessionFactory = null;
    
    //Para guardar el id del proveedor
    private int id;
    
    public ActualizarEstante() {
        initComponents();
        
        //Se carga el comboBox Dinamicamente de la BD
        cargarComboBox();
        
        //Por default estaran desabilitados por si quiere ingresar datos
        //sin haber seleccionado un proveedor alguno
        cmbUbicacionBodega.setEnabled(false);
        cmbNumeroNiveles.setEnabled(false);
        
        cmbUbicacionBodega.setEditable(false);
        cmbNumeroNiveles.setEditable(false);
        
        //Agregando mensajes de ayuda para el llenado de los campos
        cmbUbicacionBodega.setToolTipText("Seleccione la ubicacion del estante");
        cmbNumeroNiveles.setToolTipText("Seleccione el número de niveles");
    }

    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        cmbCodigoEstante = new javax.swing.JComboBox();
        jLabel2 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        btnActualizarEstante = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        btnCancelar = new javax.swing.JButton();
        cmbUbicacionBodega = new javax.swing.JComboBox();
        cmbNumeroNiveles = new javax.swing.JComboBox();

        setBackground(new java.awt.Color(255, 255, 255));
        setMaximumSize(new java.awt.Dimension(745, 400));
        setMinimumSize(new java.awt.Dimension(745, 400));
        setPreferredSize(new java.awt.Dimension(745, 400));

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        jLabel1.setText("Actualizar información del estante");

        cmbCodigoEstante.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        cmbCodigoEstante.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbCodigoEstanteItemStateChanged(evt);
            }
        });

        jLabel2.setText("Código del estante");

        btnActualizarEstante.setText("Actualizar");
        btnActualizarEstante.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnActualizarEstanteActionPerformed(evt);
            }
        });

        jLabel3.setText("Ubicación en bodega");

        jLabel4.setText("Número de niveles");

        btnCancelar.setText("Cancelar");
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });

        cmbUbicacionBodega.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "<--Seleccione un Estante-->", "Bodega", "Mostrador" }));

        cmbNumeroNiveles.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15" }));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(120, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(btnCancelar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnActualizarEstante)
                        .addContainerGap())
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 111, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                    .addComponent(jLabel2)
                                    .addGap(18, 18, 18)
                                    .addComponent(cmbCodigoEstante, javax.swing.GroupLayout.PREFERRED_SIZE, 240, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(158, 158, 158))
                                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(jLabel3)
                                        .addComponent(jLabel4))
                                    .addGap(18, 18, 18)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addComponent(cmbUbicacionBodega, 0, 240, Short.MAX_VALUE)
                                        .addComponent(cmbNumeroNiveles, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                    .addGap(157, 157, 157))))
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 546, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(79, 79, 79)))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addGap(242, 242, 242))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(73, 73, 73)
                .addComponent(jLabel1)
                .addGap(37, 37, 37)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cmbCodigoEstante, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addGap(18, 18, 18)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(34, 34, 34)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(cmbUbicacionBodega, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(cmbNumeroNiveles, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 96, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnActualizarEstante)
                    .addComponent(btnCancelar))
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void cmbCodigoEstanteItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbCodigoEstanteItemStateChanged
        
        String codigoEstante = "";
        
        List<Estante> Lestante = null;
        
        //Se extrae el proveedor seleccionado
        codigoEstante = cmbCodigoEstante.getSelectedItem().toString();
        
        if(!(codigoEstante.equalsIgnoreCase("<--Seleccione un Estante-->"))){
                        
            //Chunche de Hibernate (Inicio de sesion de hibernate)
            Session session = null;
            //Cadena de seleccion de HQL
            String hql = "FROM alemar.entidad.Estante AS p WHERE p.codigoEstante = :codigoEstante";
            try{
                try{
                    //Conexion a la BD
                    sessionFactory = HibernateConexion.getSessionFactory();
                    //Apertura de la sesion
                    session = sessionFactory.openSession();
                    Transaction txt = session.beginTransaction();                                       
                    
                    //Extraccion de los datos del proveedor de la BD en base al nombre proporcionado                    
                    Query query = session.createQuery(hql).setString("codigoEstante", codigoEstante);
                    
                    //Se pone en una lista el registro sacado
                    Lestante = query.list();
                    
                    //Guardando el id del proveedor en una variable global
                    //id = Lestante.get(0).getIdEstante();
                    
                    //Se ponen los valores en los campos de texto correspondientes
                    //para que se modifiquen
                    cmbNumeroNiveles.setEnabled(true);
                    cmbUbicacionBodega.setEnabled(true);
                    
                    txt.commit();
                    
                    }catch(Exception e){
                    System.out.println(e.getMessage());
                }
            } finally {
                //Finalizacion de la sesion de hibernate
                session.close();
                }
        }else{
            cmbUbicacionBodega.setEnabled(false);
            cmbNumeroNiveles.setEnabled(false);
            
            cmbUbicacionBodega.setSelectedIndex(0);
            cmbCodigoEstante.setSelectedIndex(0);
            cmbNumeroNiveles.setSelectedIndex(0);
        }
    }//GEN-LAST:event_cmbCodigoEstanteItemStateChanged

    private void btnActualizarEstanteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnActualizarEstanteActionPerformed
        
        //Variables que contendran los valores de los campos de texto 
        String ubicacionBodega = "";         
        String numeroNiveles = "";
        
        //Variable que maneja la respuesta del JOptionpane
        int conf;
             
        //Extrayendo la informacion de los campos de texto
        ubicacionBodega = cmbUbicacionBodega.getSelectedItem().toString();
        numeroNiveles = cmbNumeroNiveles.getSelectedItem().toString();
        int numNiveles = Integer.parseInt(numeroNiveles);
        
        //Validaciones
        if(!(ubicacionBodega.isEmpty() || 0 == ubicacionBodega.compareTo("<--Seleccione un Estante-->"))){ //Si no esta vacio algun campo           
        
            //Chunche de Hibernate (Inicio de sesion de hibernate)
            Session session = null;
            try{
                try{
                    //Conexion a la BD
                    sessionFactory = HibernateConexion.getSessionFactory();
                    //Apertura de la sesion
                    session = sessionFactory.openSession();
                    Transaction tx = session.beginTransaction();                    
                    
                    //Extrayendo el regitro del estante seleccionado
                    //en base al ID guardado previamente de manera global
                    Estante estante = (Estante)session.get(Estante.class, id);
                                        
                    //Ingresando la nueva informacion del estante al 
                    //objeto Estante
                    estante.setUbicacion(ubicacionBodega);
                    estante.setNumeroNiveles(numNiveles);

                    //Mensaje de pregunta si quiere guardar los datos proporcionados 
                    conf=JOptionPane.showConfirmDialog(null,"¿Esta seguro de ingresar esos datos?","Confirmación",JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE);
                    
                    if(conf == JOptionPane.YES_OPTION){//Si la opcion fue SI
                        
                        //Actualiza los datos del proveedor
                        session.update(estante);
                       
                        tx.commit();
                        //Mensaje de confirmacion de insercion Exitosa
                        JOptionPane.showMessageDialog(null,"Registro guardado exitosamente","Información",JOptionPane.INFORMATION_MESSAGE);
                        
                        cmbUbicacionBodega.setEnabled(false);
                        cmbNumeroNiveles.setEnabled(false);

                        cmbUbicacionBodega.setSelectedIndex(0);
                        cmbCodigoEstante.setSelectedIndex(0);
                        cmbNumeroNiveles.setSelectedIndex(0);
                    }
                    else{
                        cmbUbicacionBodega.setSelectedIndex(0);
                        cmbCodigoEstante.setSelectedIndex(0);
                        cmbNumeroNiveles.setSelectedIndex(0);
                    }
                }catch (org.hibernate.exception.ConstraintViolationException cve) {
                    session.getTransaction().rollback();
                    JOptionPane.showMessageDialog(null, "Error: el " + ubicacionBodega + " ya se encuentra asignado a otra Estante", "Duplicidad en registros", JOptionPane.ERROR_MESSAGE);
                    cmbUbicacionBodega.setSelectedIndex(0);
                }
                
            }
            catch (org.hibernate.JDBCException e) {
                JOptionPane.showMessageDialog(null, "Error: No se pueden actualizar los datos de " + ubicacionBodega + " debido a un problema de conexión con la base de datos: ", "Error de conexión", JOptionPane.ERROR_MESSAGE);
            }finally {
                //Finalizacion de la sesion de hibernate
                session.close();
                //Se vuelve a cargar el comboBox
                cargarComboBox();
        }
      }
        else {//Si hay campos vacios, mensaje de error
            JOptionPane.showMessageDialog(null,"Error, no pueden haber campos vacios","Error en los datos",JOptionPane.ERROR_MESSAGE);
            
        } 
        
    }//GEN-LAST:event_btnActualizarEstanteActionPerformed

    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
        
        cmbUbicacionBodega.setEnabled(false);
        cmbNumeroNiveles.setEnabled(false);
        
        cmbUbicacionBodega.setEditable(false);
        cmbNumeroNiveles.setEditable(false);
                        
        cmbUbicacionBodega.setSelectedIndex(0);
        cmbCodigoEstante.setSelectedIndex(0);
        cmbNumeroNiveles.setSelectedIndex(0);
        
    }//GEN-LAST:event_btnCancelarActionPerformed

    //Metodo para cargar el comboBox dinamicamente de la BD
    private void cargarComboBox(){
        
        //Obtencion de todos los proveedores de la base de datos
        //para llenar el comboBox de manera dinamica
        
        //Variable que contendra la lista de proveedores
        List<Estante> listaEstante = null;
        
        //Inicializacion de session de hibernate
        Session session = null;
        try{
            try{
                //Chunces que ocupa Hibernate
                sessionFactory = HibernateConexion.getSessionFactory();
                session = sessionFactory.openSession();
                Transaction tx = session.beginTransaction();
                
                //Obteniendo de la BD todos los proveedores                
                listaEstante = session.createQuery("from Estante").list();
                
                //Creando un modelo de comboBox                
                DefaultComboBoxModel modeloCombo = new DefaultComboBoxModel();
                
                //Poniendole un mensaje de seleccion (Pirmer elemento)
                 modeloCombo.addElement("<--Seleccione un estante-->");
                
                //Llenando el modelo de comboBox con los nombres de los proveedores
                for(Estante p : listaEstante) 
                { 
                    modeloCombo.addElement(p.getCodigoEstante());                     
                }
                
                //Agregando el modelo del comboBox al comboBox del panel
                cmbCodigoEstante.setModel(modeloCombo);
                
            }catch (org.hibernate.JDBCException e) {
                JOptionPane.showMessageDialog(null, "No se puede establecer conexión con la base de datos.", "Error de conexión", JOptionPane.ERROR_MESSAGE);
            }
            }finally {
                session.close();
        }
        
    }
    
    
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnActualizarEstante;
    private javax.swing.JButton btnCancelar;
    private javax.swing.JComboBox cmbCodigoEstante;
    private javax.swing.JComboBox cmbNumeroNiveles;
    private javax.swing.JComboBox cmbUbicacionBodega;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JSeparator jSeparator1;
    // End of variables declaration//GEN-END:variables
}

package alemar.interfaz;

import alemar.clases.VariablesGlobales;
import java.awt.BorderLayout;
import java.awt.Image;
import java.io.File;
import java.net.URL;
import javax.help.HelpBroker;
import javax.help.HelpSet;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import org.jvnet.substance.SubstanceLookAndFeel;

public class Principal extends javax.swing.JFrame {

    Notificaciones mostrar = new Notificaciones(this, true);

    public Principal() {

        JFrame.setDefaultLookAndFeelDecorated(true);
        SubstanceLookAndFeel.setSkin("org.jvnet.substance.skin.OfficeSilver2007Skin");

        super.setTitle("Principal");
        initComponents();

        //Cargamos el icono
        Image ico = new ImageIcon(getClass().getResource("/alemar/imagen/iconoEscritorio.png")).getImage();
        setIconImage(ico);
        cargarAyuda();
        super.setLocationRelativeTo(null);

        //Se carga el panel de acceso al sistema (Pantalla de Login)
        Login login = new Login(this, true);
        login.setVisible(true);

        //Cambiar icono de notificaciones

        if (mostrar.estadoNotif() == true) {
            Icon iconoNotif = new ImageIcon(getClass().getResource("/alemar/imagen/NotifON.png"));
            btnNotificaciones.setIcon(iconoNotif);
        } else {
            Icon iconoNotif = new ImageIcon(getClass().getResource("/alemar/imagen/Notif.png"));
            btnNotificaciones.setIcon(iconoNotif);
        }

        jLabel2.setText("Usuario:  " + VariablesGlobales.nombreUsuario);
        jLabel3.setText("Rol:  " + VariablesGlobales.rol);
    }

    //Metodo que sirve para cargar la ayuda del sistema usando el boton F1
    private void cargarAyuda() {

        try {
            //Cargando el fichero de ayuda
            File fichero = new File("Ayuda/ayuda.hs");
            URL hsURL = fichero.toURI().toURL();

            //Creando el HelpSet y el HelpBroker
            HelpSet helpset = new HelpSet(getClass().getClassLoader(), hsURL);
            HelpBroker hb = helpset.createHelpBroker();

            //Desplegar ayuda a trave del boton F1
            hb.enableHelpKey(this.getContentPane(), "inicio", helpset);

            //Desplegar ayuda a traves del boton de ayuda del panel
            hb.enableHelpOnButton(btnAyuda, "inicio", helpset, "javax.help.SecondaryWindow", "");

        } catch (Exception e) {
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnAyuda = new javax.swing.JButton();
        jSeparator1 = new javax.swing.JSeparator();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        btnAgregarProducto = new javax.swing.JButton();
        btnEliminaProducto = new javax.swing.JButton();
        btnActualizarProducto = new javax.swing.JButton();
        btnConsultarProducto = new javax.swing.JButton();
        AreaTrabScrollProducto = new javax.swing.JScrollPane();
        jPanel3 = new javax.swing.JPanel();
        btnAgregarMascota = new javax.swing.JButton();
        btnEliminaMascota = new javax.swing.JButton();
        btnActualizarMascota = new javax.swing.JButton();
        btnConsultarMascota = new javax.swing.JButton();
        AreaTrabScrollMascota = new javax.swing.JScrollPane();
        jPanel4 = new javax.swing.JPanel();
        btnAgregarEstante = new javax.swing.JButton();
        btnEliminaEstante = new javax.swing.JButton();
        btnActualizarEstante = new javax.swing.JButton();
        btnConsultarEstante = new javax.swing.JButton();
        AreaTrabScrollEstante = new javax.swing.JScrollPane();
        jPanel5 = new javax.swing.JPanel();
        btnAgregarArea = new javax.swing.JButton();
        btnEliminarArea = new javax.swing.JButton();
        btnActualizarArea = new javax.swing.JButton();
        btnConsultarArea = new javax.swing.JButton();
        AreaTrabScrollArea = new javax.swing.JScrollPane();
        jPanel6 = new javax.swing.JPanel();
        btnAgregarProveedor = new javax.swing.JButton();
        btnEliminarProveedor = new javax.swing.JButton();
        btnActualizarProveedor = new javax.swing.JButton();
        btnConsultarProveedor = new javax.swing.JButton();
        AreaTrabScrollProveedor = new javax.swing.JScrollPane();
        jPanel7 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        jPanel8 = new javax.swing.JPanel();
        btnAgregarUsuario = new javax.swing.JButton();
        btnEliminarUsuario = new javax.swing.JButton();
        btnActualizarUsuario = new javax.swing.JButton();
        btnConsultarUsuario = new javax.swing.JButton();
        AreaTrabScrollUsuario = new javax.swing.JScrollPane();
        jPanel9 = new javax.swing.JPanel();
        btnStockMinimo = new javax.swing.JButton();
        btnFechaVencimiento = new javax.swing.JButton();
        AreaTrabScrollNotificaciones = new javax.swing.JScrollPane();
        btnNotificaciones = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jMenuBar2 = new javax.swing.JMenuBar();
        MenuArchivo = new javax.swing.JMenu();
        MenuCambiarUsuario = new javax.swing.JMenuItem();
        MenuCerrar = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(255, 255, 255));
        setMaximumSize(new java.awt.Dimension(850, 670));
        setMinimumSize(new java.awt.Dimension(850, 670));
        setPreferredSize(new java.awt.Dimension(850, 670));
        setResizable(false);

        btnAyuda.setIcon(new javax.swing.ImageIcon(getClass().getResource("/alemar/imagen/Ayuda.png"))); // NOI18N
        btnAyuda.setBorder(null);
        btnAyuda.setBorderPainted(false);
        btnAyuda.setContentAreaFilled(false);
        btnAyuda.setMaximumSize(new java.awt.Dimension(30, 30));
        btnAyuda.setMinimumSize(new java.awt.Dimension(30, 30));
        btnAyuda.setPreferredSize(new java.awt.Dimension(30, 30));
        btnAyuda.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAyudaActionPerformed(evt);
            }
        });

        jTabbedPane1.setBackground(new java.awt.Color(255, 255, 255));
        jTabbedPane1.setMaximumSize(new java.awt.Dimension(830, 558));
        jTabbedPane1.setMinimumSize(new java.awt.Dimension(830, 558));
        jTabbedPane1.setPreferredSize(new java.awt.Dimension(830, 558));
        jTabbedPane1.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                jTabbedPane1StateChanged(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 825, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 532, Short.MAX_VALUE)
        );

        jTabbedPane1.addTab("Facturación", jPanel1);

        btnAgregarProducto.setText("Agregar Producto");
        btnAgregarProducto.setMaximumSize(new java.awt.Dimension(135, 50));
        btnAgregarProducto.setMinimumSize(new java.awt.Dimension(135, 50));
        btnAgregarProducto.setPreferredSize(new java.awt.Dimension(135, 50));
        btnAgregarProducto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAgregarProductoActionPerformed(evt);
            }
        });

        btnEliminaProducto.setText("Eliminar Producto");
        btnEliminaProducto.setMaximumSize(new java.awt.Dimension(135, 50));
        btnEliminaProducto.setMinimumSize(new java.awt.Dimension(135, 50));
        btnEliminaProducto.setPreferredSize(new java.awt.Dimension(135, 50));
        btnEliminaProducto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEliminaProductoActionPerformed(evt);
            }
        });

        btnActualizarProducto.setText("Actualizar Producto");
        btnActualizarProducto.setMaximumSize(new java.awt.Dimension(135, 50));
        btnActualizarProducto.setMinimumSize(new java.awt.Dimension(135, 50));
        btnActualizarProducto.setPreferredSize(new java.awt.Dimension(135, 50));
        btnActualizarProducto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnActualizarProductoActionPerformed(evt);
            }
        });

        btnConsultarProducto.setText("Consultar Producto");
        btnConsultarProducto.setMaximumSize(new java.awt.Dimension(135, 50));
        btnConsultarProducto.setMinimumSize(new java.awt.Dimension(135, 50));
        btnConsultarProducto.setPreferredSize(new java.awt.Dimension(135, 50));
        btnConsultarProducto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnConsultarProductoActionPerformed(evt);
            }
        });

        AreaTrabScrollProducto.setBackground(new java.awt.Color(255, 255, 255));
        AreaTrabScrollProducto.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 102, 51)));
        AreaTrabScrollProducto.setMaximumSize(new java.awt.Dimension(755, 442));
        AreaTrabScrollProducto.setMinimumSize(new java.awt.Dimension(755, 442));
        AreaTrabScrollProducto.setPreferredSize(new java.awt.Dimension(755, 442));

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(AreaTrabScrollProducto, javax.swing.GroupLayout.DEFAULT_SIZE, 805, Short.MAX_VALUE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(btnAgregarProducto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnEliminaProducto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnActualizarProducto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnConsultarProducto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnConsultarProducto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnActualizarProducto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnEliminaProducto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnAgregarProducto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(AreaTrabScrollProducto, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        jTabbedPane1.addTab("Productos", jPanel2);

        btnAgregarMascota.setText("Agregar Mascota");
        btnAgregarMascota.setMaximumSize(new java.awt.Dimension(135, 50));
        btnAgregarMascota.setMinimumSize(new java.awt.Dimension(135, 50));
        btnAgregarMascota.setPreferredSize(new java.awt.Dimension(135, 50));
        btnAgregarMascota.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAgregarMascotaActionPerformed(evt);
            }
        });

        btnEliminaMascota.setText("Eliminar Mascota");
        btnEliminaMascota.setMaximumSize(new java.awt.Dimension(135, 50));
        btnEliminaMascota.setMinimumSize(new java.awt.Dimension(135, 50));
        btnEliminaMascota.setPreferredSize(new java.awt.Dimension(135, 50));
        btnEliminaMascota.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEliminaMascotaActionPerformed(evt);
            }
        });

        btnActualizarMascota.setText("Actualizar Mascota");
        btnActualizarMascota.setMaximumSize(new java.awt.Dimension(135, 50));
        btnActualizarMascota.setMinimumSize(new java.awt.Dimension(135, 50));
        btnActualizarMascota.setPreferredSize(new java.awt.Dimension(135, 50));
        btnActualizarMascota.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnActualizarMascotaActionPerformed(evt);
            }
        });

        btnConsultarMascota.setText("Consultar Mascota");
        btnConsultarMascota.setMaximumSize(new java.awt.Dimension(135, 50));
        btnConsultarMascota.setMinimumSize(new java.awt.Dimension(135, 50));
        btnConsultarMascota.setPreferredSize(new java.awt.Dimension(135, 50));
        btnConsultarMascota.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnConsultarMascotaActionPerformed(evt);
            }
        });

        AreaTrabScrollMascota.setBackground(new java.awt.Color(255, 255, 255));
        AreaTrabScrollMascota.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 102, 51)));
        AreaTrabScrollMascota.setMaximumSize(new java.awt.Dimension(755, 442));
        AreaTrabScrollMascota.setMinimumSize(new java.awt.Dimension(755, 442));
        AreaTrabScrollMascota.setPreferredSize(new java.awt.Dimension(755, 442));

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(AreaTrabScrollMascota, javax.swing.GroupLayout.DEFAULT_SIZE, 805, Short.MAX_VALUE)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(btnAgregarMascota, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnEliminaMascota, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnActualizarMascota, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnConsultarMascota, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnConsultarMascota, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnActualizarMascota, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnEliminaMascota, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnAgregarMascota, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(AreaTrabScrollMascota, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        jTabbedPane1.addTab("Mascotas", jPanel3);

        btnAgregarEstante.setText("Agregar Estante");
        btnAgregarEstante.setMaximumSize(new java.awt.Dimension(135, 50));
        btnAgregarEstante.setMinimumSize(new java.awt.Dimension(135, 50));
        btnAgregarEstante.setPreferredSize(new java.awt.Dimension(135, 50));
        btnAgregarEstante.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAgregarEstanteActionPerformed(evt);
            }
        });

        btnEliminaEstante.setText("Eliminar Estante");
        btnEliminaEstante.setMaximumSize(new java.awt.Dimension(135, 50));
        btnEliminaEstante.setMinimumSize(new java.awt.Dimension(135, 50));
        btnEliminaEstante.setPreferredSize(new java.awt.Dimension(135, 50));
        btnEliminaEstante.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEliminaEstanteActionPerformed(evt);
            }
        });

        btnActualizarEstante.setText("Actualizar Estante");
        btnActualizarEstante.setMaximumSize(new java.awt.Dimension(135, 50));
        btnActualizarEstante.setMinimumSize(new java.awt.Dimension(135, 50));
        btnActualizarEstante.setPreferredSize(new java.awt.Dimension(135, 50));
        btnActualizarEstante.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnActualizarEstanteActionPerformed(evt);
            }
        });

        btnConsultarEstante.setText("Consultar Estante");
        btnConsultarEstante.setMaximumSize(new java.awt.Dimension(135, 50));
        btnConsultarEstante.setMinimumSize(new java.awt.Dimension(135, 50));
        btnConsultarEstante.setPreferredSize(new java.awt.Dimension(135, 50));
        btnConsultarEstante.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnConsultarEstanteActionPerformed(evt);
            }
        });

        AreaTrabScrollEstante.setBackground(new java.awt.Color(255, 255, 255));
        AreaTrabScrollEstante.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 102, 51)));
        AreaTrabScrollEstante.setMaximumSize(new java.awt.Dimension(755, 442));
        AreaTrabScrollEstante.setMinimumSize(new java.awt.Dimension(755, 442));
        AreaTrabScrollEstante.setPreferredSize(new java.awt.Dimension(755, 442));

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(AreaTrabScrollEstante, javax.swing.GroupLayout.DEFAULT_SIZE, 805, Short.MAX_VALUE)
                        .addContainerGap())
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(btnAgregarEstante, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnEliminaEstante, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnActualizarEstante, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnConsultarEstante, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnConsultarEstante, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnActualizarEstante, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnEliminaEstante, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnAgregarEstante, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(AreaTrabScrollEstante, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        jTabbedPane1.addTab("Estantes", jPanel4);

        btnAgregarArea.setText("Agregar Área");
        btnAgregarArea.setMaximumSize(new java.awt.Dimension(135, 50));
        btnAgregarArea.setMinimumSize(new java.awt.Dimension(135, 50));
        btnAgregarArea.setPreferredSize(new java.awt.Dimension(135, 50));
        btnAgregarArea.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAgregarAreaActionPerformed(evt);
            }
        });

        btnEliminarArea.setText("Eliminar Área");
        btnEliminarArea.setMaximumSize(new java.awt.Dimension(135, 50));
        btnEliminarArea.setMinimumSize(new java.awt.Dimension(135, 50));
        btnEliminarArea.setPreferredSize(new java.awt.Dimension(135, 50));
        btnEliminarArea.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEliminarAreaActionPerformed(evt);
            }
        });

        btnActualizarArea.setText("Actualizar Área");
        btnActualizarArea.setMaximumSize(new java.awt.Dimension(135, 50));
        btnActualizarArea.setMinimumSize(new java.awt.Dimension(135, 50));
        btnActualizarArea.setPreferredSize(new java.awt.Dimension(135, 50));
        btnActualizarArea.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnActualizarAreaActionPerformed(evt);
            }
        });

        btnConsultarArea.setText("Consultar Área");
        btnConsultarArea.setMaximumSize(new java.awt.Dimension(135, 50));
        btnConsultarArea.setMinimumSize(new java.awt.Dimension(135, 50));
        btnConsultarArea.setPreferredSize(new java.awt.Dimension(135, 50));
        btnConsultarArea.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnConsultarAreaActionPerformed(evt);
            }
        });

        AreaTrabScrollArea.setBackground(new java.awt.Color(255, 255, 255));
        AreaTrabScrollArea.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 102, 51)));
        AreaTrabScrollArea.setMaximumSize(new java.awt.Dimension(755, 442));
        AreaTrabScrollArea.setMinimumSize(new java.awt.Dimension(755, 442));
        AreaTrabScrollArea.setName(""); // NOI18N
        AreaTrabScrollArea.setPreferredSize(new java.awt.Dimension(755, 442));

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(AreaTrabScrollArea, javax.swing.GroupLayout.DEFAULT_SIZE, 805, Short.MAX_VALUE)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addComponent(btnAgregarArea, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnEliminarArea, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnActualizarArea, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnConsultarArea, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnConsultarArea, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnActualizarArea, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnEliminarArea, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnAgregarArea, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(AreaTrabScrollArea, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        jTabbedPane1.addTab("Áreas", jPanel5);

        btnAgregarProveedor.setText("Agregar Proveedor");
        btnAgregarProveedor.setMaximumSize(new java.awt.Dimension(135, 50));
        btnAgregarProveedor.setMinimumSize(new java.awt.Dimension(135, 50));
        btnAgregarProveedor.setPreferredSize(new java.awt.Dimension(135, 50));
        btnAgregarProveedor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAgregarProveedorActionPerformed(evt);
            }
        });

        btnEliminarProveedor.setText("Eliminar Proveedor");
        btnEliminarProveedor.setMaximumSize(new java.awt.Dimension(135, 50));
        btnEliminarProveedor.setMinimumSize(new java.awt.Dimension(135, 50));
        btnEliminarProveedor.setPreferredSize(new java.awt.Dimension(135, 50));
        btnEliminarProveedor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEliminarProveedorActionPerformed(evt);
            }
        });

        btnActualizarProveedor.setText("Actualizar Proveedor");
        btnActualizarProveedor.setMaximumSize(new java.awt.Dimension(135, 50));
        btnActualizarProveedor.setMinimumSize(new java.awt.Dimension(135, 50));
        btnActualizarProveedor.setPreferredSize(new java.awt.Dimension(135, 50));
        btnActualizarProveedor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnActualizarProveedorActionPerformed(evt);
            }
        });

        btnConsultarProveedor.setText("Consultar Proveedor");
        btnConsultarProveedor.setMaximumSize(new java.awt.Dimension(135, 50));
        btnConsultarProveedor.setMinimumSize(new java.awt.Dimension(135, 50));
        btnConsultarProveedor.setPreferredSize(new java.awt.Dimension(135, 50));
        btnConsultarProveedor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnConsultarProveedorActionPerformed(evt);
            }
        });

        AreaTrabScrollProveedor.setBackground(new java.awt.Color(255, 255, 255));
        AreaTrabScrollProveedor.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 102, 51)));
        AreaTrabScrollProveedor.setMaximumSize(new java.awt.Dimension(755, 442));
        AreaTrabScrollProveedor.setMinimumSize(new java.awt.Dimension(755, 442));
        AreaTrabScrollProveedor.setPreferredSize(new java.awt.Dimension(755, 442));

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(AreaTrabScrollProveedor, javax.swing.GroupLayout.DEFAULT_SIZE, 805, Short.MAX_VALUE)
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addComponent(btnAgregarProveedor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnEliminarProveedor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnActualizarProveedor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnConsultarProveedor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnConsultarProveedor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnActualizarProveedor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnEliminarProveedor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnAgregarProveedor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(AreaTrabScrollProveedor, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        jTabbedPane1.addTab("Proveedores", jPanel6);

        jLabel5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/alemar/imagen/construccion.png"))); // NOI18N

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, 805, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, 510, Short.MAX_VALUE)
                .addContainerGap())
        );

        jTabbedPane1.addTab("Reportes", jPanel7);

        btnAgregarUsuario.setText("Agregar Usuario");
        btnAgregarUsuario.setMaximumSize(new java.awt.Dimension(135, 50));
        btnAgregarUsuario.setMinimumSize(new java.awt.Dimension(135, 50));
        btnAgregarUsuario.setPreferredSize(new java.awt.Dimension(135, 50));
        btnAgregarUsuario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAgregarUsuarioActionPerformed(evt);
            }
        });

        btnEliminarUsuario.setText("Eliminar Usuario");
        btnEliminarUsuario.setMaximumSize(new java.awt.Dimension(135, 50));
        btnEliminarUsuario.setMinimumSize(new java.awt.Dimension(135, 50));
        btnEliminarUsuario.setPreferredSize(new java.awt.Dimension(135, 50));
        btnEliminarUsuario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEliminarUsuarioActionPerformed(evt);
            }
        });

        btnActualizarUsuario.setText("Actualizar Usuario");
        btnActualizarUsuario.setMaximumSize(new java.awt.Dimension(135, 50));
        btnActualizarUsuario.setMinimumSize(new java.awt.Dimension(135, 50));
        btnActualizarUsuario.setPreferredSize(new java.awt.Dimension(135, 50));
        btnActualizarUsuario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnActualizarUsuarioActionPerformed(evt);
            }
        });

        btnConsultarUsuario.setText("Consultar Usuario");
        btnConsultarUsuario.setMaximumSize(new java.awt.Dimension(135, 50));
        btnConsultarUsuario.setMinimumSize(new java.awt.Dimension(135, 50));
        btnConsultarUsuario.setPreferredSize(new java.awt.Dimension(135, 50));
        btnConsultarUsuario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnConsultarUsuarioActionPerformed(evt);
            }
        });

        AreaTrabScrollUsuario.setBackground(new java.awt.Color(255, 255, 255));
        AreaTrabScrollUsuario.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 102, 51)));
        AreaTrabScrollUsuario.setMaximumSize(new java.awt.Dimension(755, 442));
        AreaTrabScrollUsuario.setMinimumSize(new java.awt.Dimension(755, 442));
        AreaTrabScrollUsuario.setPreferredSize(new java.awt.Dimension(755, 442));

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(AreaTrabScrollUsuario, javax.swing.GroupLayout.DEFAULT_SIZE, 805, Short.MAX_VALUE)
                    .addGroup(jPanel8Layout.createSequentialGroup()
                        .addComponent(btnAgregarUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnEliminarUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnActualizarUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnConsultarUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnConsultarUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnActualizarUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnEliminarUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnAgregarUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(AreaTrabScrollUsuario, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        jTabbedPane1.addTab("Usuarios", jPanel8);

        btnStockMinimo.setText("Límite Minimo");
        btnStockMinimo.setMaximumSize(new java.awt.Dimension(135, 50));
        btnStockMinimo.setMinimumSize(new java.awt.Dimension(135, 50));
        btnStockMinimo.setPreferredSize(new java.awt.Dimension(135, 50));
        btnStockMinimo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnStockMinimoActionPerformed(evt);
            }
        });

        btnFechaVencimiento.setText("Límite Fecha Vencimiento");
        btnFechaVencimiento.setMaximumSize(new java.awt.Dimension(160, 50));
        btnFechaVencimiento.setMinimumSize(new java.awt.Dimension(160, 50));
        btnFechaVencimiento.setPreferredSize(new java.awt.Dimension(160, 50));
        btnFechaVencimiento.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnFechaVencimientoActionPerformed(evt);
            }
        });

        AreaTrabScrollNotificaciones.setBackground(new java.awt.Color(255, 255, 255));
        AreaTrabScrollNotificaciones.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 102, 51)));
        AreaTrabScrollNotificaciones.setMaximumSize(new java.awt.Dimension(755, 442));
        AreaTrabScrollNotificaciones.setMinimumSize(new java.awt.Dimension(755, 442));
        AreaTrabScrollNotificaciones.setPreferredSize(new java.awt.Dimension(755, 442));

        javax.swing.GroupLayout jPanel9Layout = new javax.swing.GroupLayout(jPanel9);
        jPanel9.setLayout(jPanel9Layout);
        jPanel9Layout.setHorizontalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(AreaTrabScrollNotificaciones, javax.swing.GroupLayout.DEFAULT_SIZE, 805, Short.MAX_VALUE)
                    .addGroup(jPanel9Layout.createSequentialGroup()
                        .addComponent(btnStockMinimo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnFechaVencimiento, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel9Layout.setVerticalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btnStockMinimo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnFechaVencimiento, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addComponent(AreaTrabScrollNotificaciones, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        jTabbedPane1.addTab("Notificaciones", jPanel9);

        btnNotificaciones.setIcon(new javax.swing.ImageIcon(getClass().getResource("/alemar/imagen/Notif.png"))); // NOI18N
        btnNotificaciones.setBorder(null);
        btnNotificaciones.setBorderPainted(false);
        btnNotificaciones.setContentAreaFilled(false);
        btnNotificaciones.setPreferredSize(new java.awt.Dimension(30, 30));
        btnNotificaciones.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/alemar/imagen/NotifEncima.png"))); // NOI18N
        btnNotificaciones.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNotificacionesActionPerformed(evt);
            }
        });

        MenuArchivo.setText("Archivo");

        MenuCambiarUsuario.setIcon(new javax.swing.ImageIcon(getClass().getResource("/alemar/imagen/Cambiar Session icono.png"))); // NOI18N
        MenuCambiarUsuario.setText("Cambiar usuario");
        MenuCambiarUsuario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                MenuCambiarUsuarioActionPerformed(evt);
            }
        });
        MenuArchivo.add(MenuCambiarUsuario);

        MenuCerrar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/alemar/imagen/cerrar_icono.png"))); // NOI18N
        MenuCerrar.setText("Cerrar aplicación");
        MenuCerrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                MenuCerrarActionPerformed(evt);
            }
        });
        MenuArchivo.add(MenuCerrar);

        jMenuBar2.add(MenuArchivo);

        setJMenuBar(jMenuBar2);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jSeparator1, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, 183, Short.MAX_VALUE)
                            .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnNotificaciones, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnAyuda, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(8, 8, 8))
                    .addComponent(jTabbedPane1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(btnNotificaciones, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 31, Short.MAX_VALUE)
                    .addComponent(btnAyuda, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel3)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 3, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 560, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnActualizarProveedorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnActualizarProveedorActionPerformed

        //Controlar el acceso a la actualizacion de los datos de los proveedores registrados en el sistma 

        if (!VariablesGlobales.rol.equals("Administrador")) { //Si no es el administrador del sistema
            JOptionPane.showMessageDialog(null, "Su cuenta no posee los permisos necesarios para poder acceder a esta opción", "Error de autorización", JOptionPane.WARNING_MESSAGE);
        } else {
            JPanel modificar = new JPanel();
            ActualizarProveedor modProv = new ActualizarProveedor();
            AreaTrabScrollProveedor.setViewportView(modificar);
            modificar.add(modProv, BorderLayout.CENTER);
        }

    }//GEN-LAST:event_btnActualizarProveedorActionPerformed

    private void btnAgregarUsuarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAgregarUsuarioActionPerformed

        //Controlar el acceso al ingreso de nuevos usuarios del sistema

        if (!VariablesGlobales.rol.equals("Administrador")) { //Si no es el administrador del sistema
            JOptionPane.showMessageDialog(null, "Su cuenta no posee los permisos necesarios para poder acceder a esta opción", "Error de autorización", JOptionPane.WARNING_MESSAGE);
        } else {
            JPanel border = new JPanel();
            AgregarUsuario agreUsu = new AgregarUsuario();
            AreaTrabScrollUsuario.setViewportView(border);
            border.add(agreUsu, BorderLayout.CENTER);
        }

    }//GEN-LAST:event_btnAgregarUsuarioActionPerformed

    private void btnEliminarUsuarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEliminarUsuarioActionPerformed

        //Controlar el acceso a la eliminacion de usuarios registrados en el sistema

        if (!VariablesGlobales.rol.equals("Administrador")) { //Si no es el administrador del sistema
            JOptionPane.showMessageDialog(null, "Su cuenta no posee los permisos necesarios para poder acceder a esta opción", "Error de autorización", JOptionPane.WARNING_MESSAGE);
        } else {
            JPanel eliminar = new JPanel();
            EliminarUsuario elimUsuario = new EliminarUsuario();
            AreaTrabScrollUsuario.setViewportView(eliminar);
            eliminar.add(elimUsuario, BorderLayout.CENTER);
        }

    }//GEN-LAST:event_btnEliminarUsuarioActionPerformed

    private void btnActualizarUsuarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnActualizarUsuarioActionPerformed

        //Controlar el acceso a la actualización de los datos de los usuarios reguistrados en el sistema

        if (!VariablesGlobales.rol.equals("Administrador")) { //Si no es el administrador del sistema
            JOptionPane.showMessageDialog(null, "Su cuenta no posee los permisos necesarios para poder acceder a esta opción", "Error de autorización", JOptionPane.WARNING_MESSAGE);
        } else {
            JPanel modificar = new JPanel();
            ActualizarUsuario modifUsuario = new ActualizarUsuario();
            AreaTrabScrollUsuario.setViewportView(modificar);
            modificar.add(modifUsuario, BorderLayout.CENTER);
        }

    }//GEN-LAST:event_btnActualizarUsuarioActionPerformed

    private void btnConsultarUsuarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnConsultarUsuarioActionPerformed

        //Controlar el acceso a la consulta de datos de un usuarios registrado en el sistema

        if (!VariablesGlobales.nombreUsuario.equals("SUPERADMIN")) { //Si no es el administrador del sistema
            JOptionPane.showMessageDialog(null, "Su cuenta no posee los permisos necesarios para poder acceder a esta opción", "Error de autorización", JOptionPane.WARNING_MESSAGE);
        } else {
            JPanel consultar = new JPanel();
            ConsultarUsuario ConsultarUsuario = new ConsultarUsuario();
            AreaTrabScrollUsuario.setViewportView(consultar);
            consultar.add(ConsultarUsuario, BorderLayout.CENTER);
        }

    }//GEN-LAST:event_btnConsultarUsuarioActionPerformed

    private void btnAgregarProveedorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAgregarProveedorActionPerformed

        //Controlar el acceso a la opción de ingreso de nuevos proveedores al sistema 

        if (!VariablesGlobales.rol.equals("Administrador")) { //Si no es el administrador del sistema
            JOptionPane.showMessageDialog(null, "Su cuenta no posee los permisos necesarios para poder acceder a esta opción", "Error de autorización", JOptionPane.WARNING_MESSAGE);
        } else {
            JPanel border = new JPanel();
            AgregarProveedor agreProv = new AgregarProveedor();
            AreaTrabScrollProveedor.setViewportView(border);
            border.add(agreProv, BorderLayout.CENTER);
        }

    }//GEN-LAST:event_btnAgregarProveedorActionPerformed

    private void btnEliminarProveedorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEliminarProveedorActionPerformed

        //Controlar el acceso a la eleminación de proveedores registrados en el sistema 

        if (!VariablesGlobales.rol.equals("Administrador")) { //Si no es el administrador del sistema
            JOptionPane.showMessageDialog(null, "Su cuenta no posee los permisos necesarios para poder acceder a esta opción", "Error de autorización", JOptionPane.WARNING_MESSAGE);
        } else {
            JPanel eliminar = new JPanel();
            EliminarProveedor elimProv = new EliminarProveedor();
            AreaTrabScrollProveedor.setViewportView(eliminar);
            eliminar.add(elimProv, BorderLayout.CENTER);
        }

    }//GEN-LAST:event_btnEliminarProveedorActionPerformed

    private void btnConsultarProveedorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnConsultarProveedorActionPerformed

        //Controlar el acceso a la consulta de datos de un proveedor registrado en el sistema 

        if (!VariablesGlobales.rol.equals("Administrador")) { //Si no es el administrador del sistema
            JOptionPane.showMessageDialog(null, "Su cuenta no posee los permisos necesarios para poder acceder a esta opción", "Error de autorización", JOptionPane.WARNING_MESSAGE);
        } else {
            JPanel consultar = new JPanel();
            ConsultarProveedor conProv = new ConsultarProveedor();
            AreaTrabScrollProveedor.setViewportView(consultar);
            consultar.add(conProv, BorderLayout.CENTER);
        }

    }//GEN-LAST:event_btnConsultarProveedorActionPerformed

    private void btnAgregarEstanteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAgregarEstanteActionPerformed

        //Controlar el acceso a la inserción de nuevos estantes en bodega 

        if (!VariablesGlobales.rol.equals("Administrador") && !VariablesGlobales.rol.equals("Encargado de bodega")) { //Si no es el administrador o el encargado de bodega
            JOptionPane.showMessageDialog(null, "Su cuenta no posee los permisos necesarios para poder acceder a esta opción", "Error de autorización", JOptionPane.WARNING_MESSAGE);
        } else {
            JPanel AgregarEstan = new JPanel();
            AgregarEstante agreEstan = new AgregarEstante();
            AreaTrabScrollEstante.setViewportView(AgregarEstan);
            AgregarEstan.add(agreEstan, BorderLayout.CENTER);
        }

    }//GEN-LAST:event_btnAgregarEstanteActionPerformed

    private void btnEliminaEstanteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEliminaEstanteActionPerformed

        //Controlar el acceso a la eleminación de estantes en bodega registrados en el sistema 
        if (!VariablesGlobales.rol.equals("Administrador") && !VariablesGlobales.rol.equals("Encargado de bodega")) { //Si no es el administrador o el encargado de bodega
            JOptionPane.showMessageDialog(null, "Su cuenta no posee los permisos necesarios para poder acceder a esta opción", "Error de autorización", JOptionPane.WARNING_MESSAGE);
        } else {
            JPanel Eliminar = new JPanel();
            EliminarEstante eliminarEstan = new EliminarEstante();
            AreaTrabScrollEstante.setViewportView(Eliminar);
            Eliminar.add(eliminarEstan, BorderLayout.CENTER);
        }

    }//GEN-LAST:event_btnEliminaEstanteActionPerformed

    private void btnActualizarEstanteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnActualizarEstanteActionPerformed

        //Controlar el acceso la actualización de estantes en bodega registrados en el sistema 
        if (!VariablesGlobales.rol.equals("Administrador") && !VariablesGlobales.rol.equals("Encargado de bodega")) { //Si no es el administrador o el encargado de bodega
            JOptionPane.showMessageDialog(null, "Su cuenta no posee los permisos necesarios para poder acceder a esta opción", "Error de autorización", JOptionPane.WARNING_MESSAGE);
        } else {
            JPanel Modificar = new JPanel();
            ActualizarEstante ModificarEstan = new ActualizarEstante();
            AreaTrabScrollEstante.setViewportView(Modificar);
            Modificar.add(ModificarEstan, BorderLayout.CENTER);
        }

    }//GEN-LAST:event_btnActualizarEstanteActionPerformed

    private void btnConsultarEstanteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnConsultarEstanteActionPerformed

        //Controlar el acceso la consulta de estantes en bodega registrados en el sistema 
        if (!VariablesGlobales.rol.equals("Administrador") && !VariablesGlobales.rol.equals("Encargado de bodega")) { //Si no es el administrador o el encargado de bodega
            JOptionPane.showMessageDialog(null, "Su cuenta no posee los permisos necesarios para poder acceder a esta opción", "Error de autorización", JOptionPane.WARNING_MESSAGE);
        } else {
            JPanel Consultar = new JPanel();
            ConsultarEstante consultarEstan = new ConsultarEstante();
            AreaTrabScrollEstante.setViewportView(Consultar);
            Consultar.add(consultarEstan, BorderLayout.CENTER);
        }

    }//GEN-LAST:event_btnConsultarEstanteActionPerformed

    private void btnAgregarAreaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAgregarAreaActionPerformed

        //Controlar el acceso a la inserción de nuevas areas de productos en el sistema 
        if (!VariablesGlobales.rol.equals("Administrador") && !VariablesGlobales.rol.equals("Encargado de bodega")) { //Si no es el administrador o el encargado de bodega
            JOptionPane.showMessageDialog(null, "Su cuenta no posee los permisos necesarios para poder acceder a esta opción", "Error de autorización", JOptionPane.WARNING_MESSAGE);
        } else {
            JPanel agregar = new JPanel();
            AgregarArea AgregarArea = new AgregarArea();
            AreaTrabScrollArea.setViewportView(agregar);
            agregar.add(AgregarArea, BorderLayout.CENTER);
        }

    }//GEN-LAST:event_btnAgregarAreaActionPerformed

    private void btnEliminarAreaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEliminarAreaActionPerformed

        //Controlar el acceso a la eleminación de areas de productos registradas en el sistema 
        if (!VariablesGlobales.rol.equals("Administrador") && !VariablesGlobales.rol.equals("Encargado de bodega")) { //Si no es el administrador o el encargado de bodega
            JOptionPane.showMessageDialog(null, "Su cuenta no posee los permisos necesarios para poder acceder a esta opción", "Error de autorización", JOptionPane.WARNING_MESSAGE);
        } else {
            JPanel Eliminar = new JPanel();
            EliminarArea EliminarArea = new EliminarArea();
            AreaTrabScrollArea.setViewportView(Eliminar);
            Eliminar.add(EliminarArea, BorderLayout.CENTER);
        }

    }//GEN-LAST:event_btnEliminarAreaActionPerformed

    private void btnActualizarAreaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnActualizarAreaActionPerformed

        //Controlar el acceso a la actualización de los datos de un area de productos registrada en el sistema 
        if (!VariablesGlobales.rol.equals("Administrador") && !VariablesGlobales.rol.equals("Encargado de bodega")) { //Si no es el administrador o el encargado de bodega
            JOptionPane.showMessageDialog(null, "Su cuenta no posee los permisos necesarios para poder acceder a esta opción", "Error de autorización", JOptionPane.WARNING_MESSAGE);
        } else {
            JPanel Modificar = new JPanel();
            ActualizarArea ModificarArea = new ActualizarArea();
            AreaTrabScrollArea.setViewportView(Modificar);
            Modificar.add(ModificarArea, BorderLayout.CENTER);
        }

    }//GEN-LAST:event_btnActualizarAreaActionPerformed

    private void btnConsultarAreaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnConsultarAreaActionPerformed

        //Controlar el acceso a la consulta de los datos de un area de productos registrada en el sistema 
        if (!VariablesGlobales.rol.equals("Administrador") && !VariablesGlobales.rol.equals("Encargado de bodega")) { //Si no es el administrador o el encargado de bodega
            JOptionPane.showMessageDialog(null, "Su cuenta no posee los permisos necesarios para poder acceder a esta opción", "Error de autorización", JOptionPane.WARNING_MESSAGE);
        } else {
            JPanel consultar = new JPanel();
            ConsultarArea ConsultarArea = new ConsultarArea();
            AreaTrabScrollArea.setViewportView(consultar);
            consultar.add(ConsultarArea, BorderLayout.CENTER);
        }

    }//GEN-LAST:event_btnConsultarAreaActionPerformed

    private void btnAyudaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAyudaActionPerformed
        // Carga la ayuda a travez del boton de "Ayuda"

        try {
            //Cargando el fichero de ayuda
            File fichero = new File("Ayuda/ayuda.hs");
            URL hsURL = fichero.toURI().toURL();

            //Creando el HelpSet y el HelpBroker
            HelpSet helpset = new HelpSet(getClass().getClassLoader(), hsURL);
            HelpBroker hb = helpset.createHelpBroker();

            hb.setDisplayed(true);

        } catch (Exception e) {
        }

    }//GEN-LAST:event_btnAyudaActionPerformed

    private void jTabbedPane1StateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_jTabbedPane1StateChanged

        // Carga los paneles por default a cada gestion

        //Carga Prodructos
        JPanel inicioProd = new JPanel();
        InicioProducto iniProd = new InicioProducto();
        AreaTrabScrollProducto.setViewportView(inicioProd);
        inicioProd.add(iniProd, BorderLayout.CENTER);

        //Carga Proveedores
        JPanel inicioProv = new JPanel();
        InicioProveedor iniProv = new InicioProveedor();
        AreaTrabScrollProveedor.setViewportView(inicioProv);
        inicioProv.add(iniProv, BorderLayout.CENTER);

        //Carga Estantes
        JPanel inicioEst = new JPanel();
        InicioEstante iniEst = new InicioEstante();
        AreaTrabScrollEstante.setViewportView(inicioEst);
        inicioEst.add(iniEst, BorderLayout.CENTER);

        //Carga Areas
        JPanel inicioAre = new JPanel();
        InicioArea iniAre = new InicioArea();
        AreaTrabScrollArea.setViewportView(inicioAre);
        inicioAre.add(iniAre, BorderLayout.CENTER);

        //Carga Usuarios
        JPanel inicioUsu = new JPanel();
        InicioUsuario iniUsu = new InicioUsuario();
        AreaTrabScrollUsuario.setViewportView(inicioUsu);
        inicioUsu.add(iniUsu, BorderLayout.CENTER);

        //Carga Mascotas
        JPanel inicioMas = new JPanel();
        InicioMascota iniMas = new InicioMascota();
        AreaTrabScrollMascota.setViewportView(inicioMas);
        inicioMas.add(iniMas, BorderLayout.CENTER);

        //Carga Notificaciones
        JPanel inicioNot = new JPanel();
        InicioNotificacion iniNot = new InicioNotificacion();
        AreaTrabScrollNotificaciones.setViewportView(inicioNot);
        inicioNot.add(iniNot, BorderLayout.CENTER);

    }//GEN-LAST:event_jTabbedPane1StateChanged

    private void btnNotificacionesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNotificacionesActionPerformed

        mostrar.setVisible(true);
//            //mostrar.setLocationRelativeTo(null);
        mostrar.setLocation(666, 110);

    }//GEN-LAST:event_btnNotificacionesActionPerformed

    private void btnAgregarMascotaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAgregarMascotaActionPerformed

        //Controlar el acceso de agregar mascotas al sistema 
        if (!VariablesGlobales.rol.equals("Administrador") && !VariablesGlobales.rol.equals("Gerente de ventas") && !VariablesGlobales.rol.equals("Encargado de bodega")) { //Si no es el administrador o el encargado de bodega
            JOptionPane.showMessageDialog(null, "Su cuenta no posee los permisos necesarios para poder acceder a esta opción", "Error de autorización", JOptionPane.WARNING_MESSAGE);
        } else {
            JPanel agregar = new JPanel();
            AgregarMascota AgregarMascota = new AgregarMascota();
            AreaTrabScrollMascota.setViewportView(agregar);
            agregar.add(AgregarMascota, BorderLayout.CENTER);
        }

    }//GEN-LAST:event_btnAgregarMascotaActionPerformed

    private void btnActualizarMascotaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnActualizarMascotaActionPerformed

        //Controlar el acceso de actualizar mascotas al sistema 
        if (!VariablesGlobales.rol.equals("Administrador") && !VariablesGlobales.rol.equals("Gerente de ventas") && !VariablesGlobales.rol.equals("Encargado de bodega")) { //Si no es el administrador o el encargado de bodega
            JOptionPane.showMessageDialog(null, "Su cuenta no posee los permisos necesarios para poder acceder a esta opción", "Error de autorización", JOptionPane.WARNING_MESSAGE);
        } else {
            JPanel actualizar = new JPanel();
            ActualizarMascota ActualizarMascota = new ActualizarMascota();
            AreaTrabScrollMascota.setViewportView(actualizar);
            actualizar.add(ActualizarMascota, BorderLayout.CENTER);
        }

    }//GEN-LAST:event_btnActualizarMascotaActionPerformed

    private void btnConsultarProductoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnConsultarProductoActionPerformed

        //Controlar el la consulta de los productos registrados en el sistema
        if (!VariablesGlobales.rol.equals("Administrador") && !VariablesGlobales.rol.equals("Gerente de ventas") && !VariablesGlobales.rol.equals("Encargado de bodega") && !VariablesGlobales.rol.equals("Vendedor")) { //Si no es el administrador o el encargado de bodega
            JOptionPane.showMessageDialog(null, "Su cuenta no posee los permisos necesarios para poder acceder a esta opción", "Error de autorización", JOptionPane.WARNING_MESSAGE);
        } else {
            JPanel consultar = new JPanel();
            ConsultarProducto consultarProducto = new ConsultarProducto();
            AreaTrabScrollProducto.setViewportView(consultar);
            consultar.add(consultarProducto, BorderLayout.CENTER);
        }

    }//GEN-LAST:event_btnConsultarProductoActionPerformed

    private void btnAgregarProductoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAgregarProductoActionPerformed

        //Controlar el acceso de agregar productos al sistema
        if (!VariablesGlobales.rol.equals("Administrador") && !VariablesGlobales.rol.equals("Gerente de ventas") && !VariablesGlobales.rol.equals("Encargado de bodega")) { //Si no es el administrador o el encargado de bodega
            JOptionPane.showMessageDialog(null, "Su cuenta no posee los permisos necesarios para poder acceder a esta opción", "Error de autorización", JOptionPane.WARNING_MESSAGE);
        } else {
            JPanel agregar = new JPanel();
            AgregarProducto AgregarProducto = new AgregarProducto();
            AreaTrabScrollProducto.setViewportView(agregar);
            agregar.add(AgregarProducto, BorderLayout.CENTER);
        }

    }//GEN-LAST:event_btnAgregarProductoActionPerformed

    private void btnEliminaProductoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEliminaProductoActionPerformed

        //Controlar la eliminación de productos
        if (!VariablesGlobales.rol.equals("Administrador") && !VariablesGlobales.rol.equals("Gerente de ventas") && !VariablesGlobales.rol.equals("Encargado de bodega")) { //Si no es el administrador o el encargado de bodega
            JOptionPane.showMessageDialog(null, "Su cuenta no posee los permisos necesarios para poder acceder a esta opción", "Error de autorización", JOptionPane.WARNING_MESSAGE);
        } else {
            JPanel eliminar = new JPanel();
            EliminarProducto eliminarProducto = new EliminarProducto();
            AreaTrabScrollProducto.setViewportView(eliminar);
            eliminar.add(eliminarProducto, BorderLayout.CENTER);
        }

    }//GEN-LAST:event_btnEliminaProductoActionPerformed

    private void btnActualizarProductoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnActualizarProductoActionPerformed

        //Controlar la actualización de productos en el sistema
        if (!VariablesGlobales.rol.equals("Administrador") && !VariablesGlobales.rol.equals("Gerente de ventas") && !VariablesGlobales.rol.equals("Encargado de bodega")) { //Si no es el administrador o el encargado de bodega
            JOptionPane.showMessageDialog(null, "Su cuenta no posee los permisos necesarios para poder acceder a esta opción", "Error de autorización", JOptionPane.WARNING_MESSAGE);
        } else {
            JPanel actualizar = new JPanel();
            ActualizarProducto actualizarProducto = new ActualizarProducto();
            AreaTrabScrollProducto.setViewportView(actualizar);
            actualizar.add(actualizarProducto, BorderLayout.CENTER);
        }

    }//GEN-LAST:event_btnActualizarProductoActionPerformed

    private void btnEliminaMascotaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEliminaMascotaActionPerformed

        //Controlar el acceso de actualizar mascotas al sistema 
        if (!VariablesGlobales.rol.equals("Administrador") && !VariablesGlobales.rol.equals("Gerente de ventas") && !VariablesGlobales.rol.equals("Encargado de bodega")) { //Si no es el administrador o el encargado de bodega
            JOptionPane.showMessageDialog(null, "Su cuenta no posee los permisos necesarios para poder acceder a esta opción", "Error de autorización", JOptionPane.WARNING_MESSAGE);
        } else {
            JPanel eliminar = new JPanel();
            EliminarMascota eliminarMascota = new EliminarMascota();
            AreaTrabScrollMascota.setViewportView(eliminar);
            eliminar.add(eliminarMascota, BorderLayout.CENTER);
        }

    }//GEN-LAST:event_btnEliminaMascotaActionPerformed

    private void btnConsultarMascotaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnConsultarMascotaActionPerformed

        //Controlar el acceso a la consulta de los datos de una mascota registrada en el sistema
        if (!VariablesGlobales.rol.equals("Administrador") && !VariablesGlobales.rol.equals("Gerente de ventas") && !VariablesGlobales.rol.equals("Encargado de bodega") && !VariablesGlobales.rol.equals("Vendedor")) { //Si no es el administrador o el encargado de bodega
            JOptionPane.showMessageDialog(null, "Su cuenta no posee los permisos necesarios para poder acceder a esta opción", "Error de autorización", JOptionPane.WARNING_MESSAGE);
        } else {
            JPanel consultar = new JPanel();
            ConsultarMascota consultarMascota = new ConsultarMascota();
            AreaTrabScrollMascota.setViewportView(consultar);
            consultar.add(consultarMascota, BorderLayout.CENTER);
        }

    }//GEN-LAST:event_btnConsultarMascotaActionPerformed

    private void MenuCerrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_MenuCerrarActionPerformed

        //Salir del sistema
        System.exit(0);

    }//GEN-LAST:event_MenuCerrarActionPerformed

    private void MenuCambiarUsuarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_MenuCambiarUsuarioActionPerformed

        //Se carga el panel de acceso al sistema (Pantalla de Login)
        CambiarUsuario cambiar = new CambiarUsuario(this, true);
        cambiar.setVisible(true);

        // Carga los paneles por default a cada gestion

        //Carga Prodructos
        JPanel inicioProd = new JPanel();
        InicioProducto iniProd = new InicioProducto();
        AreaTrabScrollProducto.setViewportView(inicioProd);
        inicioProd.add(iniProd, BorderLayout.CENTER);

        //Carga Proveedores
        JPanel inicioProv = new JPanel();
        InicioProveedor iniProv = new InicioProveedor();
        AreaTrabScrollProveedor.setViewportView(inicioProv);
        inicioProv.add(iniProv, BorderLayout.CENTER);

        //Carga Estantes
        JPanel inicioEst = new JPanel();
        InicioEstante iniEst = new InicioEstante();
        AreaTrabScrollEstante.setViewportView(inicioEst);
        inicioEst.add(iniEst, BorderLayout.CENTER);

        //Carga Areas
        JPanel inicioAre = new JPanel();
        InicioArea iniAre = new InicioArea();
        AreaTrabScrollArea.setViewportView(inicioAre);
        inicioAre.add(iniAre, BorderLayout.CENTER);

        //Carga Usuarios
        JPanel inicioUsu = new JPanel();
        InicioUsuario iniUsu = new InicioUsuario();
        AreaTrabScrollUsuario.setViewportView(inicioUsu);
        inicioUsu.add(iniUsu, BorderLayout.CENTER);

        //Carga Mascotas
        JPanel inicioMas = new JPanel();
        InicioMascota iniMas = new InicioMascota();
        AreaTrabScrollMascota.setViewportView(inicioMas);
        inicioMas.add(iniMas, BorderLayout.CENTER);

        //Carga Notificaciones
        JPanel inicioNot = new JPanel();
        InicioNotificacion iniNot = new InicioNotificacion();
        AreaTrabScrollNotificaciones.setViewportView(inicioNot);
        inicioNot.add(iniNot, BorderLayout.CENTER);

    }//GEN-LAST:event_MenuCambiarUsuarioActionPerformed

    private void btnStockMinimoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnStockMinimoActionPerformed

        //Controlar el acceso al cambio de stock en los productos
        if (!VariablesGlobales.rol.equals("Administrador") && !VariablesGlobales.rol.equals("Gerente de ventas")) { //Si no es el administrador o el encargado de bodega
            JOptionPane.showMessageDialog(null, "Su cuenta no posee los permisos necesarios para poder acceder a esta opción", "Error de autorización", JOptionPane.WARNING_MESSAGE);
        } else {
            JPanel EstablecerLimVen = new JPanel();
            EstablecerLimites EstaLimVen = new EstablecerLimites();
            AreaTrabScrollNotificaciones.setViewportView(EstablecerLimVen);
            EstablecerLimVen.add(EstaLimVen, BorderLayout.CENTER);
        }

    }//GEN-LAST:event_btnStockMinimoActionPerformed

    private void btnFechaVencimientoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnFechaVencimientoActionPerformed

        //Controlar el acceso al cambio de 
        if (!VariablesGlobales.rol.equals("Administrador") && !VariablesGlobales.rol.equals("Gerente de ventas")) { //Si no es el administrador o el encargado de bodega
            JOptionPane.showMessageDialog(null, "Su cuenta no posee los permisos necesarios para poder acceder a esta opción", "Error de autorización", JOptionPane.WARNING_MESSAGE);
        } else {
            JPanel EstablecerLimVen = new JPanel();
            EstablecerLimitesVencimiento EstaLimVen = new EstablecerLimitesVencimiento();
            AreaTrabScrollNotificaciones.setViewportView(EstablecerLimVen);
            EstablecerLimVen.add(EstaLimVen, BorderLayout.CENTER);
        }

    }//GEN-LAST:event_btnFechaVencimientoActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Principal().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JScrollPane AreaTrabScrollArea;
    private javax.swing.JScrollPane AreaTrabScrollEstante;
    private javax.swing.JScrollPane AreaTrabScrollMascota;
    private javax.swing.JScrollPane AreaTrabScrollNotificaciones;
    private javax.swing.JScrollPane AreaTrabScrollProducto;
    private javax.swing.JScrollPane AreaTrabScrollProveedor;
    private javax.swing.JScrollPane AreaTrabScrollUsuario;
    private javax.swing.JMenu MenuArchivo;
    private javax.swing.JMenuItem MenuCambiarUsuario;
    private javax.swing.JMenuItem MenuCerrar;
    private javax.swing.JButton btnActualizarArea;
    private javax.swing.JButton btnActualizarEstante;
    private javax.swing.JButton btnActualizarMascota;
    private javax.swing.JButton btnActualizarProducto;
    private javax.swing.JButton btnActualizarProveedor;
    private javax.swing.JButton btnActualizarUsuario;
    private javax.swing.JButton btnAgregarArea;
    private javax.swing.JButton btnAgregarEstante;
    private javax.swing.JButton btnAgregarMascota;
    private javax.swing.JButton btnAgregarProducto;
    private javax.swing.JButton btnAgregarProveedor;
    private javax.swing.JButton btnAgregarUsuario;
    private javax.swing.JButton btnAyuda;
    private javax.swing.JButton btnConsultarArea;
    private javax.swing.JButton btnConsultarEstante;
    private javax.swing.JButton btnConsultarMascota;
    private javax.swing.JButton btnConsultarProducto;
    private javax.swing.JButton btnConsultarProveedor;
    private javax.swing.JButton btnConsultarUsuario;
    private javax.swing.JButton btnEliminaEstante;
    private javax.swing.JButton btnEliminaMascota;
    private javax.swing.JButton btnEliminaProducto;
    private javax.swing.JButton btnEliminarArea;
    private javax.swing.JButton btnEliminarProveedor;
    private javax.swing.JButton btnEliminarUsuario;
    private javax.swing.JButton btnFechaVencimiento;
    public javax.swing.JButton btnNotificaciones;
    private javax.swing.JButton btnStockMinimo;
    protected static javax.swing.JLabel jLabel2;
    protected static javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JMenuBar jMenuBar2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JTabbedPane jTabbedPane1;
    // End of variables declaration//GEN-END:variables
}

//Interfaz que se encarga de consultar los datos de un Proveedor previamente registrado
package alemar.interfaz;

import alemar.clases.CargarCMBProveedor;
import alemar.configuracion.HibernateConexion;
import alemar.entidad.Proveedor;
import java.util.List;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

public class ConsultarProveedor extends javax.swing.JPanel {

    //Atributos para el uso de Hibernate
    private static SessionFactory sessionFactory = null;
    //Lista que contendra los usuarios registrados en el sistema
    List<Proveedor> listaProveedor = null;

    public ConsultarProveedor() {
        initComponents();
        //Se carga el comboBox Dinamicamente de la BD
        cargarComboBox();
    }
    
    //Metodo para cargar el comboBox dinamicamente de la BD
    private void cargarComboBox(){
       //Se inicializa la lista a null
        listaProveedor = null;

        //Instanciamos la clase CargarCMBUsuario para cargar todos los usuarios registrados en una lista
        CargarCMBProveedor cargarProveedores = new CargarCMBProveedor();
        listaProveedor = cargarProveedores.cargarProveedores();

        //Creando un modelo de comboBox                
        DefaultComboBoxModel modeloCombo = new DefaultComboBoxModel();

        //Poniendole un mensaje de seleccion (Primer elemento) al combo box
        modeloCombo.addElement("<--Seleccione un Proveedor-->");

        //Llenando el modelo de comboBox con los nombres de los usuarios
        for (Proveedor proveedor : listaProveedor) {
            //Si el usuario es el superadministrador no lo cargamos
            
                modeloCombo.addElement(proveedor.getNombre());
            
        }
        //Agregando el modelo del comboBox al comboBox del panel
        cmbNombreProveedor.setModel(modeloCombo);  
            
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnConsultarProveedor = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        txtDireccionProveedor = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        txtTelefonoProveedor = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        cmbNombreProveedor = new javax.swing.JComboBox();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtProductosProveedor = new javax.swing.JTextArea();
        jLabel6 = new javax.swing.JLabel();
        txtemail = new javax.swing.JTextField();

        setBackground(new java.awt.Color(255, 255, 255));
        setMaximumSize(new java.awt.Dimension(745, 400));
        setMinimumSize(new java.awt.Dimension(745, 400));
        setPreferredSize(new java.awt.Dimension(745, 400));

        btnConsultarProveedor.setText("Consultar");
        btnConsultarProveedor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnConsultarProveedorActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        jLabel1.setText("Consultar datos de proveedor");

        jLabel2.setText("Nombre del proveedor");

        txtDireccionProveedor.setEditable(false);

        jLabel3.setText("Dirección");

        txtTelefonoProveedor.setEditable(false);

        jLabel4.setText("Teléfono");

        jLabel5.setText("Productos que oferta");

        cmbNombreProveedor.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        txtProductosProveedor.setEditable(false);
        txtProductosProveedor.setBackground(new java.awt.Color(240, 240, 240));
        txtProductosProveedor.setColumns(20);
        txtProductosProveedor.setRows(5);
        jScrollPane1.setViewportView(txtProductosProveedor);

        jLabel6.setText("E-mail");

        txtemail.setBackground(new java.awt.Color(240, 240, 240));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(148, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addGap(18, 18, 18)
                        .addComponent(cmbNombreProveedor, javax.swing.GroupLayout.PREFERRED_SIZE, 240, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(195, 195, 195))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 540, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(57, 57, 57))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addGap(18, 18, 18)
                        .addComponent(txtDireccionProveedor, javax.swing.GroupLayout.PREFERRED_SIZE, 240, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(204, 204, 204))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(btnConsultarProveedor)
                        .addGap(49, 49, 49))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jLabel4)
                        .addGap(18, 18, 18)
                        .addComponent(txtTelefonoProveedor, javax.swing.GroupLayout.PREFERRED_SIZE, 240, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(204, 204, 204))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel5)
                            .addComponent(jLabel6))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtemail, javax.swing.GroupLayout.PREFERRED_SIZE, 240, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 240, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(204, 204, 204))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addGap(241, 241, 241))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(60, 60, 60)
                .addComponent(jLabel1)
                .addGap(27, 27, 27)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cmbNombreProveedor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(txtDireccionProveedor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(txtTelefonoProveedor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtemail, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 13, Short.MAX_VALUE)
                .addComponent(btnConsultarProveedor)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnConsultarProveedorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnConsultarProveedorActionPerformed
        //Variables que contendran los valores de los campos
        String nombre;
               
        //Se extrae el nombre del proveedor seleccionado
        nombre = cmbNombreProveedor.getSelectedItem().toString();

        if (nombre.equalsIgnoreCase("<--Seleccione un proveedor-->")) {
            JOptionPane.showMessageDialog(null, "Seleccione un nombre de proveedor", "Selección de datos", JOptionPane.ERROR_MESSAGE);
        } else {
            //Inicio de sesion de hibernate
            Session session = null;
            //Cadena de seleccion de HQL
            String hql = "FROM alemar.entidad.Proveedor AS p WHERE p.nombre = :nombre";
            
                try{
                    //Conexion a la BD
                    sessionFactory = HibernateConexion.getSessionFactory();
                    //Apertura de la sesion
                    session = sessionFactory.openSession();
                    Transaction txt = session.beginTransaction();                                       
                    
                    //Extraccion de los datos del proveedor de la BD                    
                    Query query = session.createQuery(hql).setString("nombre", nombre);
                    
                //Se extrae los registros del usuario
                listaProveedor = query.list();
                
                //Se ponen los datos del proveedor en los campos de texto para su respectiva consulta
                txtDireccionProveedor.setText(listaProveedor.get(0).getDireccion());
                txtTelefonoProveedor.setText(listaProveedor.get(0).getTelefono());
                txtProductosProveedor.setText(listaProveedor.get(0).getProductosOferta());
                txtemail.setText(listaProveedor.get(0).getEmail());
                txt.commit();
                //Finalizacion de la sesion de hibernate
                session.close();

            } catch (org.hibernate.JDBCException e) {
                JOptionPane.showMessageDialog(null, "No se puede establecer conexión con la base de datos.", "Error de conexión", JOptionPane.ERROR_MESSAGE);
            }
        }
    }
       private void cmbNombreProveedorItemStateChanged(java.awt.event.ItemEvent evt) {

        //Si ningun usuario se encuentra seleccionado, entonces se limpian los datos
        if (cmbNombreProveedor.getSelectedIndex() == 0) {
            //Limpio todos los campos 
            txtDireccionProveedor.setText("");
            txtTelefonoProveedor.setText("");
            txtProductosProveedor.setText(""); 
            txtemail.setText(""); 
            
        }
   
    }//GEN-LAST:event_btnConsultarProveedorActionPerformed

      
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnConsultarProveedor;
    private javax.swing.JComboBox cmbNombreProveedor;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JTextField txtDireccionProveedor;
    private javax.swing.JTextArea txtProductosProveedor;
    private javax.swing.JTextField txtTelefonoProveedor;
    private javax.swing.JTextField txtemail;
    // End of variables declaration//GEN-END:variables
}

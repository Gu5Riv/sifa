package alemar.entidad;
// Generated 10-21-2013 10:10:30 AM by Hibernate Tools 3.2.1.GA



/**
 * Proveedor generated by hbm2java
 */
public class Proveedor  implements java.io.Serializable {


     private Long idProveedor;
     private String nombre;
     private String direccion;
     private String telefono;
     private String productosOferta;
     private String email;

    public Proveedor() {
    }

	
    public Proveedor(String nombre, String direccion, String telefono, String productosOferta) {
        this.nombre = nombre;
        this.direccion = direccion;
        this.telefono = telefono;
        this.productosOferta = productosOferta;
    }
    public Proveedor(String nombre, String direccion, String telefono, String productosOferta, String email) {
       this.nombre = nombre;
       this.direccion = direccion;
       this.telefono = telefono;
       this.productosOferta = productosOferta;
       this.email = email;
    }
   
    public Long getIdProveedor() {
        return this.idProveedor;
    }
    
    public void setIdProveedor(Long idProveedor) {
        this.idProveedor = idProveedor;
    }
    public String getNombre() {
        return this.nombre;
    }
    
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    public String getDireccion() {
        return this.direccion;
    }
    
    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }
    public String getTelefono() {
        return this.telefono;
    }
    
    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }
    public String getProductosOferta() {
        return this.productosOferta;
    }
    
    public void setProductosOferta(String productosOferta) {
        this.productosOferta = productosOferta;
    }
    public String getEmail() {
        return this.email;
    }
    
    public void setEmail(String email) {
        this.email = email;
    }




}



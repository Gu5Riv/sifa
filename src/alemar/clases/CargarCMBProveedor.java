//Clase que se encarga de cargar los usuarios registrados en la base de datos
package alemar.clases;

import alemar.configuracion.HibernateConexion;
import alemar.entidad.Proveedor;
import alemar.entidad.Usuario;
import java.util.List;
import javax.swing.JOptionPane;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

public class CargarCMBProveedor {

    //Atributos de la clase
    //Atributo para el uso de Hibernate
    private static SessionFactory sessionFactory = null;
    //Lista que contendra a todos los usuarios registrados en el sistema
    private List<Proveedor> listaProveedor = null;

    //Metodos de la clase
    //Metodo que recupera todos los usuarios registrados en la base de datos
    public List<Proveedor> cargarProveedores() {
        //Obtencion de todos los usuarios de la base de datos para llenar el comboBox de manera dinamica

        //Inicializacion de session de hibernate
        Session session = null;

        try {
            //Variables de conexion que ocupa Hibernate
            sessionFactory = HibernateConexion.getSessionFactory();
            session = sessionFactory.openSession();
            Transaction tx = session.beginTransaction();

            //Obteniendo de la BD todos los usuarios                
            listaProveedor = session.createQuery("from Proveedor").list();
        } catch (org.hibernate.JDBCException e) { //Manejo de las excepciones de hibernate
            JOptionPane.showMessageDialog(null, "No se pudo establecer la conexión con la base de datos", "Error de conexión", JOptionPane.ERROR_MESSAGE);
        }

        //Retornamos los usuarios registarados en la base de datos
        return listaProveedor;
    }
}

//Clase que se encarga de establecer la fecha y la hora dinamicamente en el sistema
package alemar.clases;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

public class RelojVisual extends JLabel implements Runnable {

    //Atributos de la clase
    
    //Variables que contendran los datos de la fecha y hora
    private String dia, mes, año, hora, minutos, segundos;
    //Se instancia la clase Calendar para crear un nuevo calendario
    private Calendar calendario = new GregorianCalendar();
    //Creamos el hilo del constructor (El cual trabajara en tiempo real)
    Thread hilo;
       
    //Constructor de la clase
    public RelojVisual() {
        //Inicializamos el hilo
        hilo = new Thread(this);
        hilo.start();
    }

    //Metodo run el cual nos devuelve la fecha y hora con formato
    public void run() {
        
        //Esta clase se encraga de manejar los hilos
        Thread ct = Thread.currentThread();
        
        while (ct == hilo) {
            try {
                //Actualizamos el valor de la hora
                actualiza();
                
                //Java devuelve los meses desde 0 por lo tanto para que sea util para el usuario sumamos 1
                int mesE;
                mesE = Integer.valueOf(mes) + 1;
                
                //Se crea el formato final de la fecha y hora
                String resultado = dia + "/" + mesE + "/" + año + " , " + hora + ":" + minutos + ":" + segundos; 
                
                //Agregamos la fecha y la hora en el JLabel
                
                //Si no esta logeado mandamos el reloj a login
                if(VariablesGlobales.nombreUsuario.equals(""))
                {
                    alemar.interfaz.Login.jLabel5.setText(resultado);
                }
                //Si esta logeado mandamos el reloj a cambiar usuario
                if(!VariablesGlobales.nombreUsuario.equals(""))
                {
                    alemar.interfaz.CambiarUsuario.jLabel6.setText(resultado);
                }
                //El hilo finaliza al pasar un segundo                 
                Thread.sleep(1000);
                
            } catch (InterruptedException ex) { //En caso que no se pueda recuperar la información del sistema se muestra la siguiente información
                JOptionPane.showMessageDialog(null, "Se produjo un error inesperado al momento de actualizar la fecha y hora.", "Error en Fecha y hora", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    //Clase que se encarga de actualizar los datos de la fecha y la hora
    public void actualiza() {
        //Variables que recuperan la fecha y la hora actual
        Date fechaHoraActual = new Date();
        calendario.setTime(fechaHoraActual);

        //Se extrae y se da formato a la fecha y hora del sistema
        hora = String.valueOf(calendario.get(Calendar.HOUR_OF_DAY));
        minutos = calendario.get(Calendar.MINUTE) > 9 ? "" + calendario.get(Calendar.MINUTE) : "0" + calendario.get(Calendar.MINUTE);
        segundos = calendario.get(Calendar.SECOND) > 9 ? "" + calendario.get(Calendar.SECOND) : "0" + calendario.get(Calendar.SECOND);
        dia = calendario.get(Calendar.DATE) > 9 ? "" + calendario.get(Calendar.DATE) : "0" + calendario.get(Calendar.DATE);
        mes = calendario.get(Calendar.MONTH) > 9 ? "" + calendario.get(Calendar.MONTH) : "0" + calendario.get(Calendar.MONTH);
        año = calendario.get(Calendar.YEAR) > 9 ? "" + calendario.get(Calendar.YEAR) : "0" + calendario.get(Calendar.YEAR);
    }
} 
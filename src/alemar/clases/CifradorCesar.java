//Clase que se encaraga de controla el cifrado de datos en la base

package alemar.clases;

public class CifradorCesar {
    
    //Atributos utilizados para la codificación y decodificación de los datos
    private int desplazamiento;
    private String cadena;
    private String codificada="";
    private String decodificada="";
 
    //Metodos que se encargan del cifrado y la decodificación de los datos
    
    public CifradorCesar(String contraseña,int desp) {
        this.cadena = contraseña;
        this.desplazamiento = desp;
    }
 
    //Si desplazan un numero de posiciones gacua arriba los caracteres de la cadena ingresada
    public void codCesar(){
        int nuevo_caracter;
        for(int i = 0; i < this.cadena.length(); i++){
            nuevo_caracter = (int) this.cadena.charAt(i) + desplazamiento;
            this.codificada = this.codificada + (char) nuevo_caracter;
        }
    }
 
    //Se desplazan un numero de posiciones hacia abajo los caracteres de la cadena ingresada
    public void decodCesar(){
        int nuevo_caracter;
        for(int i = 0; i < this.cadena.length(); i++){
            nuevo_caracter = (int) this.cadena.charAt(i) - this.desplazamiento;
            this.decodificada = this.decodificada + (char) nuevo_caracter;
        }
    }
 
    //Se obtienen los datos codificados y decodificados
    
    public String getCodificada() {
        return codificada;
    }
 
    public String getDecodificada() {
        return decodificada;
    }
}
